Required JSON data for site config

**General**

-   storeColor

---

**First Screen**

-   storeColor
-   splashLogo
-   firstScreen_heroimage
-   firstScreen_heading
-   firstScreen_sub_heading
-   firstScreen_setup_location
-   firstScreen_login
