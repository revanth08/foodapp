<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

  class UserAvailability extends Model
{
    protected $casts = ['user_id' => 'integer'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return mixed
     */
    public function useravailability()
    {
        return $this->belongsTo('App\UserAvailability');
    }
}
