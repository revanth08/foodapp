<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $casts = ['user_id' => 'integer'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function wallet_transactions()
    {
        return $this->hasMany('App\WalletTransactions');
    }

}
