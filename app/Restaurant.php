<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $casts = ['is_featured' => 'integer'];

    protected $hidden = array('created_at', 'updated_at');

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }

    public function toggleAcceptance()
    {
        $this->is_accepted = !$this->is_accepted;
        return $this;
    }
}
