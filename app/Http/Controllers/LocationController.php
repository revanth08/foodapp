<?php

namespace App\Http\Controllers;

use App\Location;

class LocationController extends Controller
{
    public function searchLocation($query)
    {
        if ($query) {
            $locations = Location::where('name', 'LIKE', "%$query%")
                ->where("is_active", "1")
                ->get();
            return response()->json($locations);
        } else {
            //return popular cities
            $locations = Location::where('is_popular', "1")
                ->where("is_active", "1")
                ->get()
                ->take(5);
            return response()->json($locations);
        }
    }
    public function popularLocations()
    {
        $locations = Location::where('is_popular', "1")
            ->where("is_active", "1")
            ->get()
            ->take(10);
        return response()->json($locations);
    }
}
