<?php

namespace App\Http\Controllers;

use App\Addon;
use App\AddonCategory;
use App\Item;
use App\ItemCategory;
use App\Location;
use App\Order;
use App\Orderitem;
use App\PushNotify;
use App\Restaurant;
use App\Kitchen;
use App\RestaurantEarning;
use App\RestaurantPayout;
use App\User;
use App\Wallet;
use App\WalletTransactions;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Image;

class RestaurantOwnerController extends Controller
{
    public function dashboard()
    {
        $user = Auth::user();

        $restaurant = $user->restaurants;

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $newOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->select('*', DB::raw("concat(orderitems.name, ' * ', orderitems.quantity) as names, orderitems.id as orderitemsId"))
            ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->where('orderstatus_id', '1')
            ->where('declined', 0)
            ->orderBy('orders.id', 'DESC')
            ->with('restaurant', 'restaurant.location')
            ->paginate(10);
        //     ->toSql();
        // dd($newOrders);
        

        $newOrdersJS = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', '1')
            ->with('restaurant', 'restaurant.location')
            ->get();

        $acceptedOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->select('orders.*', 'orderitems.*', DB::raw('concat(orderitems.name, " * ", orderitems.quantity ) as names'))
            ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->where('orderstatus_id', '2')
            ->where('declined', 0)
            ->orderBy('orders.id')
            ->paginate(10);

        $completedOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->select('orders.*', 'orderitems.*', DB::raw('group_concat(orderitems.name, " * ", orderitems.quantity SEPARATOR ", ") as names'))
			->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
            ->where('orderstatus_id', "5")
            ->where('declined', 0)
            ->orderBy('orders.created_at', 'DESC')
            ->groupBy('orders.id')
            ->limit(10)
            ->get();

        $recentOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->take(10)
            ->get();

        $allOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->with('orderitems')
            ->get();

        $ordersCount = count($allOrders);

        $orderItemsCount = 0;
        foreach ($allOrders as $order) {
            $orderItemsCount += count($order->orderitems);
        }

        $allCompletedOrders = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('orderstatus_id', '5')
            ->with('orderitems')
            ->get();

        $totalEarning = 0;
        settype($var, 'float');
        
        $displayorder = Order::with('orderitems', 'orderitems.order_item_addons')
                    ->orderBy('id', 'DESC')
                    ->limit(10)
                    ->get();

        foreach ($allCompletedOrders as $completedOrder) {
            $totalEarning += $completedOrder->total;
        }

        return view('restaurantowner.dashboard', array(
            'restaurantsCount' => count($user->restaurants),
            'ordersCount' => $ordersCount,
            'orderItemsCount' => $orderItemsCount,
            'totalEarning' => $totalEarning,
            'newOrders' => $newOrders,
            'newOrdersJS' => $newOrdersJS,
            'acceptedOrders' => $acceptedOrders,
            'aCO' =>  $allCompletedOrders,
            'completedOrders' => $completedOrders
        ));
    }

    public function getNewOrders()
    {
        $user = Auth::user();

        $restaurant = $user->restaurants;

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $newOrders = Order::whereIn('restaurant_id', $restaurantIds)
                    ->select('*', DB::raw("concat(orderitems.name, ' * ', orderitems.quantity) as names", 'orderitems.id AS orderitemsId'))
                    ->join('orderitems', 'orderitems.order_id', '=', 'orders.id')
                    ->where('orderstatus_id', '1')
                    ->orderBy('orders.id', 'DESC')
                    ->with('restaurant', 'restaurant.location')
                    ->paginate(10);



        return response()->json($newOrders);
    }

    /**
     * @param $id
     */
    public function acceptOrder($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $order = Order::where('unique_order_id', $id)->whereIn('restaurant_id', $restaurantIds)->first();
        $orderid = Order::where('unique_order_id', $id)->first();
        
        DB::table('orderitems')
            ->where('order_id', $orderid->id)
            ->update(array('item_status' => 2));
            

        if ($order->orderstatus_id == '1') {
            $order->orderstatus_id = 2;
            $order->save();

            if (config('settings.enablePushNotificationOrders') == 'true') {

                //to user
                $notify = new PushNotify();
                $notify->sendPushNotification('2', $order->user_id);
            }

            return redirect()->back()->with(array('success' => 'Order Accepted'));
        } else {
            return redirect()->back()->with(array('message' => 'Something went wrong.'));
        }
    }

    public function restaurants()
    {
        $user = Auth::user();
        $restaurants = $user->restaurants;
        $locations = Location::all();
        $showChart = false;

        return view('restaurantowner.restaurants', array(
            'restaurants' => $restaurants,
            'locations' => $locations,
            'showChart' => $showChart
        ));
    }

    /**
     * @param $id
     */
    public function getEditRestaurant($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $restaurant = Restaurant::where('id', $id)->whereIn('id', $restaurantIds)->first();

        if ($restaurant) {
            $locations = Location::get();

            return view('restaurantowner.editRestaurant', array(
                'restaurant' => $restaurant,
                'locations' => $locations,
            ));
        } else {
            return redirect()->route('restaurant.restaurants')->with(array('message' => 'Access Denied'));
        }

    }

    /**
     * @param $id
     */
    public function disableRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $restaurant->toggleActive()->save();
            return redirect()->back()->with(array('success' => 'Operation Successful'));
        } else {
            return redirect()->route('restaurant.restaurants');
        }
    }



//  public function searchItems(Request $request)
//     {
//         $user = Auth::user();

//         $restaurantIds = $user->restaurants->pluck('id')->toArray();

//         $query = $request['query'];
//         $items = Item::whereIn('restaurant_id', $restaurantIds)
//             ->where('name', 'LIKE', '%' . $query . '%')
//             ->take('100')
//             ->paginate();
//         $count = count($items);
//         $restaurants = Restaurant::get();
//         $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
//         $itemCategories = ItemCategory::where('is_enabled', '1')->get();
//         $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

//         return view('restaurantowner.items', array(
//             'items' => $items,
//             'count' => $count,
//             'restaurants' => $restaurants,
//             'query' => $query,
//             'kitchens' => $kitchens,
//             'itemCategories' => $itemCategories,
//             'addonCategories' => $addonCategories,
//         ));
//     }



    /**
     * @param Request $request
     */
    public function saveNewRestaurant(Request $request)
    {
        $restaurant = new Restaurant();

        $restaurant->name = $request->name;
        $restaurant->description = $request->description;
        $restaurant->location_id = $request->location_id;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(150, 150)
            ->save(base_path('assets/img/restaurants/' . $filename));
        $restaurant->image = '/assets/img/restaurants/' . $filename;
        Image::make($image)
            ->resize(20, 20)
            ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
        $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;

        $restaurant->delivery_time = $request->delivery_time;
        $restaurant->price_range = $request->price_range;

        if ($request->is_pureveg == 'true') {
            $restaurant->is_pureveg = true;
        } else {
            $restaurant->is_pureveg = false;
        }

        $restaurant->slug = str_slug($request->name) . '-' . str_random(15);
        $restaurant->certificate = $request->certificate;

        $restaurant->address = $request->address;
        $restaurant->pincode = $request->pincode;
        $restaurant->landmark = $request->landmark;
        $restaurant->latitude = $request->latitude;
        $restaurant->longitude = $request->longitude;

        $restaurant->restaurant_charges = $request->restaurant_charges;
        $restaurant->delivery_charges = $request->delivery_charges;

        $restaurant->sku = time() . str_random(10);

        $restaurant->is_active = 0;

        try {
            $restaurant->save();
            $user = Auth::user();
            $user->restaurants()->attach($restaurant);
            return redirect()->back()->with(array('success' => 'Restaurant Saved'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }
    }

    /**
     * @param Request $request
     */
    public function updateRestaurant(Request $request)
    {
        $restaurant = Restaurant::where('id', $request->id)->first();

        if ($restaurant) {
            $restaurant->name = $request->name;
            $restaurant->description = $request->description;
            $restaurant->location_id = $request->location_id;

            if ($request->image == null) {
                $restaurant->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(150, 150)
                    ->save(base_path('assets/img/restaurants/' . $filename));
                $restaurant->image = '/assets/img/restaurants/' . $filename;
                Image::make($image)
                    ->resize(20, 20)
                    ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
                $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;
            }

            $restaurant->delivery_time = $request->delivery_time;
            $restaurant->price_range = $request->price_range;

            if ($request->is_pureveg == 'true') {
                $restaurant->is_pureveg = true;
            } else {
                $restaurant->is_pureveg = false;
            }

            $restaurant->certificate = $request->certificate;

            $restaurant->address = $request->address;
            $restaurant->pincode = $request->pincode;
            $restaurant->landmark = $request->landmark;
            $restaurant->latitude = $request->latitude;
            $restaurant->longitude = $request->longitude;

            $restaurant->restaurant_charges = $request->restaurant_charges;
            $restaurant->delivery_charges = $request->delivery_charges;

            try {
                $restaurant->save();
                return redirect()->back()->with(array('success' => 'Restaurant Saved'));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }
        }
    }

    public function itemcategories()
    {
        $itemCategories = ItemCategory::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        $count = count($itemCategories);

        return view('restaurantowner.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
        ));
    }
    
    
     public function searchitemcategories(Request $request)
    {
        
        $user = Auth::user();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $query = $request['query'];
        $itemCategories = ItemCategory::where('user_id', Auth::user()->id)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($itemCategories);

 

        return view('restaurantowner.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
            'query' => $query,
        ));
    }
    

    /**
     * @param Request $request
     */
    public function createItemCategory(Request $request)
    {
        $itemCategory = new ItemCategory();

        $itemCategory->name = $request->name;
        $itemCategory->user_id = Auth::user()->id;

        try {
            $itemCategory->save();
            return redirect()->back()->with(array('success' => 'Category Created'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }
    }

    /**
     * @param $id
     */
    public function disableCategory($id)
    {
        $itemCategory = ItemCategory::where('id', $id)->first();
        if ($itemCategory) {
            $itemCategory->toggleEnable()->save();
            return redirect()->back()->with(array('success' => 'Operation Successful'));
        } else {
            return redirect()->route('restaurant.itemcategories');
        }
    }

    public function items()
    {
        $user = Auth::user();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $items = Item::whereIn('restaurant_id', $restaurantIds)
            // ->where('quantity', '>', 0)
            ->orderBy('id', 'DESC')
            ->paginate(20);

        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)
          ->orderBy('id', 'DESC')
          ->get();

        $count = Item::whereIn('restaurant_id', $restaurantIds)->count();

        $restaurants = $user->restaurants;

        $itemCategories = ItemCategory::where('is_enabled', '1')
            ->where('user_id', Auth::user()->id)
            ->get();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchItems(Request $request)
    {
        $user = Auth::user();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $query = $request['query'];
        $items = Item::whereIn('restaurant_id', $restaurantIds)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($items);
        $restaurants = Restaurant::get();
        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
        $itemCategories = ItemCategory::where('is_enabled', '1')->get();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'query' => $query,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewItem(Request $request)
    {
        // dd($request->all());

        $item = new Item();

        $item->name = $request->name;
        $item->price = $request->price;
        $item->restaurant_id = $request->restaurant_id;
        $item->kitchen_id =  isset($request->kitchen_id) ? $request->kitchen_id : 0;
        $item->item_category_id = $request->item_category_id;
        $item->from_time = $request->from_time;
        $item->to_time = $request->to_time;
        $item->quantity = $request->quantity;
        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(162, 118)
            ->save(base_path('assets/img/items/' . $filename));
        $item->image = '/assets/img/items/' . $filename;
        Image::make($image)
            ->resize(25, 18)
            ->save(base_path('assets/img/items/small/' . $filename_sm));
        $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;

        if ($request->is_recommended == 'true') {
            $item->is_recommended = true;
        } else {
            $item->is_recommended = false;
        }

        if ($request->is_popular == 'true') {
            $item->is_popular = true;
        } else {
            $item->is_popular = false;
        }

        if ($request->is_new == 'true') {
            $item->is_new = true;
        } else {
            $item->is_new = false;
        }

        try {
            $item->save();
            if (isset($request->addon_category_item)) {
                $item->addon_categories()->sync($request->addon_category_item);
            }
            return redirect()->back()->with(array('success' => 'Item Saved'));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }

    }

    /**
     * @param $id
     */
    public function getEditItem($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $item = Item::where('id', $id)
            ->whereIn('restaurant_id', $restaurantIds)
            ->first();
        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)
              ->orderBy('name', 'DESC')
              ->get();

        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        if ($item) {
            $restaurants = $user->restaurants;
            $itemCategories = ItemCategory::where('is_enabled', '1')
                ->where('user_id', Auth::user()->id)
                ->get();

            return view('restaurantowner.editItem', array(
                'item' => $item,
                'restaurants' => $restaurants,
                'kitchens' => $kitchens,
                'itemCategories' => $itemCategories,
                'addonCategories' => $addonCategories,
            ));
        } else {
            return redirect()->route('restaurant.items')->with(array('message' => 'Access Denied'));
        }

    }

    /**
     * @param $id
     */
    public function disableItem($id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $item = Item::where('id', $id)
            ->whereIn('restaurant_id', $restaurantIds)
            ->first();
        if ($item) {
            $item->toggleActive()->save();
            return redirect()->back()->with(array('success' => 'Operation Successful'));
        } else {
            return redirect()->route('restaurant.items');
        }
    }

    /**
     * @param Request $request
     */
    public function updateItem(Request $request)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $item = Item::where('id', $request->id)
            ->whereIn('restaurant_id', $restaurantIds)
            // ->where('quantity', '>', 0)
            ->first();

        if ($item) {

            $item->name = $request->name;
            $item->restaurant_id = $request->restaurant_id;
            $item->kitchen_id =  isset($request->kitchen_id) ? $request->kitchen_id : 0;
            $item->item_category_id = $request->item_category_id;
            $item->from_time = $request->from_time;
            $item->to_time = $request->to_time;
            $item->quantity = $request->quantity;
            if ($request->image == null) {
                $item->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(162, 118)
                    ->save(base_path('assets/img/items/' . $filename));
                $item->image = '/assets/img/items/' . $filename;
                Image::make($image)
                    ->resize(25, 18)
                    ->save(base_path('assets/img/items/small/' . $filename_sm));
                $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;
            }

            $item->price = $request->price;

            if ($request->is_recommended == 'true') {
                $item->is_recommended = true;
            } else {
                $item->is_recommended = false;
            }

            if ($request->is_popular == 'true') {
                $item->is_popular = true;
            } else {
                $item->is_popular = false;
            }

            if ($request->is_new == 'true') {
                $item->is_new = true;
            } else {
                $item->is_new = false;
            }

            try {
                $item->save();
                if (isset($request->addon_category_item)) {
                    $item->addon_categories()->sync($request->addon_category_item);
                }
                return redirect()->back()->with(array('success' => 'Item Saved'));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }
        }
    }

    public function addonCategories()
    {
        $count = AddonCategory::all()->count();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return view('restaurantowner.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddonCategories(Request $request)
    {
        $query = $request['query'];
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($addonCategories);

        return view('restaurantowner.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddonCategory(Request $request)
    {
        $addonCategory = new AddonCategory();

        $addonCategory->name = $request->name;
        $addonCategory->type = $request->type;
        $addonCategory->user_id = Auth::user()->id;

        try {
            $addonCategory->save();
            return redirect()->back()->with(array('success' => 'Addon Category Saved'));
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        } catch (Exception $e) {
            return redirect()->back()->with(array('message' => $e->getMessage()));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }
    }

    /**
     * @param $id
     */
    public function getEditAddonCategory($id)
    {
        $addonCategory = AddonCategory::where('id', $id)->first();
        if ($addonCategory) {
            if ($addonCategory->user_id == Auth::user()->id) {
                return view('restaurantowner.editAddonCategory', array(
                    'addonCategory' => $addonCategory,
                ));
            } else {
                return redirect()
                    ->route('restaurant.editAddonCategory')
                    ->with(array('message' => 'Access Denied'));
            }
        } else {
            return redirect()
                ->route('restaurant.editAddonCategory')
                ->with(array('message' => 'Access Denied'));
        }

    }

    /**
     * @param Request $request
     */
    public function updateAddonCategory(Request $request)
    {
        $addonCategory = AddonCategory::where('id', $request->id)->first();

        if ($addonCategory) {

            $addonCategory->name = $request->name;
            $addonCategory->type = $request->type;

            try {
                $addonCategory->save();
                return redirect()->back()->with(array('success' => 'Addon Category Updated'));
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
            } catch (Exception $e) {
                return redirect()->back()->with(array('message' => $e->getMessage()));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }
        }
    }

    public function addons()
    {
        $addons = Addon::where('user_id', Auth::user()->id)->paginate();
        $count = Addon::where('user_id', Auth::user()->id)->count();
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddons(Request $request)
    {
        $query = $request['query'];
        $addons = Addon::where('user_id', Auth::user()->id)
            ->where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($addons);
        $addonCategories = AddonCategory::where('user_id', Auth::user()->id)->get();

        return view('restaurantowner.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddon(Request $request)
    {
        $addon = new Addon();

        $addon->name = $request->name;
        $addon->price = $request->price;
        $addon->user_id = Auth::user()->id;
        $addon->addon_category_id = $request->addon_category_id;

        try {
            $addon->save();
            return redirect()->back()->with(array('success' => 'Addon Saved'));
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        } catch (Exception $e) {
            return redirect()->back()->with(array('message' => $e->getMessage()));
        } catch (\Throwable $th) {
            return redirect()->back()->with(array('message' => $th));
        }

    }

    /**
     * @param $id
     */
    public function getEditAddon($id)
    {
        $addon = Addon::where('id', $id)
            ->where('user_id', Auth::user()->id)
            ->first();

        if ($addon) {
            return view('restaurantowner.editAddon', array(
                'addon' => $addon,
            ));
        } else {
            return redirect()->route('restaurant.addons')->with(array('message' => 'Access Denied'));
        }
    }

    /**
     * @param Request $request
     */
    public function updateAddon(Request $request)
    {
        $addon = Addon::where('id', $request->id)->first();

        if ($addon) {
            if ($addon->user_id == Auth::user()->id) {
                $addon->name = $request->name;
                $addon->price = $request->price;
                $addon->addon_category_id = $request->addon_category_id;

                try {
                    $addon->save();
                    return redirect()->back()->with(array('success' => 'Addon Updated'));
                } catch (\Illuminate\Database\QueryException $qe) {
                    return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
                } catch (Exception $e) {
                    return redirect()->back()->with(array('message' => $e->getMessage()));
                } catch (\Throwable $th) {
                    return redirect()->back()->with(array('message' => $th));
                }
            } else {
                return redirect()->route('restaurant.addons')->with(array('message' => 'Access Denied'));
            }
        } else {
            return redirect()->route('restaurant.addons')->with(array('message' => 'Access Denied'));
        }
    }

    public function orders()
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $itemSold = Orderitem::select('name', DB::raw("SUM(quantity) as totalQuantity"), 'price')
                             ->where('item_status', 3)
                             ->where('declined', 0)
                             ->groupBy('item_id')
                             ->orderBy('totalQuantity', 'desc')
                             ->limit(10)
                             ->get();

        $count = Order::whereIn('restaurant_id', $restaurantIds)->count();
        $orders = Order::orderBy('id', 'DESC')
                        ->whereIn('restaurant_id', $restaurantIds)
                        ->with('accept_delivery.user')
                        ->limit(10)
                        ->get();                
        return view('restaurantowner.orders', array(
            'orders' => $orders,
            'count' => $count,
            'itemSold' => $itemSold
        ));
    }

    /**
     * @param Request $request
     */
    public function postSearchOrders(Request $request)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $query = $request['query'];
        $orders = Order::whereIn('restaurant_id', $restaurantIds)
            ->where('unique_order_id', 'LIKE', '%' . $query . '%')
           ->take('100')
            ->paginate();
        $count = count($orders);
        return view('restaurantowner.orders', array(
            'orders' => $orders,
            'count' => $count,
        ));
        
    }

    /**
     * @param $order_id
     */
    public function viewOrder($order_id)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $order = Order::whereIn('restaurant_id', $restaurantIds)
            // ->select('*', )
            // ->join('orderitems', 'orderitems.order_id','=', 'orders.id')
            ->where('unique_order_id', $order_id)
            ->first();
        // dd($order);

        $orderIds = Order::whereIn('restaurant_id', $restaurantIds)
                        ->where('unique_order_id', $order_id)
                        ->get();
        // dd($orderId[0]->user_id);
        $orderId= $orderIds[0]->id;
        $declinedItems = Orderitem::where('order_id', $orderId)
                                    ->where('declined', 1)
                                    ->get();
        // dd($declinedItems);
        if ($order) {
            return view('restaurantowner.viewOrder', array(
                'order' => $order,
                'declinedItems' =>$declinedItems
            ));
        } else {
            return redirect()->route('restaurantowner.orders');
        }
    }

    /**
     * @param $restaurant_id
     */
    public function earnings(Request $request)
    {   
        
        
        $restaurant_id  = $request->restaurant_id;
        $showChart = false;
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();

        $daysChartOnline = Order::where('restaurant_id',$restaurantIds)
                                ->where('orderstatus_id', '=', 5)
                                ->where(DB::raw('LEFT(`created_at`, 10)'),'=', date("Y-m-d", time()))
                                ->where('payment_mode', 'WALLET')
                                ->sum('total');
                                
        

        $daysChartOffline = Order::where('restaurant_id',47)
                                ->where('orderstatus_id', '=', 5)
                                ->where(DB::raw('LEFT(`created_at`, 10)'),'=', date("Y-m-d", time()))
                                ->where('payment_mode', 'QRPAYMENT')
                                ->sum('total');
                                // ->toSql();

        $chartDataPresentDay = '[';
        $chartDataPresentDay .= '{value:' . $daysChartOnline . ", name: `Online`}," . '{value:'. $daysChartOffline . ", name: `Offline`}";
        $chartDataPresentDay .= ']';


        if ($restaurant_id) {
            // dd($request);
            $user = Auth::user();
            $restaurant = $user->restaurants;
            $restaurantIds = $user->restaurants->pluck('id')->toArray();


            $restaurant = Restaurant::where('id', $restaurant_id)->first();
            // check if restaurant exists
            if ($restaurant) {
                $dateRange =  $request->datetimes;
                $from1  = explode("-", $dateRange)[0];
                $from = substr($from1, 0, -1);
                $to1 = explode("-",$dateRange)[1];
                $to = substr($to1, 1);
                // dd($from, $to);

                
                $showChart = true;
                //check if restaurant belongs to the auth user
                $contains = Arr::has($restaurantIds, $restaurant->id);
                // if ($contains) {
                    //true
                    $allCompletedOrders = Order::where('restaurant_id', $restaurant->id)
                        ->where('orderstatus_id', '5')
                        ->get();

                    $totalEarning = 0;
                    settype($var, 'float');

                    foreach ($allCompletedOrders as $completedOrder) {
                        $totalEarning += $completedOrder->total;
                    }

                    // Build an array of the dates we want to show, oldest first
                    $dates = collect();
                    foreach (range(-30, 0) as $i) {
                        $date = Carbon::now()->addDays($i)->format('Y-m-d');
                        $dates->put($date, 0);
                    }

                    // Get the post counts
                    $posts = Order::where('restaurant_id', $restaurant->id)
                        ->where('orderstatus_id', '5')
                        ->where('created_at', '>=', $dates->keys()->first())
                        ->groupBy('date')
                        ->orderBy('date')
                        ->get([
                            DB::raw('DATE( created_at ) as date'),
                            DB::raw('SUM( total ) as "total"'),
                        ])
                        ->pluck('total', 'date');

                    // Merge the two collections; any results in `$posts` will overwrite the zero-value in `$dates`
                    $dates = $dates->merge($posts);

                    // dd($dates);
                    $monthlyDate = '[';
                    $monthlyEarning = '[';
                    foreach ($dates as $date => $value) {
                        $monthlyDate .= "'" . $date . "' ,";
                        $monthlyEarning .= "'" . $value . "' ,";
                    }

                    $monthlyDate = rtrim($monthlyDate, ' ,');
                    $monthlyDate = $monthlyDate . ']';

                    $monthlyEarning = rtrim($monthlyEarning, ' ,');
                    $monthlyEarning = $monthlyEarning . ']';
                    /*=====  End of Monthly Post Analytics  ======*/

                    

                    $balance = RestaurantEarning::where('restaurant_id', $restaurant->id)
                        ->where('is_requested',"=", 0)
                        ->first();

                    $totalFromOnline = Order::where('restaurant_id', $restaurant->id)
                                            ->where('orderstatus_id', '=', 5)
                                            ->whereBetween('created_at', [$from, $to])
                                            ->where('payment_mode', 'WALLET')
                                            ->sum('total');

                    $totalFromOffline = Order::where('restaurant_id', $restaurant->id)
                                            ->where('orderstatus_id', '=', 5)
                                            ->whereBetween('created_at', [$from, $to])
                                            ->where('payment_mode', 'QRPAYMENT')
                                            ->sum('total');
                    
                    // $chartData =
                    $chartData = '[';
                    $chartData .= '{value:' . $totalFromOnline . ", name: `Online`}," . '{value:'. $totalFromOffline . ", name: `Offline`}";
                    $chartData .= ']';
                    // dd($chartData);
                    

                    if (!$balance) {
                        $balanceBeforeCommission = 0;
                        $balanceAfterCommission = 0;
                    } else {
                        $balanceBeforeCommission = $balance->amount;
                        $balanceAfterCommission = ($balance->amount - ($restaurant->commission_rate / 100) * $balance->amount);
                        $balanceAfterCommission = number_format((float) $balanceAfterCommission, 2, '.', '');
                    }

                    $payoutRequests = RestaurantPayout::where('restaurant_id', $restaurant_id)->orderBy('id', 'DESC')->get();

                    return view('restaurantowner.earnings', array(
                        '$restaurant_id' =>$restaurant_id,
                        'restaurant' => $restaurant,
                        'totalEarning' => $totalEarning,
                        'monthlyDate' => $monthlyDate,
                        'monthlyEarning' => $monthlyEarning,
                        'balanceBeforeCommission' => $balanceBeforeCommission,
                        'balanceAfterCommission' => $balanceAfterCommission,
                        'payoutRequests' => $payoutRequests,
                        'chartData' => $chartData,
                        'showChart' => $showChart,
                        'fromDate' => $from, 
                        'toDate' => $to
                    ));
                // } else {
                //     return redirect()->route('restaurant.earnings')->with(array('message' => 'Access D'));
                // }
            } else {
                return redirect()->route('restaurant.earnings')->with(array('message' => 'Access Denied'));
            }

        } else {
            $user = Auth::user();
            $restaurants = $user->restaurants;

            return view('restaurantowner.earnings', array(
                'restaurants' => $restaurants,
                'chartDataPresentDay' => $chartDataPresentDay
            ));
        }

    }

    public function postearningsDateRange(Request $request) {

    }

    /**
     * @param Request $request
     */
    public function sendPayoutRequest(Request $request)
    {
        $user = Auth::user();
        $restaurant = DB::table('restaurants')->where('id', $request->restaurant_id)->first();
        
        
        $earning = RestaurantEarning::where('restaurant_id', $request->restaurant_id)
            ->where('is_requested', 0)
            ->first();


        $balance = RestaurantEarning::where('restaurant_id', $request->restaurant_id)
            ->where('is_requested', 0)
            ->first();
        
        if ($earning) {
            $payoutRequest = new RestaurantPayout;
            $payoutRequest->restaurant_id = $request->restaurant_id;
            $payoutRequest->restaurant_earning_id = $earning->id;
            $payoutRequest->amount = ($balance->amount - ((float)($restaurant->commission_rate) / 100) * $balance->amount);
            $payoutRequest->status = 'PENDING';
            try {
                $payoutRequest->save();
                $earning->is_requested = 1;
                $earning->restaurant_payout_id = $payoutRequest->id;
                $earning->save();
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
            } catch (Exception $e) {
                return redirect()->back()->with(array('message' => $e->getMessage()));
            } catch (\Throwable $th) {
                return redirect()->back()->with(array('message' => $th));
            }

            return redirect()->back()->with(array('success' => 'Payout Request Sent'));
        } else {
            return redirect()->route('restaurant.earnings')->with(array('message' => 'Access Denied'));
        }
    }

    public function pos()
    {
        $user = Auth::user();
        $t=date("Hi");
        $restaurantIds = $user->restaurants->pluck('id')->toArray();


        //$userIds = $user->users->pluck('id')->toArray();

        $items = Item::whereIn('restaurant_id', $restaurantIds)
            ->where('quantity', '>', 0)
            ->where('from_time', '<=', $t)
            ->where('to_time', '>=', $t)
            ->orderBy('id', 'DESC')
            ->get();

        $displayUsers = User::select('users.name','users.id','users.email','users.auth_token', 'ad.house')
                    ->leftJoin('restaurant_user AS ru','ru.user_id','=','users.id')
                    ->leftJoin('model_has_roles AS mr','mr.model_id','=','users.id')
                    ->leftJoin('addresses AS ad', 'ad.user_id', '=', 'users.id')
                    ->where('mr.role_id', '4')
                    ->whereNull('ru.user_id')
                    ->get();

        $displayResName = Restaurant::all();

        $displayItemCategory = Item::join('item_categories','item_categories.id','=', 'items.item_category_id')
                            ->whereIn('items.restaurant_id', $restaurantIds)
                            ->select('item_categories.id', 'item_categories.name')
                            ->distinct('item_categories.id')
                            ->get();

        $displayUniqueOrderID = Order::all();

        $displayWalletBalance = Wallet::all();

        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $restaurantC = $user->restaurants->pluck('code')->toArray();
        $restaurantIdFirst = head($restaurantIds);
        $restaurantCode = head($restaurantC);
        
        $kitchen = Kitchen::get();
        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
        $kitchenIds=Kitchen::whereIn('restaurant_id', $restaurantIds)->select('id')->get()->toArray();


        $count = Order::whereIn('restaurant_id', $restaurantIds)->count();
        $orders = Order::orderBy('id', 'DESC')
            ->whereIn('restaurant_id', $restaurantIds)
            ->with('accept_delivery.user')
            ->paginate('20');

        return view('restaurantowner.pos', array(
            'items' => $items,
            'orders' => $orders,
            'count' => $count,
            'users' => $displayUsers,
            'restaurants'=> $displayResName,
            'itemCategories' => $displayItemCategory,
            'unique_order_id' => $displayUniqueOrderID,
            'walletbalance' => $displayWalletBalance,
            'restaurantIdFirst' =>$restaurantIdFirst,
            'kitcheniId' =>$kitchenIds,
            'restaurantCode' =>$restaurantCode
        ));
    }
}
