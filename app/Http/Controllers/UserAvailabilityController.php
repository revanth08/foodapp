<?php

namespace App\Http\Controllers;

use App\Addon;
use App\Coupon;
use App\GpsTable;
use App\Item;
use App\Order;
use App\Orderitem;
use App\OrderItemAddon;
use App\Orderstatus;
use App\Restaurant;
use App\Wallet;
use App\WalletTransactions;
use App\User;
use App\UserAvailability;
use App\Http\Controllers\Controller;
use Hashids;
use Illuminate\Http\Request;
use Omnipay\Omnipay;
use Illuminate\Support\Facades\DB;


class UserAvailabilityController extends Controller
{
    /**
     * @param Request $request
     */
    public function saveUserAvailabilty(Request $request)
    {
        //save the wallet WalletTransactions
        $type = $request->availability['type'];
        $UserAvailabilityId = 0;
        foreach ($type as $value) {
          $UserAvailability = new UserAvailability();
          $UserAvailability->user_id = $request->user_id;
          $UserAvailability->from_date = $request->availability['datefrom'];
          if($request->availability['dateto'] == ''){
            $UserAvailability->to_date = $request->availability['datefrom'];
          }
          else{
            $UserAvailability->to_date = $request->availability['dateto'];
          }
          $UserAvailability->type = $value;
          $UserAvailability->save();
          $UserAvailabilityId = $UserAvailability->id;
        }
        if($UserAvailabilityId > 0){
            $response = [
                'success' => true,
                'data' => $UserAvailabilityId,
            ];
        }
        else{
            $response = [
                'success' => false,
            ];
        }
        return response()->json($response);
    }
    
    /**
     * @param Request $request
     */
    public function useravailability($param = null)
    {
        if($param){
            $splitDate = explode('&', $param, 2);
            $fromDate = $splitDate[0];
            $toDate = $splitDate[1];
            // if($fromDate == $toDate){
            //     $useravailabilities_breakfast = UserAvailability::whereBetween('from_date', array($fromDate, $toDate) )
            //         ->where('type', 'breakfast')
            //         ->get([DB::raw('COUNT( * ) as "user_counts"')]);
            //     $useravailabilities_lunch = UserAvailability::where('from_date', '=', $fromDate )
            //         ->Where('type', 'lunch')
            //         ->get([DB::raw('COUNT( * ) as "user_counts"')]);
            //     $useravailabilities_snacks = UserAvailability::where('from_date', '=', $fromDate )
            //         ->Where('type', 'snacks')
            //         ->get([DB::raw('COUNT( * ) as "user_counts"')]);
            //     $useravailabilities_dinner = UserAvailability::where('from_date', '=', $fromDate )
            //         ->Where('type', 'dinner')
            //         ->get([DB::raw('COUNT( * ) as "user_counts"')]);
        
            //     $displayUsers = User::all();
            //     $user_count = count($displayUsers);
                
            //     $count_breakfast = $useravailabilities_breakfast[0]['user_counts'];
            //     $count_lunch = $useravailabilities_lunch[0]['user_counts'];
            //     $count_snacks = $useravailabilities_snacks[0]['user_counts'];
            //     $count_dinner = $useravailabilities_dinner[0]['user_counts'];
            //     return view('admin.useravailability', array(
            //         'user_count' => (int)$user_count,
            //         'count_breakfast' => (int)$count_breakfast == 0 ? 0 : (int)$count_breakfast,
            //         'count_lunch' => (int)$count_lunch == 0 ? 0 : (int)$count_lunch,
            //         'count_snacks' => (int)$count_snacks == 0 ? 0 : (int)$count_snacks,
            //         'count_dinner' => (int)$count_dinner == 0 ? 0 : (int)$count_dinner,
            //         'user_opttedout' => ((int)$count_breakfast + (int)$count_lunch + (int)$count_snacks + (int)$count_dinner), 
            //         'user_available' => (int)$user_count - ((int)$count_breakfast + (int)$count_lunch + (int)$count_snacks + (int)$count_dinner), 
            //         'fromDate' => $fromDate,
            //         'toDate' => $toDate,
            //     ));
            // }
            // else{
                $useravailabilities_breakfast = UserAvailability::Where('type', 'breakfast')
                    ->where('from_date', '<=',$fromDate)
                    ->where('to_date', '>=',$toDate)
                    ->get([DB::raw('COUNT( * ) as "user_counts"')]);
                $useravailabilities_lunch = UserAvailability::Where('type', 'lunch')
                    ->where('from_date', '<=',$fromDate)
                    ->where('to_date', '>=',$toDate)
                    ->get([DB::raw('COUNT( * ) as "user_counts"')]);
                $useravailabilities_snacks = UserAvailability::Where('type', 'snacks')
                    ->where('from_date', '<=',$fromDate)
                    ->where('to_date', '>=',$toDate)
                    ->get([DB::raw('COUNT( * ) as "user_counts"')]);

                $useravailabilities_dinner = UserAvailability::Where('type', 'dinner')
                    ->where('from_date', '<=',$fromDate)
                    ->where('to_date', '>=',$toDate)
                    ->get([DB::raw('COUNT( * ) as "user_counts"')]);

        
                $displayUsers = User::all();
                $user_count = count($displayUsers);
              
        
                $count_breakfast = $useravailabilities_breakfast[0]['user_counts'];
                $count_lunch = $useravailabilities_lunch[0]['user_counts'];
                $count_snacks = $useravailabilities_snacks[0]['user_counts'];
                $count_dinner = $useravailabilities_dinner[0]['user_counts'];
                return view('admin.useravailability', array(
                    'user_count' => (int)$user_count,
                    'count_breakfast' => (int)$count_breakfast == 0 ? 0 : (int)$count_breakfast,
                    'count_lunch' => (int)$count_lunch == 0 ? 0 : (int)$count_lunch,
                    'count_snacks' => (int)$count_snacks == 0 ? 0 : (int)$count_snacks,
                    'count_dinner' => (int)$count_dinner == 0 ? 0 : (int)$count_dinner,
                    'user_opttedout' => ((int)$count_breakfast + (int)$count_lunch + (int)$count_snacks + (int)$count_dinner), 
                    'user_available' => (int)$user_count - ((int)$count_breakfast + (int)$count_lunch + (int)$count_snacks + (int)$count_dinner), 
                    'fromDate' => $fromDate,
                    'toDate' => $toDate,
                ));
            // }
        }
        else{
            $displayUsers = User::all();
            $user_count = count($displayUsers);
            return view('admin.useravailability', array(
                'user_count' => $user_count,
                'fromDate' => date('Y-m-d'),
                'toDate' => date('Y-m-d'),
                'count_breakfast' => 0,
                'count_lunch' => 0,
                'count_snacks' => 0,
                'count_dinner' => 0,
                'user_opttedout' => 0, 
                'user_available' => 0, 
            ));
        }
            // $fromDate = date('Y-m-d');
            // $toDate = date('Y-m-d');
            // $useravailabilities_breakfast = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
            //     ->Where('type', 'breakfast')
            //     ->take('1000');
            // $useravailabilities_lunch = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
            //     ->Where('type', 'lunch')
            //     ->take('1000');
            // $useravailabilities_snacks = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
            //     ->Where('type', 'snacks')
            //     ->take('1000');
            // $useravailabilities_dinner = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
            //     ->Where('type', 'dinner')
            //     ->take('1000');
    
            // $displayUsers = User::all();
            // $user_count = count($displayUsers);
    
            // $count_breakfast = count($useravailabilities_breakfast);
            // $count_lunch = count($useravailabilities_lunch);
            // $count_snacks = count($useravailabilities_snacks);
            // $count_dinner = count($useravailabilities_dinner);
    
            
    }
     /**
     * @param Request $request
     */
    public function useravailability1(Request $request)
    {
        return view('admin.useravailability', array(
                'user_count' => 100,
                'count_breakfast' => 4,
                'count_lunch' => 5,
                'count_snacks' => 2,
                'count_dinner' => 2,
            ));
        // if($request['from_date'] == ''){
        //     $fromDate = date('Y-m-d');
        //     $toDate = date('Y-m-d');
        //     $useravailabilities_breakfast = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'breakfast')
        //         ->take('1000');
        //     $useravailabilities_lunch = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'lunch')
        //         ->take('1000');
        //     $useravailabilities_snacks = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'snacks')
        //         ->take('1000');
        //     $useravailabilities_dinner = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'dinner')
        //         ->take('1000');
    
        //     $displayUsers = User::all();
        //     $user_count = count($displayUsers);
    
        //     $count_breakfast = count($useravailabilities_breakfast);
        //     $count_lunch = count($useravailabilities_lunch);
        //     $count_snacks = count($useravailabilities_snacks);
        //     $count_dinner = count($useravailabilities_dinner);
    
        //     return view('admin.useravailability', array(
        //         'user_count' => $user_count,
        //         'count_breakfast' => $count_breakfast,
        //         'count_lunch' => $count_lunch,
        //         'count_snacks' => $count_snacks,
        //         'count_dinner' => $count_dinner,
        //     ));
        // }
        // else{
        //     $fromDate = $request['from_date'];
        //     $toDate = $request['to_date'];
        //     $useravailabilities_breakfast = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'breakfast')
        //         ->take('1000');
        //     $useravailabilities_lunch = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'lunch')
        //         ->take('1000');
        //     $useravailabilities_snacks = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'snacks')
        //         ->take('1000');
        //     $useravailabilities_dinner = UserAvailability::whereBetween('from_date', [$fromDate, $toDate] )
        //         ->Where('type', 'dinner')
        //         ->take('1000');
    
        //     $displayUsers = User::all();
        //     $user_count = count($displayUsers);
    
        //     $count_breakfast = count($useravailabilities_breakfast);
        //     $count_lunch = count($useravailabilities_lunch);
        //     $count_snacks = count($useravailabilities_snacks);
        //     $count_dinner = count($useravailabilities_dinner);
    
        //     return view('admin.useravailability', array(
        //         'user_count' => $user_count,
        //         'count_breakfast' => $count_breakfast,
        //         'count_lunch' => $count_lunch,
        //         'count_snacks' => $count_snacks,
        //         'count_dinner' => $count_dinner,
        //     ));
        // }
    }

}
