<?php

namespace App\Http\Controllers;

use App\Kitchen;
use App\Orderitem;
use App\Addon;
use App\AddonCategory;
use App\Item;
use App\ItemCategory;
use App\Location;
use App\Order;
use App\PushNotify;
use App\Restaurant;
use App\RestaurantEarning;
use App\RestaurantPayout;
use App\User;
use App\Wallet;
use App\WalletTransactions;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Image;

class KitchenOwnerController extends Controller
{
    public function searchKitchen($id)
    {
            $kitchens = Kitchen::where('id', '=', $id)
                ->get();
            return response()->json($kitchens);
    }

    public function kitchens()
    {
        $user = Auth::user();
        $restaurant = $user->restaurants;
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        

        $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)->get();
        $kitchenIds=Kitchen::whereIn('restaurant_id', $restaurantIds)->select('id')->get()->toArray();
        $kitchenItemsInProgress = Orderitem::whereIn('kitchen_id',$kitchenIds)
                                ->where('item_status','=','2')
                                ->where('declined', 0)
                                ->latest('created_at')
                                ->orderBy('created_at')
                                ->paginate(10);

        $preparingOrders = Orderitem::whereIn('orderitems.kitchen_id',$kitchenIds)
                                ->select('*', DB::raw("orderitems.id AS orderitemId"))
                                ->join('orders AS oi','oi.id','=','orderitems.order_id')
                                ->where('oi.orderstatus_id','=','2')
                                ->where('declined', 0)
                                ->whereIn('orderitems.item_status', [2,3])
                                ->orderBy('orderitems.created_at','desc')
                                ->paginate(10);
        $displayUsers = User::all();
        return view('restaurantowner.kitchens', array(
            'kitchens' => $kitchens,
            'kitchenItemsInProgress' => $kitchenItemsInProgress,
            'preparingOrders' => $preparingOrders,
            'users' => $displayUsers,
            'restaurant' =>$restaurant
        ));
    }


    public function viewKitchenStatus($kitchen_id)
    {
        $user = Auth::user();
        $kitchenId=$kitchen_id;
        $restaurants = Restaurant::get();
        $kitchen = Kitchen::get();
        $kitchenItemsInProgress = Orderitem::where('kitchen_id','=',$kitchenId)
                                ->where('item_status','=','2')
                                ->where('declined', 0)
                                ->latest('created_at')
                                ->orderBy('kitchen_id')
                                ->get();


        $kitchenItemsCompleted = Orderitem::where('kitchen_id','=',$kitchenId)
                                ->where('item_status','=','3')
                                ->where('declined', 0)
                                ->latest('created_at')
                                ->orderBy('id')
                                ->get();

        $displayorder = Order::with('orderitems', 'orderitems.order_item_addons')
                            ->orderBy('id', 'DESC')
                            ->limit(2)
                            ->get();

        $orderitems = Orderitem::all();
        $displayCompletedOrder = Order::with('orderitems', 'orderitems.order_item_addons')
                                    ->orderBy('id', 'DESC')
                                    ->skip(2)
                                    ->limit(3)
                                    ->get();

        $items = Orderitem::join('orders','orders.id','=', 'orderitems.order_id')
                                ->get();

        $count = Order::with('orderitems', 'orderitems.order_item_addons')
                        ->orderBy('id', 'DESC')
                        ->limit(2)
                        ->get();

        return view('restaurantowner.viewKitchenStatus', array(
          'orders' => $displayorder,
          'items' => $items,
          'completedOrder' => $displayCompletedOrder,
          'kitchenItemsInProgress' => $kitchenItemsInProgress,
          'kitchenItemsCompleted' => $kitchenItemsCompleted,
          'orderitem' => $orderitems,
          'restaurants' => $restaurants

        ));

    }
    

    public function acceptOrderItem($itemId)
    {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $orderItem =  Orderitem::where('id',$itemId)->first();
        // dd($orderItem, $itemId);
        $orderId = $orderItem->order_id;
        
        if ($orderItem->item_status == '2') {
            $orderItem->item_status = 3;
            $orderItem->save();
            
            if (config('settings.enablePushNotificationOrders') == 'true') {
                //to user
                $notify = new PushNotify();
                $notify->sendPushNotification('2', $order->user_id);
            }
            $totalItems =  Orderitem::where('order_id',$orderId)
                                    ->where('declined', '=', 0)
                                    ->count();
            $totalItemsCompleted = Orderitem::where('order_id', $orderId)
                                            ->where('item_status','=','3')
                                            ->count();
            if($totalItems == $totalItemsCompleted) {
                $ss = Order::where('id', $orderId)
                            ->update(['orderstatus_id' => 5]);
            }
            return redirect()->back()->with(array('success' => 'Item Completed'));
        }
        else {
                return redirect()->back()->with(array('message' => 'Something went wrong.'));
        }
    }

    

    public function declineItems($itemId) {
        $user = Auth::user();
        $restaurantIds = $user->restaurants->pluck('id')->toArray();
        $orderItem =  Orderitem::where('id',$itemId)->first();
        $orderId = $orderItem->order_id;
        $order = Order::where('id', $orderId)->get();
        $userId = $order[0]->user_id;
        $orderItemPrice = $orderItem->price * $orderItem->quantity;
        $totalPrice  = Order::where('id', $orderId)
                            ->select('total')
                            ->get();
        $totalPriceAfterDeduction = $totalPrice[0]->total - $orderItemPrice;

        if ($orderItem->item_status == '0') {
            $orderItem->item_status = 5;
            $orderItem->declined = 1;
            $orderItem->save();

            $totalPriceUpdate= Order::where('id', $orderId)
                                    ->update(['total' => $totalPriceAfterDeduction]);
            
            $userDetails= Wallet::where('user_id', $userId)->first();
            $userWalletBalance = $userDetails->balance;
            $updateUserWallet  = $orderItemPrice+$userWalletBalance;
            $userDetails->balance = $updateUserWallet;
            $userDetails->save();                        
            
            $totalItems =  Orderitem::where('order_id',$orderId)->where('declined', '=', 0)->count();
            $totalItemsCompleted = Orderitem::where('order_id', $orderId)
                                            ->where('item_status','=','3')
                                            ->count();
            if($totalItems == $totalItemsCompleted) {
                $ss = Order::where('id', $orderId)
                    ->update(['orderstatus_id' => 5]);
            }
            return redirect()->back()->with(array('message' => 'Item Removed'));
        }
        else {
                return redirect()->back()->with(array('message' => 'Something went wrong.'));
        }


    }

    public function saveNewKitchen(Request $request)
    {
        $user = Auth::user();

        $kitchen = new Kitchen();

        $kitchen->name = $request->name;
        $kitchen->restaurant_id = $request->restaurant_id;

        try {
            $kitchen->save();
            return redirect()->back()->with(array('success' => 'Kitchen Saved'));
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(array('message' => 'Something went wrong. Please check your form and try again.'));
        }
      }

      public function getEditKitchen($id)
      {
          $user = Auth::user();
          $kitchen = Kitchen::where('id', $id)->first();
          if ($kitchen) {
              return view('restaurantowner.editKitchen', array(
                  'kitchen' => $kitchen,
              ));
          } else {
              return redirect()->route('restaurant.restaurants')->with(array('message' => 'Access Denied'));
          }
      }

      public function inActiveNewKitchen($id)
      {
          $kitchen = Kitchen::where('id', $id)->first();
          if ($kitchen) {
              $kitchen->toggleActive()->save();
              return redirect()->back()->with(array('success' => 'Operation Successful'));
          } else {
              return redirect()->route('kitchen.kitchens');
          }
      }
}
