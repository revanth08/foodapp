<?php

namespace App\Http\Controllers;

use App\Addon;
use App\Coupon;
use App\GpsTable;
use App\Item;
use App\Order;
use App\Orderitem;
use App\OrderItemAddon;
use App\Orderstatus;
use App\Restaurant;
use App\Wallet;
use App\WalletTransactions;
use App\User;
use Hashids;
use Illuminate\Http\Request;
use Omnipay\Omnipay;


class WalletController extends Controller
{
    /**
     * @param Request $request
     */
    public function getWalletTransactions($user_id)
    {
        $walletTransactions = Wallet::where('user_id', $user_id)->with(array('wallet_transactions' => function($query) {$query->orderBy('id', 'DESC');}))->get();
        return response()->json($walletTransactions);
    }

    public function saveWalletTransaction(Request $request)
    {
      /*  $walletTransactions = new walletTransactions();
        $walletTransactions->wallet_id = $request->wallet_id;
        $walletTransactions->order_reference = $request->order_reference;
        $walletTransactions->amount = $request->amount;
        $walletTransactions->transaction_type  = 'D';
        $walletTransactions->save();
        return response()->json($walletTransactions);
        */
        //if ($request['method'] == 'WALLET') {
                $walletDetails = Wallet::where('id', $request->w)->first();

                $orderTotal =$request->amount;
                $qrCode=json_decode($request->order_reference,true);
              //  var_dump($request->order_reference);

                $t=time();
                $unique_order_id ='';
                if($qrCode["oid"] !=''){
                    $unique_order_id = $qrCode["oid"];
                 // $unique_order_id = 'ODR' . '-' . date("m-d",$t) . '-' .strtoupper(uniqid());
                }
                else{
                  $unique_order_id = 'MCNT' . '-' . date("m-d",$t) . '-' .strtoupper(uniqid());
                }



                //write code here to check the wallet balance and deduct

                if ($walletDetails->balance >=  $orderTotal) {

                  $WalletTransactions = new WalletTransactions();

                  //save the wallet WalletTransactions
                  $WalletTransactions->wallet_id = $walletDetails->id;
                  $WalletTransactions->order_reference=$unique_order_id;
                  $WalletTransactions->amount=$orderTotal;
                  $WalletTransactions->transaction_type = 'D';
                  $WalletTransactions->save();
                  $WalletTransactionsId = $WalletTransactions->id;

                  $walletDetails->user_id= $request->u;
                  $walletDetails->balance=$walletDetails->balance-$orderTotal;
                  $walletDetails->save();

                  //$order = new Order();
                  $order = Order::where('unique_order_id', $qrCode["oid"])->first();
                  //var_dump($WalletTransactionsId);
                  if(!empty($order)){
                    //echo"asd";
                  //$order->id =$qrCode["oid"];
                  $order->orderstatus_id =1;
                  $order->transaction_id =$WalletTransactionsId;
                  $order->save();
                 }

                    $response = [
                        'success' => true,
                        'data' => $walletDetails,
                    ];

                    return response()->json($response);

                } else {

                    $response = [
                        'success' => false,
                        'data' => null,
                    ];

                    return response()->json($response);
                }

        //}
    }

    /**
     * @param Request $request
     */
    public function addWalletBalance(Request $request)
    {
        $wallet = new Wallet();
        $WalletTransactions = new WalletTransactions();

        $walletId = 0;

        try{

            $wallets = Wallet::where('user_id', $request->userId)->first();
            if(!empty($wallets)){
                $wallets->user_id= $request->userId;
                $wallets->is_active= 1;
                $wallets->balance = (float)$wallets->balance + (float)$request->walletBalance;
                $wallets->save();
                $walletId = $wallets->save();
            }
            else{
                $wallet->user_id= $request->userId;
                $wallet->is_active= 1;
                $wallet->balance = $request->walletBalance;
                $wallet->save();
                $walletId = $wallet->save();
            }

            // $fp = fopen('/home/click365com/public_html/foodappnew/app/Http/Controllers/lidn.txt', 'w');
            // fwrite($fp, $wallets);
            // fclose($fp);

            $WalletTransactions->wallet_id = $walletId;
            $WalletTransactions->order_reference = $request->comment;
            $WalletTransactions->amount = $request->walletBalance;
            $WalletTransactions->transaction_type = 'C';
            $WalletTransactions->save();

            return response()->json([
                'success' => true,
                'userName' => $request->userName
            ], 201);
        } catch (\Throwable $th) {
            // $fp = fopen('/home/click365com/public_html/foodappnew/app/Http/Controllers/lidn.txt', 'w');
            // fwrite($fp, $th);
            // fclose($fp);
        }
    }

    /**
     * @param Request $request
     */
  /*  public function payFromWallet(Request $request)
    {
      alert("df");
      if ($request['method'] == 'WALLET') {
              $walletDetails = Wallet::where('user_id', $request['user']['data']['id'])->first();

              //write code here to check the wallet balance and deduct

              if ($walletDetails->balance >=  $orderTotal) {

                $WalletTransactions = new WalletTransactions();

                //save the wallet WalletTransactions
                $WalletTransactions->wallet_id = $walletDetails->id;
                $WalletTransactions->order_reference=$unique_order_id;
                $WalletTransactions->amount=$orderTotal;
                $WalletTransactions->transaction_type = 'D';
                $WalletTransactions->save();

                $walletDetails->user_id= $newOrder->user_id;
                $walletDetails->balance=$walletDetails->balance-$orderTotal;
                $walletDetails->save();

                  $response = [
                      'success' => true,
                      'data' => $walletDetails,
                  ];

                  return response()->json($response);

              } else {

                  $response = [
                      'success' => false,
                      'data' => null,
                  ];

                  return response()->json($response);
              }

      }
    }*/


}
