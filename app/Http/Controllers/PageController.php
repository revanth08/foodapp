<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Auth;

class PageController extends Controller
{
    public function indexPage()
    {
        return redirect()->route('get.login');
    }

    public function loginPage()
    {
        if (Auth::user()) {
            if (Auth::user()->hasRole("Admin")) {
                return redirect()->route("admin.dashboard");
            }
            if (Auth::user()->hasRole("Restaurant Owner")) {
                return redirect()->route("restaurant.dashboard");
            }
        }
        return view("auth.login");
    }

    public function getPages()
    {
        $pages = Page::all();
        return response()->json($pages);
    }
}
