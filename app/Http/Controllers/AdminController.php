<?php

namespace App\Http\Controllers;

use App\Addon;
use App\AddonCategory;
use App\Item;
use App\Kitchen;
use App\ItemCategory;
use App\Location;
use App\Order;
use App\Orderstatus;
use App\Page;
use App\PromoSlider;
use App\Restaurant;
use App\RestaurantPayout;
use App\Slide;
use App\User;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Image;
use Omnipay\Omnipay;
use Spatie\Permission\Models\Role;
use App\Wallet;
class AdminController extends Controller
{

    /**
     * @return mixed
     */
    public function dashboard()
    {
        $displayUsers = User::all();
        $displayUsers = count($displayUsers);

        $displayRestaurants = Restaurant::all();
        $displayRestaurants = count($displayRestaurants);

        $displaySales = Order::where('orderstatus_id', 5)->get();
        $displaySales = count($displaySales);

        $displayEarnings = Order::where('orderstatus_id', 5)->get();
        $total = 0;
        foreach ($displayEarnings as $de) {
            $total += $de->total;
        }
        $displayEarnings = $total;

        $orders = Order::orderBy('id', 'DESC')->take(10)->get();
        $users = User::orderBy('id', 'DESC')->take(10)->get();

        $todaysDate = Carbon::now()->format('Y-m-d');

        $orderStatusesName = '[';

        $orderStatuses = Orderstatus::get(['name'])
            ->pluck('name')
            ->toArray();
        foreach ($orderStatuses as $key => $value) {
            $orderStatusesName .= "'" . $value . "' ,";
        }
        $orderStatusesName = rtrim($orderStatusesName, ' ,');
        $orderStatusesName = $orderStatusesName . ']';

        $orderStatusOrders = Order::all();

        $orderStatusOrders = $orderStatusOrders->groupBy('orderstatus_id')->map(function ($orderCount) {
            return $orderCount->count();
        });

        $orderStatusesData = '[';
        foreach ($orderStatusOrders as $key => $value) {
            if ($key == 1) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Order Placed'},";
            }
            if ($key == 2) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Preparing Order'},";
            }
            if ($key == 3) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Delivery Guy Assigned'},";
            }
            if ($key == 4) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Order Picked Up'},";
            }
            if ($key == 5) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Delivered'},";
            }
            if ($key == 6) {
                $orderStatusesData .= '{value:' . $value . ", name: 'Canceled'},";
            }
        }
        $orderStatusesData = rtrim($orderStatusesData, ',');
        $orderStatusesData .= ']';

        $ifAnyOrders = Order::all()->count();
        if ($ifAnyOrders == 0) {
            $ifAnyOrders = false;
        } else {
            $ifAnyOrders = true;
        }

        return view('admin.dashboard', array(
            'displayUsers' => $displayUsers,
            'displayRestaurants' => $displayRestaurants,
            'displaySales' => $displaySales,
            'displayEarnings' => $displayEarnings,
            'orders' => $orders,
            'users' => $users,
            'todaysDate' => $todaysDate,
            'orderStatusesName' => $orderStatusesName,
            'orderStatusesData' => $orderStatusesData,
            'ifAnyOrders' => $ifAnyOrders,
        ));
    }

    public function users()
    {
        $count = User::count();
        $users = User::orderBy('id', 'DESC')->paginate('20');
        $roles = Role::all();
        return view('admin.users', array(
            'count' => $count,
            'users' => $users,
            'roles' => $roles,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewUser(Request $request)
    {
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'delivery_pin' => strtoupper(str_random(5)),
                'password' => bcrypt($request->password),
            ]);

            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }

            $wallet = new Wallet();
            //$wallets = Wallet::where('user_id', $user->id)->first();

                $wallet->user_id= $user->id;
                $wallet->is_active= 1;
                $wallet->balance = 0;
                $wallet->save();
                $walletId = $wallet->save();

            return response()->json([
                'success' => false,
                'name' => $request->name,
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th,
            ], 401);
        }
    }
    
    /**
     * @param Request $request
     */
    public function postSearchUsers(Request $request)
    {
        $query = $request['query'];
        $users = User::where('name', 'LIKE', '%' . $query . '%')
            ->orWhere('email', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();

        $roles = Role::all();

        $count = count($users);

        return view('admin.users', array(
            'users' => $users,
            'query' => $query,
            'count' => $count,
            'roles' => $roles,
        ));
    }

    /**
     * @param $id
     */
    public function getEditUser($id)
    {
        $user = User::where('id', $id)->first();
        $roles = Role::get();
        return view('admin.editUser', array(
            'user' => $user,
            'roles' => $roles,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateUser(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        try {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            if ($request->has('password')) {
                $user->password = bcrypt($request->password);
            }
            if ($request->has('roles')) {
                $user->syncRoles($request->roles);
            }
            $user->save();
            return redirect()->back();
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    public function manageDeliveryGuys()
    {
        $users = User::role('Delivery Guy')->orderBy('id', 'DESC')->paginate();
        $count = count($users);
        return view('admin.manageDeliveryGuys', array(
            'users' => $users,
            'count' => $count,
        ));
    }

    /**
     * @param $id
     */
    public function getManageDeliveryGuysRestaurants($id)
    {
        $user = User::where('id', $id)->first();
        if ($user->hasRole('Delivery Guy')) {
            $userRestaurants = $user->restaurants;

            $allRestaurants = Restaurant::get();

            return view('admin.manageDeliveryGuysRestaurants', array(
                'user' => $user,
                'userRestaurants' => $userRestaurants,
                'allRestaurants' => $allRestaurants,
            ));
        }
    }

    /**
     * @param Request $request
     */
    public function updateDeliveryGuysRestaurants(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $user->restaurants()->sync($request->user_restaurants);
        $user->save();
        return redirect()->back()->with(['success' => 'Delivery Guy Updated']);
    }

    public function manageRestaurantOwners()
    {
        $users = User::role('Restaurant Owner')->orderBy('id', 'DESC')->paginate();
        $count = count($users);
        return view('admin.manageRestaurantOwners', array(
            'users' => $users,
            'count' => $count,
        ));
    }
    
    public function getKitchen(Request $request){
        
        
        
        $fp = fopen('/home/qwickpay/public_html/app/Http/Controllers/lidn.txt', 'w');
            fwrite($fp, $request);
            fclose($fp);
        //if ($request->ajax()) {
            $kitchens = Kitchen::whereIn('restaurant_id', $request->id)->get();
            return view('admin.tems', array(
                'success' => true,
                'kitchens' => $kitchens,
            ));
            // $response = [
            //     'success' => true,
            //     'data' => $kitchens,
            // ];
    
            // return response()->json($response);
        //}
        // $kitchens = Kitchen::whereIn('restaurant_id', $restaurantIds)
        //   ->orderBy('id', 'DESC')
        //   ->get();
        
        // $response = [
        //     'success' => true,
        //     'data' => $kitchens,
        // ];

        // return response()->json($response);
          
        //return redirect()->back()->with(['success' => 'Delivery Guy Updated']);
    }

    /**
     * @param $id
     */
    public function getManageRestaurantOwnersRestaurants($id)
    {
        $user = User::where('id', $id)->first();
        if ($user->hasRole('Restaurant Owner')) {
            $userRestaurants = $user->restaurants;

            $allRestaurants = Restaurant::get();

            return view('admin.manageDeliveryGuysRestaurants', array(
                'user' => $user,
                'userRestaurants' => $userRestaurants,
                'allRestaurants' => $allRestaurants,
            ));
        }
    }

    /**
     * @param Request $request
     */
    public function updateManageRestaurantOwnersRestaurants(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        $user->restaurants()->sync($request->user_restaurants);
        $user->save();
        return redirect()->back()->with(['success' => 'Restaurant Owner Updated']);
    }

    public function orders()
    {
        $count = Order::count();
        $orders = Order::orderBy('id', 'DESC')->with('accept_delivery.user')->paginate('20');
        // dd($orders);
        return view('admin.orders', array(
            'orders' => $orders,
            'count' => $count,
        ));
    }
    
    

    /**
     * @param Request $request
     */
    public function postSearchOrders(Request $request)
    {
        $query = $request['query'];
        $orders = Order::where('unique_order_id', 'LIKE', '%' . $query . '%')
            ->take('100')->paginate();
        $count = count($orders);
        return view('admin.orders', array(
            'orders' => $orders,
            'count' => $count,
        ));
    }

    /**
     * @param $order_id
     */
    public function viewOrder($order_id)
    {
        $order = Order::where('unique_order_id', $order_id)->first();
        // ->update(['quantity' => quantity-$oderquantity])

        if ($order) {
            return view('admin.viewOrder', array(
                'order' => $order,
                
            ));
        } else {
            return redirect()->route('admin.orders');
        }
    }

    public function sliders()
    {
        $sliders = PromoSlider::orderBy('id', 'DESC')->get();
        $count = count($sliders);
        $locations = Location::all();
        return view('admin.sliders', array(
            'sliders' => $sliders,
            'count' => $count,
            'locations' => $locations,
        ));
    }

    /**
     * @param $id
     */
    public function getEditSlider($id)
    {
        $slider = PromoSlider::where('id', $id)->first();

        if ($slider) {
            return view('admin.editSlider', array(
                'slider' => $slider,
                'slides' => $slider->slides,
            ));
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param Request $request
     */
    public function createSlider(Request $request)
    {
        $slider = new PromoSlider();
        $slider->name = $request->name;
        $slider->location_id = $request->location_id;
        $slider->save();
        return redirect()->back()->with(['success' => 'New Slider Created']);
    }

    /**
     * @param $id
     */
    public function disableSlider($id)
    {
        $slider = PromoSlider::where('id', $id)->first();
        if ($slider) {
            $slider->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param $id
     */
    public function deleteSlider($id)
    {
        $slider = PromoSlider::where('id', $id)->first();
        if ($slider) {
            $slides = $slider->slides;
            foreach ($slides as $slide) {
                $slide->delete();
            }
            $slider->delete();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param Request $request
     */
    public function saveSlide(Request $request)
    {
        $url = url('/');
        $url = substr($url, 0, strrpos($url, '/')); //this will give url without "/public"

        $slide = new Slide();
        $slide->promo_slider_id = $request->promo_slider_id;
        $slide->name = $request->name;
        $slide->url = $request->url;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();

        Image::make($image)
            ->resize(384, 384)
            ->save(base_path('assets/img/slider/' . $filename));
        $slide->image = '/assets/img/slider/' . $filename;

        Image::make($image)
            ->resize(20, 20)
            ->save(base_path('assets/img/slider/small/' . $filename_sm));
        $slide->image_placeholder = '/assets/img/slider/small/' . $filename_sm;

        $slide->save();

        return redirect()->back()->with(['success' => 'New Slide Created']);
    }

    /**
     * @param $id
     */
    public function deleteSlide($id)
    {
        $slide = Slide::where('id', $id)->first();
        if ($slide) {
            $slide->delete();
            return redirect()->back()->with(['success' => 'Deleted']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    /**
     * @param $id
     */
    public function disableSlide($id)
    {
        $slide = Slide::where('id', $id)->first();
        if ($slide) {
            $slide->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.sliders');
        }
    }

    public function restaurants()
    {
        $count = Restaurant::all()->count();
        $restaurants = Restaurant::orderBy('id', 'DESC')->where('is_accepted', '1')
        ->paginate(10);
        $locations = Location::get();

        return view('admin.restaurants', array(
            'restaurants' => $restaurants,
            'count' => $count,
            'locations' => $locations,

        ));
    }

    public function pendingAcceptance()
    {
        $count = Restaurant::all()->count();
        $restaurants = Restaurant::orderBy('id', 'DESC')->where('is_accepted', '0')->paginate(10000);
        $count = count($restaurants);
        $locations = Location::get();

        return view('admin.restaurants', array(
            'restaurants' => $restaurants,
            'count' => $count,
            'locations' => $locations,
        ));
    }

    /**
     * @param $id
     */
    public function acceptRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $restaurant->toggleAcceptance()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.restaurants');
        }
    }

    /**
     * @param Request $request
     */
    public function searchRestaurants(Request $request)
    {
        $query = $request['query'];
        $restaurants = Restaurant::where('name', 'LIKE', '%' . $query . '%')
            ->orWhere('sku', 'LIKE', '%' . $query . '%')
            ->take('1000')
            ->paginate();
        $count = count($restaurants);
        $locations = Location::get();

        return view('admin.restaurants', array(
            'restaurants' => $restaurants,
            'query' => $query,
            'count' => $count,
            'locations' => $locations,
        ));
    }

    /**
     * @param $id
     */
    public function disableRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $restaurant->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.restaurants');
        }
    }

    /**
     * @param $id
     */
    public function deleteRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        if ($restaurant) {
            $items = $restaurant->items;
            foreach ($items as $item) {
                $item->delete();
            }
            $restaurant->delete();
            return redirect()->route('admin.restaurants');
        } else {
            return redirect()->route('admin.restaurants');
        }
    }

    /**
     * @param Request $request
     */
    public function saveNewRestaurant(Request $request)
    {
        $restaurant = new Restaurant();

        $restaurant->name = $request->name;
        $restaurant->description = $request->description;
        $restaurant->location_id = $request->location_id;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(160, 117)
            ->save(base_path('assets/img/restaurants/' . $filename));
        $restaurant->image = '/assets/img/restaurants/' . $filename;
        Image::make($image)
            ->resize(20, 20)
            ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
        $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;

        $restaurant->rating = $request->rating;
        $restaurant->delivery_time = $request->delivery_time;
        $restaurant->price_range = $request->price_range;

        if ($request->is_pureveg == 'true') {
            $restaurant->is_pureveg = true;
        } else {
            $restaurant->is_pureveg = false;
        }

        if ($request->is_featured == 'true') {
            $restaurant->is_featured = true;
        } else {
            $restaurant->is_featured = false;
        }

        $restaurant->slug = str_slug($request->name) . '-' . str_random(15);
        $restaurant->certificate = $request->certificate;

        $restaurant->address = $request->address;
        $restaurant->pincode = $request->pincode;
        $restaurant->landmark = $request->landmark;
        $restaurant->latitude = $request->latitude;
        $restaurant->longitude = $request->longitude;

        $restaurant->restaurant_charges = $request->restaurant_charges;
        $restaurant->delivery_charges = $request->delivery_charges;
        $restaurant->commission_rate = $request->commission_rate;

        $restaurant->sku = time() . str_random(10);
        $restaurant->is_active = 0;
        $restaurant->is_accepted = 1;
        try {
            $restaurant->save();
            return redirect()->back()->with(['success' => 'Restaurant Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function getEditRestaurant($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        $locations = Location::get();

        return view('admin.editRestaurant', array(
            'restaurant' => $restaurant,
            'locations' => $locations,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateRestaurant(Request $request)
    {
        $restaurant = Restaurant::where('id', $request->id)->first();

        if ($restaurant) {
            $restaurant->name = $request->name;
            $restaurant->description = $request->description;
            $restaurant->location_id = $request->location_id;

            if ($request->image == null) {
                $restaurant->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(160, 117)
                    ->save(base_path('assets/img/restaurants/' . $filename));
                $restaurant->image = '/assets/img/restaurants/' . $filename;
                Image::make($image)
                    ->resize(20, 20)
                    ->save(base_path('assets/img/restaurants/small/' . $filename_sm));
                $restaurant->placeholder_image = '/assets/img/restaurants/small/' . $filename_sm;
            }

            $restaurant->rating = $request->rating;
            $restaurant->delivery_time = $request->delivery_time;
            $restaurant->price_range = $request->price_range;

            if ($request->is_pureveg == 'true') {
                $restaurant->is_pureveg = true;
            } else {
                $restaurant->is_pureveg = false;
            }

            if ($request->is_featured == 'true') {
                $restaurant->is_featured = true;
            } else {
                $restaurant->is_featured = false;
            }

            $restaurant->certificate = $request->certificate;

            $restaurant->address = $request->address;
            $restaurant->pincode = $request->pincode;
            $restaurant->landmark = $request->landmark;
            $restaurant->latitude = $request->latitude;
            $restaurant->longitude = $request->longitude;

            $restaurant->restaurant_charges = $request->restaurant_charges;
            $restaurant->delivery_charges = $request->delivery_charges;
            $restaurant->commission_rate = $request->commission_rate;

            try {
                $restaurant->save();
                return redirect()->back()->with(['success' => 'Restaurant Saved']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function items()
    {
        $count = Item::all()->count();
        $items = Item::orderBy('id', 'DESC')->paginate(20);
        $restaurants = Restaurant::all();
        $kitchens = Kitchen::all();
        /*$kitchens = Kitchen::where('restaurant_id', '4')->get();*/
        $itemCategories = ItemCategory::orderBy('id', 'DESC')->where('is_enabled', '1')->paginate(20);
        $itemCategoriesCount = count($itemCategories);
        $addonCategories = AddonCategory::all();

        return view('admin.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'itemCategoriesCount' => $itemCategoriesCount,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchItems(Request $request)
    {
        $query = $request['query'];
        $items = Item::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($items);
        $restaurants = Restaurant::get();
        $kitchens = Kitchen::get();
        $itemCategories = ItemCategory::where('is_enabled', '1')->get();
        $addonCategories = AddonCategory::all();

        return view('admin.items', array(
            'items' => $items,
            'count' => $count,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'query' => $query,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewItem(Request $request)
    {
        // dd($request->all());

        $item = new Item();

        $item->name = $request->name;
        $item->price = $request->price;
        $item->restaurant_id = $request->restaurant_id;
        $item->from_time = $request->from_time ;
        $item->to_time = $request->to_time;
        $item->kitchen_id =  isset($request->kitchen_id) ? $request->kitchen_id : 0;
        $item->item_category_id = $request->item_category_id;
        $item->quantity = $request->quantity;

        $image = $request->file('image');
        $rand_name = time() . str_random(10);
        $filename = $rand_name . '.' . $image->getClientOriginalExtension();
        $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
        Image::make($image)
            ->resize(162, 118)
            ->save(base_path('assets/img/items/' . $filename));
        $item->image = '/assets/img/items/' . $filename;
        Image::make($image)
            ->resize(25, 18)
            ->save(base_path('assets/img/items/small/' . $filename_sm));
        $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;

        if ($request->is_recommended == 'true') {
            $item->is_recommended = true;
        } else {
            $item->is_recommended = false;
        }

        if ($request->is_popular == 'true') {
            $item->is_popular = true;
        } else {
            $item->is_popular = false;
        }

        if ($request->is_new == 'true') {
            $item->is_new = true;
        } else {
            $item->is_new = false;
        }
        try {
            $item->save();
            if (isset($request->addon_category_item)) {
                $item->addon_categories()->sync($request->addon_category_item);
            }
            return redirect()->back()->with(['success' => 'Item Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }

    }

    /**
     * @param $id
     */
    public function getEditItem($id)
    {
        $item = Item::where('id', $id)->first();
        $restaurants = Restaurant::get();
        $kitchens = Kitchen::where('is_active', '1')->get();
        $itemCategories = ItemCategory::where('is_enabled', '1')->get();
        $addonCategories = AddonCategory::all();

        return view('admin.editItem', array(
            'item' => $item,
            'restaurants' => $restaurants,
            'kitchens' => $kitchens,
            'itemCategories' => $itemCategories,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param $id
     */
    public function disableItem($id)
    {
        $item = Item::where('id', $id)->first();
        if ($item) {
            $item->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.items');
        }
    }

    /**
     * @param Request $request
     */
    public function updateItem(Request $request)
    {
        $item = Item::where('id', $request->id)->first();

        if ($item) {

            $item->name = $request->name;
            $item->restaurant_id = $request->restaurant_id;
            $item->kitchen_id =  isset($request->kitchen_id) ? $request->kitchen_id : 0;
            $item->item_category_id = $request->item_category_id;
            $item->from_time = $request->from_time;
            $item->to_time = $request->to_time;
            $item->quantity = $request->quantity;
            if ($request->image == null) {
                $item->image = $request->old_image;
            } else {
                $image = $request->file('image');
                $rand_name = time() . str_random(10);
                $filename = $rand_name . '.' . $image->getClientOriginalExtension();
                $filename_sm = $rand_name . '-sm.' . $image->getClientOriginalExtension();
                Image::make($image)
                    ->resize(162, 118)
                    ->save(base_path('assets/img/items/' . $filename));
                $item->image = '/assets/img/items/' . $filename;
                Image::make($image)
                    ->resize(25, 18)
                    ->save(base_path('assets/img/items/small/' . $filename_sm));
                $item->placeholder_image = '/assets/img/items/small/' . $filename_sm;
            }

            $item->price = $request->price;

            if ($request->is_recommended == 'true') {
                $item->is_recommended = true;
            } else {
                $item->is_recommended = false;
            }

            if ($request->is_popular == 'true') {
                $item->is_popular = true;
            } else {
                $item->is_popular = false;
            }

            if ($request->is_new == 'true') {
                $item->is_new = true;
            } else {
                $item->is_new = false;
            }

            try {
                $item->save();
                if (isset($request->addon_category_item)) {
                    $item->addon_categories()->sync($request->addon_category_item);
                }
                return redirect()->back()->with(['success' => 'Item Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function addonCategories()
    {
        $count = AddonCategory::all()->count();
        $addonCategories = AddonCategory::orderBy('id', 'DESC')->paginate(20);

        return view('admin.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddonCategories(Request $request)
    {
        $query = $request['query'];
        $addonCategories = AddonCategory::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')->paginate();
        $count = count($addonCategories);

        return view('admin.addonCategories', array(
            'addonCategories' => $addonCategories,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddonCategory(Request $request)
    {
        $addonCategory = new AddonCategory();

        $addonCategory->name = $request->name;
        $addonCategory->type = $request->type;
        $addonCategory->user_id = Auth::user()->id;

        try {
            $addonCategory->save();
            return redirect()->back()->with(['success' => 'Addon Category Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function getEditAddonCategory($id)
    {
        $addonCategory = AddonCategory::where('id', $id)->first();
        return view('admin.editAddonCategory', array(
            'addonCategory' => $addonCategory,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateAddonCategory(Request $request)
    {
        $addonCategory = AddonCategory::where('id', $request->id)->first();

        if ($addonCategory) {

            $addonCategory->name = $request->name;
            $addonCategory->type = $request->type;

            try {
                $addonCategory->save();
                return redirect()->back()->with(['success' => 'Addon Category Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function addons()
    {
        $count = Addon::all()->count();
        $addons = Addon::orderBy('id', 'DESC')->paginate(20);
        $addonCategories = AddonCategory::all();

        return view('admin.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function searchAddons(Request $request)
    {
        $query = $request['query'];
        $addons = Addon::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')->paginate();
        $count = count($addons);
        $addonCategories = AddonCategory::all();

        return view('admin.addons', array(
            'addons' => $addons,
            'count' => $count,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewAddon(Request $request)
    {
        // dd($request->all());
        $addon = new Addon();

        $addon->name = $request->name;
        $addon->price = $request->price;
        $addon->user_id = Auth::user()->id;
        $addon->addon_category_id = $request->addon_category_id;

        try {
            $addon->save();
            return redirect()->back()->with(['success' => 'Addon Saved']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }

    }

    /**
     * @param $id
     */
    public function getEditAddon($id)
    {
        $addon = Addon::where('id', $id)->first();
        $addonCategories = AddonCategory::all();
        return view('admin.editAddon', array(
            'addon' => $addon,
            'addonCategories' => $addonCategories,
        ));
    }

    /**
     * @param Request $request
     */
    public function updateAddon(Request $request)
    {
        $addon = Addon::where('id', $request->id)->first();

        if ($addon) {

            $addon->name = $request->name;
            $addon->price = $request->price;
            $addon->addon_category_id = $request->addon_category_id;

            try {
                $addon->save();
                return redirect()->back()->with(['success' => 'Addon Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function itemcategories()
    {
        $itemCategories = ItemCategory::orderBy('id', 'DESC')->paginate(1000);
        $count = count($itemCategories);

        return view('admin.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
        ));
    }
    
     public function searchitemcategories(Request $request)
    {
        $query = $request['query'];
        $itemCategories = ItemCategory::where('name', 'LIKE', '%' . $query . '%')
            ->take('100')
            ->paginate();
        $count = count($itemCategories);

 

        return view('admin.itemcategories', array(
            'itemCategories' => $itemCategories,
            'count' => $count,
            'query' => $query,
        ));
    }
    
    
    
    // public function searchitemcategories(Request $request)
    // {
    //     $query = $request['query'];
    //     $items = ItemCategory::where('name', 'LIKE', '%' . $query . '%')
    //         ->take('100')
    //         ->paginate();
    //     $count = count($items);
    //     $itemCategories = ItemCategory::where('is_enabled', '1')->get();
     
    //     return view('admin.itemcategories', array(
    //         'itemCategories' => $itemCategories,
    //         'count' => $count,
    //     ));
    // }
   
    /**
     * @param Request $request
     */
    public function createItemCategory(Request $request)
    {
        $itemCategory = new ItemCategory();

        $itemCategory->name = $request->name;
        $itemCategory->user_id = Auth::user()->id;

        try {
            $itemCategory->save();
            return redirect()->back()->with(['success' => 'Category Created']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function disableCategory($id)
    {
        $itemCategory = ItemCategory::where('id', $id)->first();
        if ($itemCategory) {
            $itemCategory->toggleEnable()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.itemcategories');
        }
    }

    public function locations()
    {
        $locations = Location::orderBy('id', 'DESC')->paginate(1000);
        $locationsAll = Location::all();
        $count = count($locationsAll);
        return view('admin.locations', array(
            'locations' => $locations,
            'count' => $count,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewLocation(Request $request)
    {
        // dd($request->all());

        $location = new Location();

        $location->name = $request->name;
        $location->description = $request->description;

        if ($request->is_popular == 'true') {
            $location->is_popular = true;
        } else {
            $location->is_popular = false;
        }

        if ($request->is_active == 'true') {
            $location->is_active = true;
        } else {
            $location->is_active = false;
        }

        try {
            $location->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function disableLocation($id)
    {
        $location = Location::where('id', $id)->first();
        if ($location) {
            $location->toggleActive()->save();
            return redirect()->back()->with(['success' => 'Operation Successful']);
        } else {
            return redirect()->route('admin.locations');
        }
    }

    /**
     * @param $id
     */
    public function editLocation($id)
    {
        $location = Location::where('id', $id)->first();
        if ($location) {
            return view('admin.editLocation', array(
                'location' => $location,
            ));
        } else {
            return redirect()->route('admin.editLocation');
        }
    }

    /**
     * @param Request $request
     */
    public function updateLocation(Request $request)
    {
        $location = Location::where('id', $request->id)->first();

        if ($location) {
            $location->name = $request->name;
            $location->description = $request->description;
            if ($request->is_popular == 'true') {
                $location->is_popular = true;
            } else {
                $location->is_popular = false;
            }

            if ($request->is_active == 'true') {
                $location->is_active = true;
            } else {
                $location->is_active = false;
            }

            try {
                $location->save();
                return redirect()->back()->with(['success' => 'Operation Successful']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        }
    }

    public function pages()
    {
        $pages = Page::all();
        return view('admin.pages', array(
            'pages' => $pages,
        ));
    }

    /**
     * @param Request $request
     */
    public function saveNewPage(Request $request)
    {
        $page = new Page();
        $page->name = $request->name;
        $page->slug = $request->slug;
        $page->body = $request->body;

        try {
            $page->save();
            return redirect()->back()->with(['success' => 'New Page Created']);
        } catch (\Illuminate\Database\QueryException $qe) {
            return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
        } catch (Exception $e) {
            return redirect()->back()->with(['message' => $e->getMessage()]);
        } catch (\Throwable $th) {
            return redirect()->back()->with(['message' => $th]);
        }
    }

    /**
     * @param $id
     */
    public function getEditPage($id)
    {
        $page = Page::where('id', $id)->first();

        if ($page) {
            return view('admin.editPage', array(
                'page' => $page,
            ));
        } else {
            return redirect()->route('admin.pages');
        }
    }

    /**
     * @param Request $request
     */
    public function updatePage(Request $request)
    {
        $page = Page::where('id', $request->id)->first();

        if ($page) {
            $page->name = $request->name;
            $page->slug = $request->slug;
            $page->body = $request->body;
            try {
                $page->save();
                return redirect()->back()->with(['success' => 'Page Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }
        } else {
            return redirect()->route('admin.pages');
        }
    }

    /**
     * @param $id
     */
    public function deletePage($id)
    {
        $page = Page::where('id', $id)->first();
        if ($page) {
            $page->delete();
            return redirect()->back()->with(['success' => 'Deleted']);
        } else {
            return redirect()->route('admin.pages');
        }
    }

    public function restaurantpayouts()
    {
        $count = RestaurantPayout::all()->count();

        $restaurantPayouts = RestaurantPayout::paginate('20');

        return view('admin.restaurantPayouts', array(
            'restaurantPayouts' => $restaurantPayouts,
            'count' => $count,
        ));
    }

    /**
     * @param $id
     */
    public function viewRestaurantPayout($id)
    {
        $restaurantPayout = RestaurantPayout::where('id', $id)->first();

        if ($restaurantPayout) {
            return view('admin.viewRestaurantPayout', array(
                'restaurantPayout' => $restaurantPayout,
            ));
        }
    }

    /**
     * @param Request $request
     */
    public function updateRestaurantPayout(Request $request)
    {
        $restaurantPayout = RestaurantPayout::where('id', $request->id)->first();

        if ($restaurantPayout) {
            $restaurantPayout->status = $request->status;
            $restaurantPayout->transaction_mode = $request->transaction_mode;
            $restaurantPayout->transaction_id = $request->transaction_id;
            $restaurantPayout->message = $request->message;
            try {
                $restaurantPayout->save();
                return redirect()->back()->with(['success' => 'Restaurant Payout Updated']);
            } catch (\Illuminate\Database\QueryException $qe) {
                return redirect()->back()->with(['message' => 'Something went wrong. Please check your form and try again.']);
            } catch (Exception $e) {
                return redirect()->back()->with(['message' => $e->getMessage()]);
            } catch (\Throwable $th) {
                return redirect()->back()->with(['message' => $th]);
            }

        }
    }
}
