<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use JWTAuthException;
use Spatie\Permission\Models\Role;
use App\Wallet;
class UserController extends Controller
{
    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    private function getToken($email, $password)
    {
        $token = null;
        //$credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt(['email' => $email, 'password' => $password])) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'Password or email is invalid..',
                    'token' => $token,
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'Token creation failed',
            ]);
        }
        return $token;
    }

    /**
     * @param Request $request
     */
    public function login(Request $request)
    {
        $user = \App\User::where('email', $request->email)->get()->first();

        if ($request->has('password')) {
            if ($user && \Hash::check($request->password, $user->password)) // The passwords match...
            {
                $token = self::getToken($request->email, $request->password);
                $user->auth_token = $token;
                $user->save();
                if ($user->default_address_id !== 0) {
                    $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                } else {
                    $default_address = null;
                }

                $running_order = \App\Order::where('user_id', $user->id)->where('orderstatus_id', '1')->first();

                $response = [
                    'success' => true,
                    'data' => [
                        'id' => $user->id,
                        'auth_token' => $user->auth_token,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'default_address_id' => $user->default_address_id,
                        'default_address' => $default_address,
                        'delivery_pin' => $user->delivery_pin,
                    ],
                    'running_order' => $running_order,
                ];
            } else {
                $response = ['success' => false, 'data' => 'Record doesnt exists'];
            }
        }

        if ($request->has('accessToken')) {
            //if user exists with the email address, update the user JWT token
            if ($user) {
                $token = JWTAuth::fromUser($user);
                $user->auth_token = $token;
                $user->save();
                if ($user->default_address_id !== 0) {
                    $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                } else {
                    $default_address = null;
                }

                $running_order = \App\Order::where('user_id', $user->id)->where('orderstatus_id', '1')->first();

                $response = [
                    'success' => true,
                    'data' => [
                        'id' => $user->id,
                        'auth_token' => $user->auth_token,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'default_address_id' => $user->default_address_id,
                        'default_address' => $default_address,
                        'delivery_pin' => $user->delivery_pin,
                    ],
                    'running_order' => $running_order,
                ];
            }
            //if user doesnt exists, create the user.
            else {
                $user = new User();
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = \Hash::make(str_random(8));
                $user->delivery_pin = strtoupper(str_random(5));

                try {
                    $user->save();
                    $user->assignRole('Customer');
                    $token = JWTAuth::fromUser($user);
                    $user->auth_token = $token;
                    $user->save();

                } catch (\Throwable $e) {
                    $response = ['success' => false, 'data' => 'Something went wrong. Please try again.'];
                    return response()->json($response, 201);
                }

                if ($user->default_address_id !== 0) {
                    $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                } else {
                    $default_address = null;
                }

                $running_order = \App\Order::where('user_id', $user->id)->where('orderstatus_id', '1')->first();

                $response = [
                    'success' => true,
                    'data' => [
                        'id' => $user->id,
                        'auth_token' => $user->auth_token,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'default_address_id' => $user->default_address_id,
                        'default_address' => $default_address,
                        'delivery_pin' => $user->delivery_pin,
                    ],
                    'running_order' => $running_order,
                ];
            }
        }

        return response()->json($response, 201);
    }

    /**
     * @param Request $request
     */
    public function register(Request $request)
    {
        $payload = [
            'password' => \Hash::make($request->password),
            'email' => $request->email,
            'name' => $request->name,
            'phone' => $request->phone,
            'delivery_pin' => strtoupper(str_random(5)),
            'auth_token' => '',
        ];

        try {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6'],
                'phone' => ['required'],
            ]);

            $user = new \App\User($payload);
            if ($user->save()) {

                $token = self::getToken($request->email, $request->password); // generate user token

                if (!is_string($token)) {
                    return response()->json(['success' => false, 'data' => 'Token generation failed'], 201);
                }

                $user = \App\User::where('email', $request->email)->get()->first();

                $user->auth_token = $token; // update user token

                $user->save();
                $user->assignRole('Customer');

                $wallet = new Wallet();
                //$wallets = Wallet::where('user_id', $user->id)->first();

                    $wallet->user_id= $user->id;
                    $wallet->is_active= 1;
                    $wallet->balance = 0;
                    $wallet->save();
                    $walletId = $wallet->save();


                if ($user->default_address_id !== 0) {
                    $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
                } else {
                    $default_address = null;
                }

                $response = [
                    'success' => true,
                    'data' => [
                        'id' => $user->id,
                        'auth_token' => $user->auth_token,
                        'name' => $user->name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                        'default_address_id' => $user->default_address_id,
                        'default_address' => $default_address,
                        'delivery_pin' => $user->delivery_pin,
                    ],
                    'running_order' => null,
                ];
            } else {
                $response = ['success' => false, 'data' => 'Couldnt register user'];
            }
        } catch (\Throwable $e) {
            $response = ['success' => false, 'data' => 'Couldnt register user'];
            return response()->json($response, 201);
        }

        return response()->json($response, 201);
    }

    /**
     * @param Request $request
     */
    public function updateUserInfo(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();
        if ($user->default_address_id !== 0) {
            $default_address = \App\Address::where('id', $user->default_address_id)->get(['address', 'house', 'landmark', 'tag'])->first();
        } else {
            $default_address = null;
        }

        $running_order = \App\Order::where('user_id', $request->user_id)
            ->whereIn('orderstatus_id', ['1', '2', '3', '4'])
            ->with('restaurant')
            ->latest('id')
            ->first();

        $response = [
            'success' => true,
            'data' => [
                'id' => $user->id,
                'auth_token' => $user->auth_token,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'default_address_id' => $user->default_address_id,
                'default_address' => $default_address,
                'delivery_pin' => $user->delivery_pin,
            ],
            'running_order' => $running_order,
        ];

        return response()->json($response);
    }
}
