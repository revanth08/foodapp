<?php
namespace App\Http\Controllers;

use App\Addon;
use App\Coupon;
use App\GpsTable;
use App\Item;
use App\Order;
use App\Orderitem;
use App\OrderItemAddon;
use App\Orderstatus;
use App\Restaurant;
use App\User;
use App\Wallet;
use App\Kitchen;
use App\WalletTransactions;
use Hashids;
use Illuminate\Http\Request;
use Omnipay\Omnipay;

class OrderController extends Controller
{
    /**
     * @param Request $request
     */
    public function placeOrder(Request $request)
    {
        // dd($request->all());
        $newOrder = new Order();

        $checkingIfEmpty = Order::count();

        $lastOrder = Order::orderBy('id', 'desc')->first();

        if ($lastOrder)
        {
            $lastOrderId = $lastOrder->id;
            $newId = $lastOrderId + 1;
            $uniqueId = Hashids::encode($newId);
        }
        else
        {
            //first order
            $newId = 1;
        }

        $restaurantCode = $request['restaurantCode'];
        $houseAddress = $request['user']['data']['default_address']['house'];


        $uniqueId = Hashids::encode($newId);
        $unique_order_id = $restaurantCode . date('md') . $newId . $request['user']['data']['default_address']['house'];
        $newOrder->unique_order_id = $unique_order_id;

        $newOrder->user_id = $request['user']['data']['id'];

        $newOrder->orderstatus_id = '1';

        $newOrder->location = $request['location'];

        $full_address = $request['user']['data']['default_address']['house'] . ', ' . $request['user']['data']['default_address']['address'] . ', ' . $request['user']['data']['default_address']['landmark'];
        $newOrder->address = $full_address;

        //get restaurant charges
        $restaurant_id = $request['order'][0]['restaurant_id'];
        $restaurant = Restaurant::where('id', $restaurant_id)->first();

        $newOrder->restaurant_charge = $restaurant->restaurant_charges;
        $newOrder->delivery_charge = $restaurant->delivery_charges;

        $orderTotal = 0;
        foreach ($request['order'] as $oI)
        {
            $originalItem = Item::where('id', $oI['id'])->first();
            $orderTotal += ($originalItem->price * $oI['quantity']);

            if (isset($oI['selectedaddons']))
            {
                foreach ($oI['selectedaddons'] as $selectedaddon)
                {
                    $addon = Addon::where('id', $selectedaddon['addon_id'])->first();
                    if ($addon)
                    {
                        $orderTotal += $addon->price * $oI['quantity'];
                    }
                }
            }
        }
        $orderTotal = $orderTotal + $restaurant->restaurant_charges + $restaurant->delivery_charges;

        if ($request->coupon)
        {
            $coupon = Coupon::where('code', strtoupper($request['coupon']['code']))->first();
            if ($coupon)
            {
                $newOrder->coupon_name = $request['coupon']['code'];
                if ($coupon->discount_type == 'PERCENTAGE')
                {
                    $orderTotal = $orderTotal - (($coupon->discount / 100) * $orderTotal);
                }
                if ($coupon->discount_type == 'AMOUNT')
                {
                    $orderTotal = $orderTotal - $coupon->discount;
                }
                $coupon->count = $coupon->count + 1;
                $coupon->save();
            }
        }

        if (config('settings.taxApplicable') == 'true')
        {
            $newOrder->tax = config('settings.taxPercentage');
            $orderTotal = $orderTotal + (float)(((float)config('settings.taxPercentage') / 100) * $orderTotal);
        }

        $newOrder->total = $orderTotal;

        $newOrder->order_comment = $request['order_comment'];
        $newOrder->delivery_time = $request['delivery_time'];

        $newOrder->payment_mode = $request['method'];

        $newOrder->restaurant_id = $request['order'][0]['restaurant_id'];

        //process stripe payment
        if ($request['method'] == 'STRIPE')
        {
            //successfuly received user token
            if ($request['payment_token'])
            {
                $gateway = Omnipay::create('Stripe');
                $gateway->setApiKey(config('settings.stripeSecretKey'));

                $token = $request['payment_token']['id'];

                $response = $gateway->purchase(['amount' => $orderTotal, 'currency' => config('settings.currencyId') , 'token' => $token, 'metadata' => ['OrderId' => $unique_order_id, 'Name' => $request['user']['data']['name'], 'Email' => $request['user']['data']['email'], ], ])->send();

                if ($response->isSuccessful())
                {
                    //when success then save order
                    $newOrder->save();

                    foreach ($request['order'] as $orderItem)
                    {
                        $item = new Orderitem();
                        $item->order_id = $newOrder->id;
                        $item->item_id = $orderItem['id'];
                        $item->name = $orderItem['name'];
                        $item->quantity = $orderItem['quantity'];
                        $item->price = $orderItem['price'];
                        $item->kitchen_id = $orderItem['kitchen_id'];
                        $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                            ->select('name')
                            ->first() ['name'];
                        $item->save();

                        if (isset($orderItem['selectedaddons']))
                        {
                            foreach ($orderItem['selectedaddons'] as $selectedaddon)
                            {
                                $addon = new OrderItemAddon();
                                $addon->orderitem_id = $item->id;
                                $addon->addon_category_name = $selectedaddon['addon_category_name'];
                                $addon->addon_name = $selectedaddon['addon_name'];
                                $addon->addon_price = $selectedaddon['price'];
                                $addon->save();
                            }
                        }
                    }

                    $gps_table = new GpsTable();
                    $gps_table->order_id = $newOrder->id;
                    $gps_table->save();

                    $response = ['success' => true, 'data' => $newOrder, ];

                    return response()->json($response);

                }
                elseif ($response->isRedirect())
                {

                }
                else
                {

                    $response = ['success' => false, 'data' => null, ];

                    return response()->json($response);
                }
            }
        }

        //process paypal payment
        if ($request['method'] == 'PAYPAL' || ['method'] == 'PAYSTACK')
        {
            //successfuly received payment
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $item = new Orderitem();
                $item->order_id = $newOrder->id;
                $item->item_id = $orderItem['id'];
                $item->name = $orderItem['name'];
                $item->quantity = $orderItem['quantity'];
                $item->price = $orderItem['price'];
                $item->kitchen_id = $orderItem['kitchen_id'];
                $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                    ->select('name')
                    ->first() ['name'];
                $item->save();
                if (isset($orderItem['selectedaddons']))
                {
                    foreach ($orderItem['selectedaddons'] as $selectedaddon)
                    {
                        $addon = new OrderItemAddon();
                        $addon->orderitem_id = $item->id;
                        $addon->addon_category_name = $selectedaddon['addon_category_name'];
                        $addon->addon_name = $selectedaddon['addon_name'];
                        $addon->addon_price = $selectedaddon['price'];
                        $addon->save();
                    }
                }
            }

            $gps_table = new GpsTable();
            $gps_table->order_id = $newOrder->id;
            $gps_table->save();

            $response = ['success' => true, 'data' => $newOrder, ];

            return response()->json($response);

        }

        if ($request['method'] == 'WALLET')
        {
            $walletDetails = Wallet::where('user_id', $newOrder->user_id)
                ->first();

            //write code here to check the wallet balance and deduct
            $orderItemsIds = [];

            if ($walletDetails->balance >= $orderTotal)
            {

                //code start
                foreach ($request['order'] as $orderItem)
                {
                    $orderItemsIds[] = $orderItem['id'];
                }
                //var_dump($orderItemsIds);
                $itemsOG = Item::whereIn('id', $orderItemsIds)->get();

                foreach ($itemsOG as $itemog)
                {
                    foreach ($request['order'] as $orderItem)
                    {
                        //  echo $orderItem['quantity'];
                        //  echo $itemog->quantity ;
                        if ($itemog->id == $orderItem['id'])
                        {

                            if ($itemog->quantity >= $orderItem['quantity'])
                            {
                                // $itemsOG = Item::where('id', $orderItemsIds)->first();
                                // $itemsOG->save();
                                // $itemog->quantity =$itemog->quantity-$orderItem['quantity'];
                                $itemog['quantity'] = $itemog['quantity'] - $orderItem['quantity'];
                                //  echo $itemog->quantity ;
                                // $itemsOG->quantity=$itemog;
                                //  $itemsOG->save();
                                //  $item = new Orderitem();
                                

                                $itemog->name = $itemog['name'];
                                $itemog->quantity = $itemog['quantity'];
                                $itemog->price = $itemog['price'];
                                $itemog->kitchen_id = $itemog['kitchen_id'];
                                $itemog->save();

                                //  $itemog->quantity = $itemog->quantity-$orderItem['quantity'];
                                
                            }
                            else
                            {
                                $response = ['success' => false, 'data' => null, ];

                                return response()->json($response);
                            }
                        }
                    }

                }
                //code end
                $WalletTransactions = new WalletTransactions();

                //save the wallet WalletTransactions
                $WalletTransactions->wallet_id = $walletDetails->id;
                $WalletTransactions->order_reference = $unique_order_id;
                $WalletTransactions->amount = $orderTotal;
                $WalletTransactions->transaction_type = 'D';
                $WalletTransactions->save();

                $walletDetails->user_id = $newOrder->user_id;
                $walletDetails->balance = $walletDetails->balance - $orderTotal;
                $walletDetails->save();

                //when success then save order
                $newOrder->save();

                foreach ($request['order'] as $orderItem)
                {

                    $item = new Orderitem();
                    $item->order_id = $newOrder->id;
                    $item->item_id = $orderItem['id'];
                    $item->name = $orderItem['name'];
                    $item->quantity = $orderItem['quantity'];
                    $item->price = $orderItem['price'];
                    $item->kitchen_id = $orderItem['kitchen_id'];
                    $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                        ->select('name')
                        ->first() ['name'];
                    $item->save();

                    if (isset($orderItem['selectedaddons']))
                    {
                        foreach ($orderItem['selectedaddons'] as $selectedaddon)
                        {
                            $addon = new OrderItemAddon();
                            $addon->orderitem_id = $item->id;
                            $addon->addon_category_name = $selectedaddon['addon_category_name'];
                            $addon->addon_name = $selectedaddon['addon_name'];
                            $addon->addon_price = $selectedaddon['price'];
                            $addon->save();
                        }
                    }
                }

                $gps_table = new GpsTable();
                $gps_table->order_id = $newOrder->id;
                $gps_table->save();

                $response = ['success' => true, 'data' => $newOrder, ];

                return response()->json($response);

            }
            else
            {

                $response = ['success' => false, 'data' => null, ];

                return response()->json($response);
            }

        }

        if ($request['method'] == 'QRPAYMENT')
        {
            $newOrder->orderstatus_id = '0';
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $orderItemsIds[] = $orderItem['id'];
            }
            $itemsOG = Item::whereIn('id', $orderItemsIds)->get();
            foreach ($itemsOG as $itemog)
            {
                foreach ($request['order'] as $orderItem)
                {
                    if ($itemog->id == $orderItem['id'])
                    {
                        if ($itemog->quantity >= $orderItem['quantity'])
                        {
                            $itemog['quantity'] = $itemog['quantity'] - $orderItem['quantity'];
                            $itemog->name = $itemog['name'];
                            $itemog->quantity = $itemog['quantity'];
                            $itemog->price = $itemog['price'];
                            $itemog->kitchen_id = $itemog['kitchen_id'];
                            $itemog->save();
                        }
                        else {
                            $response = ['success' => false, 'data' => null, ];
                            return response()->json($response);
                        }
                    }
                }

            }
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $item = new Orderitem();
                $item->order_id = $newOrder->id;
                $item->item_id = $orderItem['id'];
                $item->name = $orderItem['name'];
                $item->quantity = $orderItem['quantity'];
                $item->price = $orderItem['price'];
                $item->kitchen_id = $orderItem['kitchen_id'];
                $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                    ->select('name')
                    ->first() ['name'];
                $item->save();

                if (isset($orderItem['selectedaddons']))
                {
                    foreach ($orderItem['selectedaddons'] as $selectedaddon)
                    {
                        $addon = new OrderItemAddon();
                        $addon->orderitem_id = $item->id;
                        $addon->addon_category_name = $selectedaddon['addon_category_name'];
                        $addon->addon_name = $selectedaddon['addon_name'];
                        $addon->addon_price = $selectedaddon['price'];
                        $addon->save();
                    }
                }
            }
        }
        //if new payment gateway is added, write elseif here
        else
        {
            $newOrder->orderstatus_id = '0';
            $newOrder->save();
            foreach ($request['order'] as $orderItem)
            {
                $item = new Orderitem();
                $item->order_id = $newOrder->id;
                $item->item_id = $orderItem['id'];
                $item->name = $orderItem['name'];
                $item->quantity = $orderItem['quantity'];
                $item->price = $orderItem['price'];
                $originalItem = Item::where('id', $item->item_id)
                    ->first();
                $item->kitchen_id = $originalItem->kitchen_id;
                $item->kitchen_name = Kitchen::where('id', $item->kitchen_id)
                    ->select('name')
                    ->first() ['name'];
                $item->save();
                if (isset($orderItem['selectedaddons']))
                {
                    foreach ($orderItem['selectedaddons'] as $selectedaddon)
                    {
                        $addon = new OrderItemAddon();
                        $addon->orderitem_id = $item->id;
                        $addon->addon_category_name = $selectedaddon['addon_category_name'];
                        $addon->addon_name = $selectedaddon['addon_name'];
                        $addon->addon_price = $selectedaddon['price'];
                        $addon->save();
                    }
                }
            }

            $gps_table = new GpsTable();
            $gps_table->order_id = $newOrder->id;
            $gps_table->save();

            $response = ['success' => true, 'data' => $newOrder, ];

            return response()->json($response);
        }
    }

    /**
     * @param Request $request
     */
    public function getOrders(Request $request)
    {
        $orders = Order::where('user_id', $request->user_id)
            ->with('orderitems', 'orderitems.order_item_addons')
            ->orderBy('id', 'DESC')
            ->get();
        return response()
            ->json($orders);
    }

    /**
     * @param Request $request
     */
    public function getOrderByID($order_id)
    {
        $orders = Order::where('unique_order_id', $order_id)->get();
        return response()
            ->json($orders);
    }

    /**
     * @param Request $request
     */
    public function getOrderItems(Request $request)
    {
        $items = Orderitem::where('order_id', $request->order_id)
            ->get();
        return response()
            ->json($items);

    }
}

