<?php

namespace App\Http\Controllers;

use App\Item;
use App\Location;
use App\Restaurant;
use DB;
use Illuminate\Http\Request;

// from update
class RestaurantController extends Controller
{
    /**
     * @param $location
     */
    public function getRestaurants($location)
    {
        $location = Location::where('name', $location)->first();
        $location_id = $location->id;
        $restaurants = Restaurant::where('location_id', $location_id)
            ->where('is_accepted', '1')
            ->where('is_active', '1')
            ->paginate(5);
        return response()->json($restaurants);
    }

    /**
     * @param $slug
     */
    public function getRestaurantInfo($slug)
    {
        $restaurant = Restaurant::where('slug', $slug)->first();
        return response()->json($restaurant);
    }
    /**
     * @param $id
     */
    public function getRestaurantInfoById($id)
    {
        $restaurant = Restaurant::where('id', $id)->first();
        return response()->json($restaurant);
    }

    /**
     * @param $slug
     */
    public function getRestaurantItems($slug)
    {
        
        $t=date("Hi");
        //echo $t;
        
        $restaurant = Restaurant::where('slug', $slug)->first();

        $recommended = Item::where('restaurant_id', $restaurant->id)->where('is_recommended', '1')
            ->where('is_active', '1')
            ->where('quantity', '>', 0)
            ->where('from_time', '<=', $t)
            ->where('to_time', '>=', $t)
            ->with('addon_categories', 'addon_categories.addons')
            ->get();
            
            

        // $items = Item::with('add')
        $items = Item::where('restaurant_id', $restaurant->id)
            ->join('item_categories', 'items.item_category_id', '=', 'item_categories.id')
            ->where('is_enabled', '1')
            ->where('is_active', '1')
            ->where('quantity', '>', 0)
            ->where('from_time', '<=', $t)
            ->where('to_time', '>=', $t)
            ->with('addon_categories', 'addon_categories.addons')
            ->get(array('items.*', 'item_categories.name as category_name'));

        $items = json_decode($items, true);

        $array = [];
        foreach ($items as $item) {
            $item['originalQuantity'] = $item['quantity'];
            $array[$item['category_name']][] = $item;
        }
        $recommendedArray = [];
        foreach ($recommended as $item) {
            $item['originalQuantity'] = $item['quantity'];
            $recommendedArray[] = $item;
        }
        // $fp = fopen('/home/qwickpay/public_html/app/Http/Controllers/res.txt', 'w');
        // fwrite($fp, json_encode($array));
        // fclose($fp);
        // sleep(3);
        return response()->json(array(
            'recommended' => $recommendedArray,
            'items' => $array,
        ));
    }
    /**
     * @param Request $request
     */
    public function searchRestaurants(Request $request)
    {
        $location = Location::where('name', $request->location)->first();
        $location_id = $location->id;

        $query = $request->q;
        $restaurants = Restaurant::where('name', 'LIKE', "%$query%")
            ->where('location_id', $location_id)
            ->where('is_active', '1')
            ->get();
        return response()->json($restaurants);
    }
}
