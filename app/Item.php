<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $hidden = array('created_at', 'updated_at');

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }

    public function item_category()
    {
        return $this->belongsTo('App\ItemCategory');
    }

    public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }

    public function addon_categories()
    {
        return $this->belongsToMany(AddonCategory::class);
    }

}
