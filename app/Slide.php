<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    public function promoslider()
    {
        return $this->belongsTo('App\PromoSlider');
    }

    public function toggleActive()
    {
        $this->is_active = !$this->is_active;
        return $this;
    }
}
