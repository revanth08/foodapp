import { GET_ORDERS, GET_ORDER_BY_ID } from "./actionTypes";

const initialState = {
    orders: [],
    singleOrder:[]
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_ORDERS:
            return { ...state, orders: action.payload };
        case GET_ORDER_BY_ID:
                return { ...state, singleOrder: action.payload };
        default:
            return state;
    }
}
