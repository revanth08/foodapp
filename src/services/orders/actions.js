import { GET_ORDERS, GET_ORDER_BY_ID } from "./actionTypes";
import { GET_ORDERS_URL, GET_SINGLE_ORDER_URL } from "../../configs";
import Axios from "axios";

export const getOrders = (token, user_id) => dispatch => {
    Axios.post(GET_ORDERS_URL, {
        token: token,
        user_id: user_id
    })
        .then(response => {
            const orders = response.data;
            return dispatch({ type: GET_ORDERS, payload: orders });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const getSingleOrder = (token,order_id) => dispatch => {
    Axios.post(GET_SINGLE_ORDER_URL + "/" + order_id + "?token=" + token)
        .then(response => {
            const singleOrder = response.data;
            return dispatch({ type: GET_ORDER_BY_ID, payload: singleOrder });
        })
        .catch(function(error) {
            console.log(error);
        });
};
