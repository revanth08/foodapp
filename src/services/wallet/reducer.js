import { GET_WALLET_DETAILS } from "./actionTypes";
import { GET_WALLET_ANALYTICS } from "./actionTypes";

const initialState = {
    wallets: [],
    walletAnalytics: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_WALLET_DETAILS:
            return { ...state, wallets: action.payload };
        case GET_WALLET_ANALYTICS:
          return { ...state, walletAnalytics: action.payload };
        default:
            return state;
    }
}
