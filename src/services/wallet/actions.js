import { GET_WALLET_DETAILS,SAVE_WALLET_TRANSACTION, GET_WALLET_ANALYTICS } from "./actionTypes";
import { GET_WALLET_URL ,SAVE_WALLET_URL} from "../../configs";

import Axios from "axios";


export const getWalletTransactions = (token, user_id) => dispatch => {
    Axios.post(GET_WALLET_URL + "/" + user_id + "?token=" + token)
        .then(response => {
            const wallets = response.data;
            return dispatch({ type: GET_WALLET_DETAILS, payload: wallets });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const getWalletAnalytics = (token, user_id) => dispatch => {
    Axios.post(GET_WALLET_URL + "/" + user_id + "?token=" + token + "&d="+new Date())
        .then(response => {
            const wallets = response.data;
            return dispatch({ type: GET_WALLET_ANALYTICS, payload: wallets });
        })
        .catch(function(error) {
            console.log(error);
        });
};

export const saveWalletTransaction = (token,u, w, order_reference, amount) => dispatch => {
  //console.log(saveWalletTransaction);
    Axios.post(SAVE_WALLET_URL, {
        token: token,
        u:u,
        w:w,
        order_reference: order_reference,
        amount: amount
    })
        .then(response => {
            const wallettransaction = response.data;
            return dispatch({
                type: SAVE_WALLET_TRANSACTION,
                payload: wallettransaction
            });
        })
        .catch(function(error) {
            console.log(error);
        });
};
