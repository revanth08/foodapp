import {  SAVE_AVAILABILITY } from "./actionTypes";
import {
    // GET_AVAILABILITY_URL,
    SAVE_AVAILABILITY_URL,
    // DELETE_AVAILABILITY_URL,
    // SET_DEFAULT_URL
} from "../../configs";

import Axios from "axios";

// export const getAvailability = (id, token) => dispatch => {
//     Axios.get(GET_AVAILABILITY_URL + "/" + id + "?token=" + token)
//         .then(response => {
//             const availability = response.data;
//             return dispatch({
//                 type: GET_AVAILABILITY,
//                 payload: availability
//             });
//         })
//         .catch(function(error) {
//             console.log(error);
//         });
// };

export const saveAvailability = (user_id, token, availability) => dispatch => {
    Axios.post(SAVE_AVAILABILITY_URL, {
        token: token,
        user_id: user_id,
        availability: availability
    })
        .then(response => {
            const availability = response.data;
            return dispatch({
                type: SAVE_AVAILABILITY,
                payload: availability
            });
        })
        .catch(function(error) {
            console.log(error);
        });
};
//
// export const deleteAvailability = (user_id, availability_id, token) => dispatch => {
//     Axios.post(DELETE_AVAILABILITY_URL, {
//         token: token,
//         user_id: user_id,
//         availability_id: availability_id
//     })
//         .then(response => {
//             const availability = response.data;
//             return dispatch({
//                 type: DELETE_AVAILABILITY,
//                 payload: availability
//             });
//         })
//         .catch(function(error) {
//             console.log(error);
//         });
// };

// export const setDefaultAvailability = (user_id, availability_id, token) => dispatch => {
//     Axios.post(SET_DEFAULT_URL, {
//         token: token,
//         user_id: user_id,
//         availability_id: availability_id
//     })
//         .then(response => {
//             const availability = response.data;
//             return dispatch({
//                 type: SET_DEFAULT,
//                 payload: availability
//             });
//         })
//         .catch(function(error) {
//             console.log(error);
//         });
// };
