import {  SAVE_AVAILABILITY } from "./actionTypes";

const initialState = {
    availability: []
};

export default function(state = initialState, action) {
    switch (action.type) {
        // case GET_AVAILABILITY:
        //     return { ...state, availability: action.payload };
        case SAVE_AVAILABILITY:
            return { ...state, availability: action.payload };
        // case DELETE_AVAILABILITY:
        //     return { ...state, availability: action.payload };
        // case SET_DEFAULT:
        //     return { ...state, availability: action.payload };
        default:
            return state;
    }
}
