import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";

import Footer from "../Footer";
import UserInfo from "./UserInfo";
import UserMenu from "./UserMenu";
import { getPages } from "../../../services/pages/actions";
import { getWalletTransactions, getWalletAnalytics } from "../../../services/wallet/actions";
import Logout from "./Logout";
import Meta from "../../helpers/meta";

class Account extends Component {
    componentDidMount() {
        const { user } = this.props;
        if (localStorage.getItem("storeColor") !== null) {
            if (user.success) {
                this.props.getPages();
                this.props.getWalletTransactions(user.data.auth_token, user.data.id);
                this.props.getWalletAnalytics(user.data.auth_token, user.data.id);
            }
        }
    }

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }
        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { user, pages } = this.props;
        if (!user.success) {
            return (
                //redirect to login page if not loggedin
                <Redirect to={"/login"} />
            );
        }
        return (
            <React.Fragment>
                <Meta
                    seotitle="Account"
                    seodescription={localStorage.getItem("seoMetaDescription")}
                    ogtype="website"
                    ogtitle={localStorage.getItem("seoOgTitle")}
                    ogdescription={localStorage.getItem("seoOgDescription")}
                    ogurl={window.location.href}
                    twittertitle={localStorage.getItem("seoTwitterTitle")}
                    twitterdescription={localStorage.getItem("seoTwitterDescription")}
                />
                <UserInfo user_info={user.data} />
                <UserMenu pages={pages} />
                <Logout />
                <Footer active_account={true} />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    pages: state.pages.pages,
    wallets: state.wallets.wallets
});

export default connect(
    mapStateToProps,
    { getPages,getWalletTransactions, getWalletAnalytics }
)(Account);
