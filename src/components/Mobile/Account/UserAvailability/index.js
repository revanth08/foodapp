import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";

import {  saveAvailability  } from "../../../../services/userAvailability/actions";
import BackWithSearch from "../../Elements/BackWithSearch";
import ContentLoader from "react-content-loader";

class UserAvailability extends Component {
  static contextTypes = {
      router: () => null
  };

  state = {
    datefrom: "",
    dateto: "",
    type: [],
    loading: false,
    error: false
  };

    handleInput = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    };

    handleChange = e => {
      var options = e.target.options;
      var value = [];
      for (var i = 0, l = options.length; i < l; i++) {
        if (options[i].selected) {
          value.push(options[i].value);
        }
      }
      this.setState({ type: value });
    }



    handleSubmit = event => {
      event.preventDefault();
      // if ((this.state.datefrom > this.state.dateto)) {
          this.setState({ error: false });
          const { user } = this.props;
          if (user.success) {
              this.setState({ loading: true });
              this.props.saveAvailability(user.data.id, user.data.auth_token, this.state);
              this.context.router.history.goBack();
          }
      // } else {
      //     console.log('error result', (this.state.datefrom > this.state.dateto));
      //     this.setState({ error: true });
      //     // alert(end date should be greater then start date);
      // }
    };

    render() {
        if (window.innerWidth > 768) {
            return <Redirect to="/" />;
        }

        if (localStorage.getItem("storeColor") === null) {
            return <Redirect to={"/"} />;
        }
        const { availabilty, user } = this.props;
        return (
            <React.Fragment>
                {this.state.loading ? (
                    <div className="height-100 overlay-loading">
                        <div>
                            <img
                                src="/assets/img/loading-food.gif"
                                alt={localStorage.getItem("pleaseWaitText")}
                            />
                        </div>
                    </div>
                ) : (
                    <React.Fragment>
                        <BackWithSearch
                            boxshadow={true}
                            has_title={true}
                            title='User Unavailability'
                            disbale_search={true}
                        />
                        <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent">
                        <form onSubmit={this.handleSubmit}>
                        <div className="form-group m-0 mt-30 mb-30">
                          <label className="col-12 add-date-input-label">
                              Date Range
                          </label>
                          <div className="col-md-3">From :-
                              <input
                                  type="date"
                                  name="datefrom"
                                   required={true}
                                  onChange={this.handleInput}
                                  className="form-control add-date-input"
                              />
                          </div>
                          <div className="col-md-3">To :-
                              <input
                                  type="date"
                                  name="dateto"
                                   required={true}
                                  onChange={this.handleInput}
                                  className="form-control add-date-input"
                              />
                          </div>
                          <label className="col-12 add-type-select-label" >
                              Type
                          </label>
                          <div className="col-md-9">
                          <select
                              name="type"
                                multiple={true}
                                 required={true}
                                onChange={this.handleChange}
                                className="form-control add-type-select-label"
                            >
                              <option value="breakfast">Breakfast</option>
                              <option value="lunch">Lunch</option>
                              <option value="snacks">Snacks</option>
                              <option value="dinner">Dinner</option>
                            </select>

                          </div>
                        </div>

                        <button
                            type="submit"
                            className="btn-save-userAvailability"
                            style={{ backgroundColor: localStorage.getItem("storeColor") }}

                        >
                            Save Unavailability

                        </button>

                        </form>
                        </div>
                    </React.Fragment>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user
    //availabilty: state.availabilty.availabilty
});
console.log(mapStateToProps);
export default connect(
    mapStateToProps,
    {saveAvailability }
)(UserAvailability);
