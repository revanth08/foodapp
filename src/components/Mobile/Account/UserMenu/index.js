import React, { Component } from "react";
import Collapsible from "react-collapsible";
import Popup from "reactjs-popup";

import DelayLink from "../../../helpers/delayLink";

class UserMenu extends Component {
    render() {
        const { pages } = this.props;
        return (
            <React.Fragment>
                <Collapsible
                    trigger={localStorage.getItem("accountMyAccount")}
                    transitionTime={200}
                >
                    <div className="category-list-item">
                        <DelayLink to={"/my-addresses"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-home" />
                                </div>
                                <div className="flex-auto border-0">
                                    {localStorage.getItem("accountManageAddress")}
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                    <div className="category-list-item">
                        <DelayLink to={"/my-orders"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-basket-loaded" />
                                </div>
                                <div className="flex-auto border-0">
                                    {localStorage.getItem("accountMyOrders")}
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                    <div className="category-list-item">
                        <DelayLink to={"/my-wallet"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                    Wallet Transactions
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                    <div className="category-list-item">
                        <DelayLink to={"/qrcode"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                   Scan to Pay
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                    <div className="category-list-item">
                        <DelayLink to={"/user-availabilty"} delay={200}>
                            <div className="display-flex">
                                <div className="mr-10 border-0">
                                    <i className="si si-wallet" />
                                </div>
                                <div className="flex-auto border-0">
                                   Availability
                                </div>
                                <div className="flex-auto text-right">
                                    <i className="si si-arrow-right" />
                                </div>
                            </div>
                        </DelayLink>
                    </div>
                </Collapsible>
                <Collapsible trigger={localStorage.getItem("accountHelpFaq")} transitionTime={200}>
                    {pages.map(page => (
                        <div key={page.id} className="category-list-item">
                            <Popup
                                trigger={
                                    <div className="display-flex">
                                        <div className="flex-auto border-0">{page.name}</div>
                                        <div className="flex-auto text-right">
                                            <i className="si si-arrow-right" />
                                        </div>
                                    </div>
                                }
                                modal
                                closeOnDocumentClick
                            >
                                {close => (
                                    <div className="pages-modal">
                                        <div
                                            onClick={close}
                                            className="close-modal-header text-right"
                                        >
                                            <span className="close-modal-icon">&times;</span>
                                        </div>
                                        <div
                                            className="mt-50"
                                            dangerouslySetInnerHTML={{ __html: page.body }}
                                        />
                                    </div>
                                )}
                            </Popup>
                        </div>
                    ))}
                </Collapsible>
            </React.Fragment>
        );
    }
}

export default UserMenu;
