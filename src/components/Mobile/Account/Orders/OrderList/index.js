import React, { Component } from "react";
import Moment from "react-moment";
import { formatPrice } from "../../../../helpers/formatPrice";

class OrderList extends Component {
    __getOrderStatus = id => {
        if (id === 1) {
            return "Order Placed";
        }
        if (id === 2) {
            return "Preparing Order";
        }
        if (id === 3) {
            return "Delivery Guy Assigned";
        }
        if (id === 4) {
            return "Order Picked Up";
        }
        if (id === 5) {
            return "Delivered";
        }
        if (id === 6) {
            return "Canceled";
        }
    };
    _getTotalItemCost = item => {
        let itemCost = parseFloat(item.price) * item.quantity;
        if (item.order_item_addons.length) {
            item.order_item_addons.map(addon => {
                itemCost += parseFloat(addon.addon_price) * item.quantity;
                return itemCost;
            });
        }
        return formatPrice(itemCost);
    };
    render() {
        const { order } = this.props;
        return (
            <React.Fragment>
                <div className="mb-20">
                    <div className="display-flex">
                        <div className="flex-auto">
                            <button className="mr-5 btn btn-square btn-outline-secondary min-width-125 mb-10">
                                {this.__getOrderStatus(order.orderstatus_id)}
                            </button>
                            <h6
                                className="font-w700"
                                style={{ color: localStorage.getItem("storeColor") }}
                            >
                                {order.unique_order_id}
                            </h6>
                        </div>
                        <div className="flex-auto">
                            <span className="text-muted pull-right">
                                <Moment fromNow>{order.created_at}</Moment>
                            </span>
                        </div>
                    </div>
                    {order.orderitems.map(item => (
                        <div className="display-flex pb-5" key={item.id}>
                            <span className="order-item-quantity mr-10">x{item.quantity}</span>
                            <div className="flex-auto text-left">{item.name}</div>
                            <div className="flex-auto text-right">
                                {localStorage.getItem("currencyFormat")}
                                {this._getTotalItemCost(item)}
                            </div>
                        </div>
                    ))}
                    <React.Fragment>
                        {order.coupon_name && (
                            <div className="display-flex mt-10 font-w700">
                                <React.Fragment>
                                    <div className="flex-auto">Coupon: </div>
                                    <div className="flex-auto text-right">{order.coupon_name}</div>
                                </React.Fragment>
                            </div>
                        )}

                        {order.tax && (
                            <div className="display-flex mt-10 font-w700">
                                <React.Fragment>
                                    <div className="flex-auto">Tax: </div>
                                    <div className="flex-auto text-right">{order.tax}%</div>
                                </React.Fragment>
                            </div>
                        )}
                        <div className="display-flex mt-10 font-w700">
                            <div className="flex-auto">
                                {localStorage.getItem("orderTextTotal")}
                            </div>
                            <div className="flex-auto text-right">
                                {localStorage.getItem("currencyFormat")} {order.total}
                            </div>
                        </div>
                    </React.Fragment>
                </div>
                <hr />
            </React.Fragment>
        );
    }
}

export default OrderList;
