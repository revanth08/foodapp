import React, { Component } from "react";
import Ink from "react-ink";
import { connect } from "react-redux";
import { getWalletTransactions, getWalletAnalytics } from "../../../../../services/wallet/actions";
import { updateUserInfo } from "../../../../../services/user/actions";
import { formatPrice } from "../../../../helpers/formatPrice";
import Moment from "react-moment";



import {Pie, Line} from 'react-chartjs-2';

class WalletTransactionsList extends Component {
    static contextTypes = {
        router: () => null
    };
    constructor(props) {
        super(props);

        this.state = {
          Data: {}
        }
      }

    _getTransactionFormat = item => {
        if (item.transaction_type === 'D') {
          return 'red';
        }
        else {
          return 'green';
        }

    };

    _getTransactionSign = item => {
        if (item.transaction_type === 'D') {
          return '-';
        }
        else {
          return '+';
        }

    };

    _getWalletBalanceColor (walletBalance) {
        if (walletBalance >= 0) {
          return "green";
        }
        else {
          return "red";
        }

    };

    _walletStatus = wallet => {
        if (wallet.is_active === 1) {
          return "Active";
        }
        else {
          return "InActive";
        }

    };
    getRandomColor = function() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    getRandomColorEachEmployee = function(count) {
        var data =[];
        for (var i = 0; i < count; i++) {
            data.push(this.getRandomColor());
        }
        return data;
    }
    componentDidMount() {
      const { walletGraph, user } = this.props;
      console.log(this.props.wallet);
      let labelName = [];
      let plotvalue = [];
      this.props.wallet.wallet_transactions.map(item => (
        item.transaction_type === 'D'? labelName.push(item.created_at) : null,
        item.transaction_type === 'D'? plotvalue.push(item.amount) : null
      ));
      // let l = ['Visits',  'Categories',  'Categories',  'Data 4'];
      // let c = ['10',  '2',  '0',  '4'];
          this.setState({
            Data: {
              labels: labelName ,
              datasets:[
                 {
                    label:'Spends',
                    data: plotvalue,
                    backgroundColor: this.getRandomColorEachEmployee(plotvalue.length)
                 }
              ]
           }
           });
    }
    render() {
        const { wallet, user } = this.props;
        const options = {
          animation: {
              animateScale: true
          },
            legend: {
                display: false,
                position: 'left'
            },
        };
        return (

          <React.Fragment>
            <div className="mb-20" >
                  <div className="display-flex">
                      <div className="flex-auto">
                      <h6 className="font-w500" style={{ color: this._getWalletBalanceColor(wallet.balance) }}>
                          Wallet Balance: {localStorage.getItem("currencyFormat")}{wallet.balance}
                      </h6>
                      </div>
                  </div>
                  <hr />
                  {wallet.wallet_transactions.length != 0 && (
                  <div className="display-flex pb-5" >
                      <div className="flex-auto text-left">Transaction Date</div>
                      <div className="flex-auto text-center">Reference</div>
                       <div className="flex-auto text-right">Amount</div>
                       </div>
                  )}
                  {wallet.wallet_transactions.length === 0 && (
                      <div className="text-center mt-50 font-w600 text-muted">
                      No Wallet Transactions
                          {localStorage.getItem("noWalletTransactions")}
                      </div>
                  )}
                  <hr />
                  {wallet.wallet_transactions.map(item => (
                      <div className="display-flex pb-5" key={item.id}>
                          <span className="flex-auto text-left"><Moment format="DD-MM-YYYY HH:mm:ss">{item.created_at}</Moment></span>
                          <div className="flex-auto text-left">{item.order_reference}</div>
                           <div className="flex-auto text-right" style={{ color: this._getTransactionFormat(item) }}>
                               {this._getTransactionSign(item)}{localStorage.getItem("currencyFormat")}
                               {item.amount}
                           </div>
                          <hr />
                      </div>
                  ))}
              </div>

              <Pie
                data = {this.state.Data}
                options={options} />

      </React.Fragment>

        );
    }
}
const mapStateToProps = state => ({});

export default connect(
    mapStateToProps,
    { getWalletTransactions, getWalletAnalytics }
)(WalletTransactionsList);
