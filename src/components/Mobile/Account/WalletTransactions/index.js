import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { getWalletTransactions, getWalletAnalytics } from "../../../../services/wallet/actions";
import { updateUserInfo } from "../../../../services/user/actions";
import BackWithSearch from "../../Elements/BackWithSearch";
import WalletTransactionsList from "./WalletTransactionsList";
import ContentLoader from "react-content-loader";

class WalletTransactions extends Component {
    state = {
        no_transactions: false,
        loading: false
    };

    componentDidMount() {
        const { user } = this.props;
        if (user.success) {
            this.props.getWalletTransactions(user.data.auth_token, user.data.id);
            this.props.getWalletAnalytics(user.data.auth_token, user.data.id);
            console.log('came here');
        }
    }

    componentWillReceiveProps(nextProps) {
        // const { user } = this.props;
        const { wallets } = this.props;
        if (wallets.length === 0) {
            this.setState({ no_transactions: true, loading: false });
        }

        this.setState({ loading: false });
    }


    render() {
      if (window.innerWidth > 768) {
          return <Redirect to="/" />;
      }
      const { user, wallets } = this.props;
      console.log(this.props);
      if (localStorage.getItem("storeColor") === null) {
          return <Redirect to={"/"} />;
      }
      if (!user.success) {
          return <Redirect to={"/login"} />;
      }

         return (
            <React.Fragment>
                <BackWithSearch
                    boxshadow={true}
                    has_title={true}
                    title="Wallet"
                    disbale_search={true}
                />
                <div className="block-content block-content-full bg-white pt-80 pb-80 height-100-percent" type="text/label">
                  {wallets.length === 0 && !this.state.no_transactions && (
                        <ContentLoader
                            height={600}
                            width={400}
                            speed={1.2}
                            primaryColor="#f3f3f3"
                            secondaryColor="#ecebeb"
                        >
                            <rect x="0" y="0" rx="0" ry="0" width="75" height="22" />
                            <rect x="0" y="30" rx="0" ry="0" width="350" height="18" />
                            <rect x="0" y="60" rx="0" ry="0" width="300" height="18" />
                            <rect x="0" y="90" rx="0" ry="0" width="100" height="18" />

                            <rect x="0" y={0 + 170} rx="0" ry="0" width="75" height="22" />
                            <rect x="0" y={30 + 170} rx="0" ry="0" width="350" height="18" />
                            <rect x="0" y={60 + 170} rx="0" ry="0" width="300" height="18" />
                            <rect x="0" y={90 + 170} rx="0" ry="0" width="100" height="18" />

                            <rect x="0" y={0 + 340} rx="0" ry="0" width="75" height="22" />
                            <rect x="0" y={30 + 340} rx="0" ry="0" width="350" height="18" />
                            <rect x="0" y={60 + 340} rx="0" ry="0" width="300" height="18" />
                            <rect x="0" y={90 + 340} rx="0" ry="0" width="100" height="18" />
                        </ContentLoader>
                    )}
                    {wallets.length === 0 && (
                        <div className="text-center mt-50 font-w600 text-muted">
                            {localStorage.getItem("noWalletTransactions")}
                        </div>
                    )}
                    {wallets.map(wallet => (
                        <WalletTransactionsList
                          key={wallet.id}
                          wallet={wallet}
                        />
                    ))}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user.user,
    wallets: state.wallets.wallets
});
console.log(mapStateToProps);
export default connect(
    mapStateToProps,
    { getWalletTransactions,updateUserInfo, getWalletAnalytics }
)(WalletTransactions);
