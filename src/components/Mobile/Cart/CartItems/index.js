import React, { Component } from "react";
import Ink from "react-ink";

import { formatPrice } from "../../../helpers/formatPrice";

class CartItems extends Component {
    _getItemTotal = item => {
        let addonTotal = 0;
        let sum = 0;
        if (item.selectedaddons) {
            item.selectedaddons.map(addonArray => {
                addonTotal += parseFloat(addonArray.price);
                return addonTotal;
            });
        }
        sum += item.price * item.quantity + addonTotal * item.quantity;
        sum = parseFloat(sum);
        return localStorage.getItem("currencyFormat") + formatPrice(sum);
    };
    _generateKey = pre => {
        let newkey = `${pre}_${new Date().getTime()}_${Math.random()
            .toString(36)
            .substring(2, 15) +
            Math.random()
                .toString(36)
                .substring(2, 15)}`;
        console.log(newkey);
        return newkey;
    };
    render() {
        const { addProduct, removeProduct, item } = this.props;
        return (
            <React.Fragment>
                <div className="cart-item-meta pt-15 pb-15 align-items-center">
                    <div className="cart-item-name">
                        {item.name}
                        {item.selectedaddons && (
                            <React.Fragment>
                                <br />
                                {item.selectedaddons.map((addonArray, index) => (
                                    <React.Fragment key={item.id + addonArray.addon_id}>
                                        <span style={{ color: "#adadad", fontSize: "0.8rem" }}>
                                            {(index ? ", " : "") + addonArray.addon_name}
                                        </span>
                                    </React.Fragment>
                                ))}
                            </React.Fragment>
                        )}
                    </div>
                    <div className="btn-group btn-group-sm cart-item-btn">
                        <button
                            type="button"
                            className="btn btn-add-remove"
                            style={{
                                color: localStorage.getItem("cartColor-bg")
                            }}
                            onClick={() => removeProduct(item)}
                        >
                            <span className="btn-dec">-</span>
                            <Ink duration="500" />
                        </button>
                        <button type="button" className="btn btn-quantity">
                            {item.quantity}
                        </button>
                        <button
                            type="button"
                            className="btn btn-add-remove"
                            style={{
                                color: localStorage.getItem("cartColor-bg")
                            }}
                            onClick={() => addProduct(item)}
                        >
                            <span className="btn-inc">+</span>
                            <Ink duration="500" />
                        </button>
                    </div>
                    <div className="cart-item-price">
                        <React.Fragment>{this._getItemTotal(item)}</React.Fragment>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default CartItems;
