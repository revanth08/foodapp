import React, { Component } from "react";

class OrderComment extends Component {
    state = {
        comment: "",
        deliveryTime:""
    };

    componentDidMount() {
        this.setState({ comment: localStorage.getItem("orderComment") });
        this.setState({ comment: localStorage.getItem("orderDeliveryTime") });
    }

    handleInput = event => {
        this.setState({ comment: event.target.value });
        localStorage.setItem("orderComment", event.target.value);
    };


    handleChange = event => {
      //alert(e.target.value);
      //var options = e.target.value;
      this.setState({ deliveryTime: event.target.value });
      localStorage.setItem("orderDeliveryTime", event.target.value);
    }


    render() {
        return (
            <React.Fragment>
                <input
                    className="form-control order-comment mb-20"
                    type="text"
                    placeholder={localStorage.getItem("cartSuggestionPlaceholder")}
                    onChange={this.handleInput}
                    value={this.state.comment || ""}
                />

                <div class="form-group row">
                  <label class="col-lg-3 col-form-label">Pick the delivery time:</label>
                  <div class="col-lg-9">
                  <input
                      type="time"
                      name="deliveryTime"
                      onChange={this.handleChange}
                      className="form-control"
                  />
                  </div>
              </div>
            </React.Fragment>
        );
    }
}

export default OrderComment;
