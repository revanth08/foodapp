import React, { Component } from "react";
import { connect } from "react-redux";
import { saveDb } from "../../../services/savedb/actions";
import moment from "moment";
class Logout extends Component {
    state = {
        confirm: false
    };
    componentDidMount() {
        console.log(this.props);
    }

    showConfirm = () => {
        this.setState({ confirm: true });
    };

    handleLogout = event => {
        event.preventDefault();
        const email = this.props.email;
        let time = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        const type = "logout";
        this.props.saveDb(email, time, type);
        localStorage.removeItem("logged_in");
        localStorage.removeItem("email");
        localStorage.removeItem("time");
    };

    render() {
        return (
            <React.Fragment>
                {this.state.confirm ? (
                    <div className="col-md-12 confirm-block">
                        <div className="block block-themed">
                            <div className="block-header bg-grey">
                                <h3 className="block-title text-center">
                                    <b>Are you sure?</b>
                                </h3>
                            </div>
                            <div className="block-content">
                                <button
                                    type="button"
                                    className="btn btn-hero btn-rounded btn-noborder btn-info mr-5 mb-5 float-right"
                                    onClick={this.handleLogout}
                                >
                                    <i className="si si-check mr-5" />
                                    Yes
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-hero btn-rounded btn-noborder btn-danger mr-5 mb-5"
                                >
                                    <i className="fa fa-times mr-5" />
                                    Nope
                                </button>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div className="text-center" onClick={this.showConfirm}>
                        <div className="btn btn-outline-danger btn-lg mr-5 mb-5 mt-50">
                            <i className="si si-logout mr-5" />
                            Logout
                        </div>
                    </div>
                )}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    success: state.savedb.success
});

export default connect(
    mapStateToProps,
    { saveDb }
)(Logout);
