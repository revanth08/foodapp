import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";

import { setCurrentUser } from "../../../services/login/actions";

class User extends Component {
    handleLogin = email => {
        this.props.setCurrentUser(email);
    };
    render() {
        return (
            <React.Fragment>
                <div>
                    {this.props.user.map(single => (
                        <div
                            key={single.id}
                            className="col-md-6 col-xl-3"
                            onClick={() => this.handleLogin(single.email)}
                        >
                            <NavLink
                                to={`login/${single.email}`}
                                className="block block-link-shadow block-rounded"
                            >
                                <div className="block-content block-content-full clearfix">
                                    <div className="float-right">
                                        <img
                                            className="img-avatar"
                                            src={single.avatar}
                                            alt={single.name}
                                        />
                                    </div>
                                    <div className="float-left mt-10">
                                        <div className="font-w600 mb-5">{single.name}</div>
                                        <div className="font-size-sm text-muted">
                                            {single.title}
                                        </div>
                                    </div>
                                </div>
                            </NavLink>
                        </div>
                    ))}
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    current_user: state.current_user.current_user
});

export default connect(
    mapStateToProps,
    { setCurrentUser }
)(User);
