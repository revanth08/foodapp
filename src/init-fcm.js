import * as firebase from "firebase/app";
import "firebase/messaging";

let messaging;

if (firebase.messaging.isSupported()) {
    if (process.env.REACT_APP_FIREBASE_ID !== "" || process.env.REACT_APP_FIREBASE_ID !== "") {
        const initializedFirebaseApp = firebase.initializeApp({
            messagingSenderId: process.env.REACT_APP_FIREBASE_ID
        });
        messaging = initializedFirebaseApp.messaging();
        messaging.usePublicVapidKey(process.env.REACT_APP_FIREBASE_PUBLIC_KEY);
    } else {
        const initializedFirebaseApp = firebase.initializeApp({
            messagingSenderId: "829340569792"
        });
        messaging = initializedFirebaseApp.messaging();
        messaging.usePublicVapidKey(
            "BMJ6FadiHUBCERDUWs7hRcOQ56JCvRYCFE4HGRQcKxmebB8HetD5-Y242qP2TdRD1bWefyShN7GL9gVVEDiRLms"
        );
    }
}
export default messaging;
