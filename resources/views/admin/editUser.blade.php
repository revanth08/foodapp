@extends('admin.layouts.master')
@section("title") Edit User - Dashboard
@endsection
@section('content')
<style>
    #showPassword {
    cursor: pointer;
    padding: 5px;
    border: 1px solid #E0E0E0;
    border-radius: 0.275rem;
    color: #9E9E9E;
    }
    #showPassword:hover {
    color: #616161;
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Editing</span>
                <span class="badge badge-primary badge-pill animated flipInX">"{{ $user->email }}"</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left" id="addWalletBalance"
                    data-toggle="modal" data-target="#addWalletBalanceModal">
                <b><i class="icon-gift"></i></b>
                Add Wallet Balance
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.updateUser') }}" method="POST">
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-address-book mr-2"></i> User Details
                    </legend>
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="name"
                                value="{{ $user->name }}" placeholder="Enter Full Name" required
                                autocomplete="new-name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="email"
                                value="{{ $user->email }}" placeholder="Emter Email Address" required
                                autocomplete="new-email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="phone" value="{{ $user->phone }}" 
                                placeholder="Enter Phone Number" required autocomplete="new-phone">
                        </div>
                    </div>
                    <div class="form-group row form-group-feedback form-group-feedback-right">
                        <label class="col-lg-3 col-form-label">Password:</label>
                        <div class="col-lg-9">
                            <input id="passwordInput" type="password" class="form-control form-control-lg"
                                name="password" placeholder="Enter Password (min 6 characters)"
                                autocomplete="new-password">
                        </div>
                        <div class="form-control-feedback form-control-feedback-lg">
                            <span id="showPassword"><i class="icon-unlocked2"></i> Show</span>
                        </div>
                    </div>
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-tree7 mr-2"></i> Role Management
                    </legend>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Current Role:</label>
                        <div class="col-lg-9">
                            @foreach ($user->roles as $role)
                            <span class="badge badge-flat border-grey-800 text-default text-capitalize font-size-lg">
                            {{ $role->name }}
                            </span> @endforeach
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">New Role:</label>
                        <div class="col-lg-9">
                            <select multiple="multiple" class="form-control select" data-fouc name="roles[]">
                                @foreach ($roles as $role)
                                <option value="{{ $role->name }}" class="text-capitalize">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        UPDATE
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
<div id="addWalletBalanceModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Add Wallet Balance</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="#" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Wallet Balance:</label>
                        <div class="col-lg-9">
                            <input type="number" class="form-control form-control-lg" name="walletBalance" id="walletBalance" min="0" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Comment:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="comment" id="comment" required>
                        </div>
                    </div>
                    <input type="hidden" name="userId" id="userId" value="{{ $user->id }}">
                    <input type="hidden" name="userName" id="userName" value="{{ $user->name }}">
                    <div class="text-right">
                        <button id="AddWalletBalance" type="button" class="btn btn-primary">
                        Add
                        <i class="icon-database-insert ml-1"></i></button>
                    </div>
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#showPassword").click(function (e) { 
            $("#passwordInput").attr("type", "text");
        });
        $('.select').select2({
            minimumResultsForSearch: Infinity,
            placeholder: 'Select Role/s (Old roles will be revoked and these roles will be applied)',
        });
    });
    $("#AddWalletBalance").click(function (e) { 
            let walletBalance = $("#walletBalance").val();
            let comment = $("#comment").val();
            let userId = $("#userId").val();
            let token = $("#token").val();
            let userName = $("#userName").val();
    if(walletBalance && comment !=0)
    {
            $(this).attr("disabled", "disabled").html("ADD <i class='icon-spinner9 spinner ml-1'></i>")
    
            $.ajax({
                type: "POST",
                url: "{{ route("admin.addWalletBalance") }}",
                data: {_token: token, walletBalance: walletBalance, comment: comment, userId: userId, userName: userName},
                dataType: "JSON",
                success: function (response) {
                     //if success then reset the form and "SAVE" button and show success toast or message
                    $.jGrowl('Wallet Balance Added For ' +response.userName +' is successfull.', {
                    position: 'bottom-center',
                    header: 'SUCCESS!!!',
                    theme: 'bg-dark',
                });
                $("#AddWalletBalance").removeAttr("disabled").html("ADD <i class='icon-database-insert ml-1'></i>")
                $("#walletBalance").val("");
                $("#comment").val("");
                },
                error: function (response) {
                    //else say something went wrong and show the form again 
                    $.jGrowl(response.responseJSON.message.errorInfo[2], {
                    position: 'bottom-center',
                    header: 'ERROR!!!',
                    theme: 'bg-danger',
                });
                $("#AddWalletBalance").removeAttr("disabled").html("ADD <i class='icon-database-insert ml-1'></i>")
                }
            });
    }
    else
    {
        alert('Please fill in the field')
    }
        });
</script>
@endsection