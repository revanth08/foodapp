@extends('admin.layouts.master')
@section("title") Users - Dashboard
@endsection
@section('content')
<style>
    #showPassword {
    cursor: pointer;
    padding: 5px;
    border: 1px solid #E0E0E0;
    border-radius: 0.275rem;
    color: #9E9E9E;
    }
    #showPassword:hover {
    color: #616161;
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                @if(empty($query))
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
                @else
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX mr-2">{{ $count }}</span>
                <span class="font-weight-bold mr-2">Results for "{{ $query }}"</span>
                @endif
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewUser"
                    data-toggle="modal" data-target="#addNewUserModal">
                <b><i class="icon-user-plus"></i></b>
                Add New User
                </button>
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left" id="addBulkWalletImport"
                    data-toggle="modal" data-target="#addBulkWalletImportModal">
                <b><i class="icon-database-insert"></i></b>
                Bulk CSV Upload For Wallet Topup
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <form action="{{ route('admin.post.searchUsers') }}" method="GET">
        <div class="form-group form-group-feedback form-group-feedback-right search-box">
            <input type="text" class="form-control form-control-lg search-input"
                placeholder="Search with user name or email..." name="query">
            <div class="form-control-feedback form-control-feedback-lg">
                <button class="btn btn-default" onclick="http://localhost/foodapp/public/admin/orders/searchUsers" >Search</button>
            </div>
        </div>
         <button type="submit" class="hidden">Search</button>
        @csrf
    </form>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th style="width: 20%;">Name</th>
                            <th style="width: 55%">Email</th>
                            <th style="width: 15%;">Created</th>
                            <th style="width: 10%;">Role</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at->diffForHumans() }}</td>
                            <td>
                                @foreach ($user->roles as $role)
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                {{ $role->name }}
                                </span> @endforeach
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.get.editUser', $user->id) }}"
                                    class="badge badge-primary badge-icon"> EDIT <i
                                    class="icon-database-edit2 ml-1"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<div id="addNewUserModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Add New User</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Name:</label>
                        <div class="col-lg-9">
                            <input type="text" id="newUserName" class="form-control form-control-lg" name="name"
                                placeholder="Enter Full Name" required autocomplete="new-name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email:</label>
                        <div class="col-lg-9">
                            <input type="text" id="newUserEmail" class="form-control form-control-lg" name="email"
                                placeholder="Enter Email Address" required autocomplete="new-email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Phone:</label>
                        <div class="col-lg-9">
                            <input type="text" id="newUserPhone" class="form-control form-control-lg" name="phone"
                                placeholder="Enter Phone Number" required autocomplete="new-phone">
                        </div>
                    </div>
                    <div class="form-group row form-group-feedback form-group-feedback-right">
                        <label class="col-lg-3 col-form-label">Password:</label>
                        <div class="col-lg-9">
                            <input id="newUserPassword" type="password" class="form-control form-control-lg"
                                name="password" placeholder="Enter Password (min 6 characters)" required
                                autocomplete="new-password">
                        </div>
                        <div class="form-control-feedback form-control-feedback-lg">
                            <span id="showPassword"><i class="icon-unlocked2"></i> Show</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Role:</label>
                        <div class="col-lg-9">
                            <select id="newUserRole" multiple="multiple" class="form-control select" data-fouc>
                                @foreach ($roles as $role)
                                <option value="{{ $role->name }}" class="text-capitalize">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="text-right">
                        <button id="saveNewUser" type="button" class="btn btn-primary">
                        SAVE
                        <i class="icon-database-insert ml-1"></i></button>
                    </div>
                    <input type="hidden" id="token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
    </div>
</div>
<div id="addBulkWalletImportModal" class="modal fade mt-5" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">CSV Bulk Upload for Wallet</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.walletBulkUpload') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">CSV File: </label>
                        <div class="col-lg-10">
                            <div class="uploader">
                                <!--<div class="uniform-uploader">-->
                                <!--    <input type="file" accept=".csv" name="WalletImport_csv" class="form-control-uniform form-control-lg" required="">-->
                                <!--    <span class="filename" style="user-select: none;">No file selected</span>-->
                                <!--    <span class="action btn btn-light" style="user-select: none;">Choose File</span>-->
                                <!--</div>-->
                                <input type="file" accept=".csv" name="WalletImport_csv" class="form-control-uniform form-control-lg" required>
                            </div>
                        </div>
                    </div>
                    <!--<div class="text-left">-->
                    <!--    <button type="button" class="btn btn-primary" id="downloadSampleLocationCsv">-->
                    <!--    Download Sample CSV-->
                    <!--    <i class="icon-file-download ml-1"></i>-->
                    <!--    </button>-->
                    <!--</div>-->
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        Upload
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#showPassword").click(function (e) { 
            $("#newUserPassword").attr("type", "text");
        });
    
        $('.select').select2({
            minimumResultsForSearch: Infinity,
            placeholder: 'Select Role/s (Old roles will be revoked and these roles will be applied)',
        });
        
        $("#saveNewUser").click(function (e) { 
            let token = $("#token").val();
            let name = $("#newUserName").val();
            let email = $("#newUserEmail").val();
            let phone = $("#newUserPhone").val();
            let password = $("#newUserPassword").val();
            let roles = $("#newUserRole").children("option").filter(":selected").text();
    
            $(this).attr("disabled", "disabled").html("SAVE <i class='icon-spinner9 spinner ml-1'></i>")
    
            $.ajax({
                type: "POST",
                url: "{{ route("admin.saveNewUser") }}",
                data: {_token: token, name: name, email: email, phone: phone, password: password, roles: roles},
                dataType: "JSON",
                success: function (response) {
                     //if success then reset the form and "SAVE" button and show success toast or message
                    $.jGrowl('User ' +response.name +' created successfully.', {
                    position: 'bottom-center',
                    header: 'SUCCESS!!!',
                    theme: 'bg-dark',
                });
                $("#saveNewUser").removeAttr("disabled").html("SAVE <i class='icon-database-insert ml-1'></i>")
                $("#newUserName").val("");
                $("#newUserEmail").val("");
                $("#newUserPassword").val("");
                $("#newUserRole").val("");
                },
                error: function (response) {
                    //else say something went wrong and show the form again 
                    $.jGrowl(response.responseJSON.message.errorInfo[2], {
                    position: 'bottom-center',
                    header: 'ERROR!!!',
                    theme: 'bg-danger',
                });
                $("#saveNewUser").removeAttr("disabled").html("SAVE <i class='icon-database-insert ml-1'></i>")
                }
            });
        });
    });
    
</script>
@endsection