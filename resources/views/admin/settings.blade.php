@extends('admin.layouts.master')
@section("title") Settings - Dashboard
@endsection
@section('content')
<style>
    .disable-switch {
    opacity: 0.5;
    pointer-events: none;    
    }
</style>
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">SETTINGS</span>
            </h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-12">
        <div class="card" style="min-height: 100vh;">
            <div class="card-body">
                <form action="{{ route('admin.saveSettings') }}" method="POST" enctype="multipart/form-data">
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        Save All Settings
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    <div class="d-lg-flex justify-content-lg-left">
                        <ul class="nav nav-pills flex-column mr-lg-3 wmin-lg-250 mb-lg-0">
                            <li class="nav-item">
                                <a href="#generalSettings" class="nav-link active" data-toggle="tab">
                                <i class="icon-gear mr-2"></i>
                                General
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#seoSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-zoomin3 mr-2"></i>
                                SEO
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#designSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-brush mr-2"></i>
                                Design
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#pushNotificationSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-bubble-dots4 mr-2"></i>
                                Push Notifications
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#socialLoginSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-feed2 mr-2"></i>
                                Social Login
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#mapSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-location4 mr-2"></i>
                                Maps
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#paymentGatewaySettings" class="nav-link" data-toggle="tab">
                                <i class="icon-coin-dollar mr-2"></i>
                                Payment Gateway
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#googleAnalyticsSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-graph mr-2"></i>
                                Google Analytics
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#taxSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-percent mr-2"></i>
                                Tax Settings
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#translationSettings" class="nav-link" data-toggle="tab">
                                <i class="icon-font-size2 mr-2"></i>
                                Translations
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" style="width: 100%; padding: 0 25px;">
                            <div class="tab-pane fade show active" id="generalSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Website's General Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Store Name:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="storeName"
                                            value="{{ config('settings.storeName') }}" placeholder="Enter Store Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Website URL:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="storeUrl"
                                            value="{{ config('settings.storeUrl') }}" placeholder="Enter website URL like: https://yourdomain.com">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Store Currency:</strong></label>
                                    <div class="col-lg-9">
                                        <select name="currencyId" class="form-control form-control-lg">
                                        <option value="AED" @if(config('settings.currencyId') == "AED") selected @endif>AED</option>
                                        <option value="AFN" @if(config('settings.currencyId') == "AFN") selected @endif>AFN</option>
                                        <option value="ALL" @if(config('settings.currencyId') == "ALL") selected @endif>ALL</option>
                                        <option value="AMD" @if(config('settings.currencyId') == "AMD") selected @endif>AMD</option>
                                        <option value="ANG" @if(config('settings.currencyId') == "ANG") selected @endif>ANG</option>
                                        <option value="ANG" @if(config('settings.currencyId') == "ANG") selected @endif>ANG</option>
                                        <option value="AOA" @if(config('settings.currencyId') == "AOA") selected @endif>AOA</option>
                                        <option value="ARS" @if(config('settings.currencyId') == "ARS") selected @endif>ARS</option>
                                        <option value="AUD" @if(config('settings.currencyId') == "AUD") selected @endif>AUD</option>
                                        <option value="AWG" @if(config('settings.currencyId') == "AWG") selected @endif>AWG</option>
                                        <option value="AZN" @if(config('settings.currencyId') == "AZN") selected @endif>AZN</option>
                                        <option value="BAM" @if(config('settings.currencyId') == "BAM") selected @endif>BAM</option>
                                        <option value="BBD" @if(config('settings.currencyId') == "BBD") selected @endif>BBD</option>
                                        <option value="BDT" @if(config('settings.currencyId') == "BDT") selected @endif>BDT</option>
                                        <option value="BGN" @if(config('settings.currencyId') == "BGN") selected @endif>BGN</option>
                                        <option value="BHD" @if(config('settings.currencyId') == "BHD") selected @endif>BHD</option>
                                        <option value="BIF" @if(config('settings.currencyId') == "BIF") selected @endif>BIF</option>
                                        <option value="BMD" @if(config('settings.currencyId') == "BMD") selected @endif>BMD</option>
                                        <option value="BND" @if(config('settings.currencyId') == "BND") selected @endif>BND</option>
                                        <option value="BOB" @if(config('settings.currencyId') == "BOB") selected @endif>BOB</option>
                                        <option value="BOV" @if(config('settings.currencyId') == "BOV") selected @endif>BOV</option>
                                        <option value="BRL" @if(config('settings.currencyId') == "BRL") selected @endif>BRL</option>
                                        <option value="BSD" @if(config('settings.currencyId') == "BSD") selected @endif>BSD</option>
                                        <option value="BTN" @if(config('settings.currencyId') == "BTN") selected @endif>BTN</option>
                                        <option value="BWP" @if(config('settings.currencyId') == "BWP") selected @endif>BWP</option>
                                        <option value="BYN" @if(config('settings.currencyId') == "BYN") selected @endif>BYN</option>
                                        <option value="BZD" @if(config('settings.currencyId') == "BZD") selected @endif>BZD</option>
                                        <option value="CAD" @if(config('settings.currencyId') == "CAD") selected @endif>CAD</option>
                                        <option value="CDF" @if(config('settings.currencyId') == "CDF") selected @endif>CDF</option>
                                        <option value="CHE" @if(config('settings.currencyId') == "CHE") selected @endif>CHE</option>
                                        <option value="CHF" @if(config('settings.currencyId') == "CHF") selected @endif>CHF</option>
                                        <option value="CHW" @if(config('settings.currencyId') == "CHW") selected @endif>CHW</option>
                                        <option value="CLF" @if(config('settings.currencyId') == "CLF") selected @endif>CLF</option>
                                        <option value="CLP" @if(config('settings.currencyId') == "CLP") selected @endif>CLP</option>
                                        <option value="CNY" @if(config('settings.currencyId') == "CNY") selected @endif>CNY</option>
                                        <option value="COP" @if(config('settings.currencyId') == "COP") selected @endif>COP</option>
                                        <option value="COU" @if(config('settings.currencyId') == "COU") selected @endif>COU</option>
                                        <option value="CRC" @if(config('settings.currencyId') == "CRC") selected @endif>CRC</option>
                                        <option value="CUC" @if(config('settings.currencyId') == "CUC") selected @endif>CUC</option>
                                        <option value="CVE" @if(config('settings.currencyId') == "CVE") selected @endif>CVE</option>
                                        <option value="CZK" @if(config('settings.currencyId') == "CZK") selected @endif>CZK</option>
                                        <option value="DJF" @if(config('settings.currencyId') == "DJF") selected @endif>DJF</option>
                                        <option value="DKK" @if(config('settings.currencyId') == "DKK") selected @endif>DKK</option>
                                        <option value="DOP" @if(config('settings.currencyId') == "DOP") selected @endif>DOP</option>
                                        <option value="DZD" @if(config('settings.currencyId') == "DZD") selected @endif>DZD</option>
                                        <option value="EGP" @if(config('settings.currencyId') == "EGP") selected @endif>EGP</option>
                                        <option value="ERN" @if(config('settings.currencyId') == "ERN") selected @endif>ERN</option>
                                        <option value="ETB" @if(config('settings.currencyId') == "ETB") selected @endif>ETB</option>
                                        <option value="EUR" @if(config('settings.currencyId') == "EUR") selected @endif>EUR</option>
                                        <option value="FJD" @if(config('settings.currencyId') == "FJD") selected @endif>FJD</option>
                                        <option value="FKP" @if(config('settings.currencyId') == "FKP") selected @endif>FKP</option>
                                        <option value="GBP" @if(config('settings.currencyId') == "GBP") selected @endif>GBP</option>
                                        <option value="GEL" @if(config('settings.currencyId') == "GEL") selected @endif>GEL</option>
                                        <option value="GHS" @if(config('settings.currencyId') == "GHS") selected @endif>GHS</option>
                                        <option value="GIP" @if(config('settings.currencyId') == "GIP") selected @endif>GIP</option>
                                        <option value="GMD" @if(config('settings.currencyId') == "GMD") selected @endif>GMD</option>
                                        <option value="GNF" @if(config('settings.currencyId') == "GNF") selected @endif>GNF</option>
                                        <option value="GTQ" @if(config('settings.currencyId') == "GTQ") selected @endif>GTQ</option>
                                        <option value="GYD" @if(config('settings.currencyId') == "GYD") selected @endif>GYD</option>
                                        <option value="HKD" @if(config('settings.currencyId') == "HKD") selected @endif>HKD</option>
                                        <option value="HNL" @if(config('settings.currencyId') == "HNL") selected @endif>HNL</option>
                                        <option value="HRK" @if(config('settings.currencyId') == "HRK") selected @endif>HRK</option>
                                        <option value="HTG" @if(config('settings.currencyId') == "HTG") selected @endif>HTG</option>
                                        <option value="HUF" @if(config('settings.currencyId') == "HUF") selected @endif>HUF</option>
                                        <option value="IDR" @if(config('settings.currencyId') == "IDR") selected @endif>IDR</option>
                                        <option value="ILS" @if(config('settings.currencyId') == "ILS") selected @endif>ILS</option>
                                        <option value="INR" @if(config('settings.currencyId') == "INR") selected @endif>INR</option>
                                        <option value="IQD" @if(config('settings.currencyId') == "IQD") selected @endif>IQD</option>
                                        <option value="IRR" @if(config('settings.currencyId') == "IRR") selected @endif>IRR</option>
                                        <option value="ISK" @if(config('settings.currencyId') == "ISK") selected @endif>ISK</option>
                                        <option value="JMD" @if(config('settings.currencyId') == "JMD") selected @endif>JMD</option>
                                        <option value="JOD" @if(config('settings.currencyId') == "JOD") selected @endif>JOD</option>
                                        <option value="JPY" @if(config('settings.currencyId') == "JPY") selected @endif>JPY</option>
                                        <option value="KES" @if(config('settings.currencyId') == "KES") selected @endif>KES</option>
                                        <option value="KGS" @if(config('settings.currencyId') == "KGS") selected @endif>KGS</option>
                                        <option value="KHR" @if(config('settings.currencyId') == "KHR") selected @endif>KHR</option>
                                        <option value="KMF" @if(config('settings.currencyId') == "KMF") selected @endif>KMF</option>
                                        <option value="KPW" @if(config('settings.currencyId') == "KPW") selected @endif>KPW</option>
                                        <option value="KRW" @if(config('settings.currencyId') == "KRW") selected @endif>KRW</option>
                                        <option value="KWD" @if(config('settings.currencyId') == "KWD") selected @endif>KWD</option>
                                        <option value="KYD" @if(config('settings.currencyId') == "KYD") selected @endif>KYD</option>
                                        <option value="KZT" @if(config('settings.currencyId') == "KZT") selected @endif>KZT</option>
                                        <option value="LAK" @if(config('settings.currencyId') == "LAK") selected @endif>LAK</option>
                                        <option value="LBP" @if(config('settings.currencyId') == "LBP") selected @endif>LBP</option>
                                        <option value="LKR" @if(config('settings.currencyId') == "LKR") selected @endif>LKR</option>
                                        <option value="LRD" @if(config('settings.currencyId') == "LRD") selected @endif>LRD</option>
                                        <option value="LSL" @if(config('settings.currencyId') == "LSL") selected @endif>LSL</option>
                                        <option value="LYD" @if(config('settings.currencyId') == "LYD") selected @endif>LYD</option>
                                        <option value="MAD" @if(config('settings.currencyId') == "MAD") selected @endif>MAD</option>
                                        <option value="MDL" @if(config('settings.currencyId') == "MDL") selected @endif>MDL</option>
                                        <option value="MGA" @if(config('settings.currencyId') == "MGA") selected @endif>MGA</option>
                                        <option value="MKD" @if(config('settings.currencyId') == "MKD") selected @endif>MKD</option>
                                        <option value="MMK" @if(config('settings.currencyId') == "MMK") selected @endif>MMK</option>
                                        <option value="MNT" @if(config('settings.currencyId') == "MNT") selected @endif>MNT</option>
                                        <option value="MOP" @if(config('settings.currencyId') == "MOP") selected @endif>MOP</option>
                                        <option value="MRU" @if(config('settings.currencyId') == "MRU") selected @endif>MRU</option>
                                        <option value="MUR" @if(config('settings.currencyId') == "MUR") selected @endif>MUR</option>
                                        <option value="MVR" @if(config('settings.currencyId') == "MVR") selected @endif>MVR</option>
                                        <option value="MWK" @if(config('settings.currencyId') == "MWK") selected @endif>MWK</option>
                                        <option value="MXN" @if(config('settings.currencyId') == "MXN") selected @endif>MXN</option>
                                        <option value="MXV" @if(config('settings.currencyId') == "MXV") selected @endif>MXV</option>
                                        <option value="MYR" @if(config('settings.currencyId') == "MYR") selected @endif>MYR</option>
                                        <option value="MZN" @if(config('settings.currencyId') == "MZN") selected @endif>MZN</option>
                                        <option value="NAD" @if(config('settings.currencyId') == "NAD") selected @endif>NAD</option>
                                        <option value="NGN" @if(config('settings.currencyId') == "NGN") selected @endif>NGN</option>
                                        <option value="NIO" @if(config('settings.currencyId') == "NIO") selected @endif>NIO</option>
                                        <option value="NOK" @if(config('settings.currencyId') == "NOK") selected @endif>NOK</option>
                                        <option value="NZD" @if(config('settings.currencyId') == "NZD") selected @endif>NZD</option>
                                        <option value="OMR" @if(config('settings.currencyId') == "OMR") selected @endif>OMR</option>
                                        <option value="PAB" @if(config('settings.currencyId') == "PAB") selected @endif>PAB</option>
                                        <option value="PEN" @if(config('settings.currencyId') == "PEN") selected @endif>PEN</option>
                                        <option value="PGK" @if(config('settings.currencyId') == "PGK") selected @endif>PGK</option>
                                        <option value="PHP" @if(config('settings.currencyId') == "PHP") selected @endif>PHP</option>
                                        <option value="PKR" @if(config('settings.currencyId') == "PKR") selected @endif>PKR</option>
                                        <option value="PLN" @if(config('settings.currencyId') == "PLN") selected @endif>PLN</option>
                                        <option value="PYG" @if(config('settings.currencyId') == "PYG") selected @endif>PYG</option>
                                        <option value="QAR" @if(config('settings.currencyId') == "QAR") selected @endif>QAR</option>
                                        <option value="RON" @if(config('settings.currencyId') == "RON") selected @endif>RON</option>
                                        <option value="RSD" @if(config('settings.currencyId') == "RSD") selected @endif>RSD</option>
                                        <option value="RUB" @if(config('settings.currencyId') == "RUB") selected @endif>RUB</option>
                                        <option value="RWF" @if(config('settings.currencyId') == "RWF") selected @endif>RWF</option>
                                        <option value="SAR" @if(config('settings.currencyId') == "SAR") selected @endif>SAR</option>
                                        <option value="SBD" @if(config('settings.currencyId') == "SBD") selected @endif>SBD</option>
                                        <option value="SCR" @if(config('settings.currencyId') == "SCR") selected @endif>SCR</option>
                                        <option value="SDG" @if(config('settings.currencyId') == "SDG") selected @endif>SDG</option>
                                        <option value="SEK" @if(config('settings.currencyId') == "SEK") selected @endif>SEK</option>
                                        <option value="SGD" @if(config('settings.currencyId') == "SGD") selected @endif>SGD</option>
                                        <option value="SHP" @if(config('settings.currencyId') == "SHP") selected @endif>SHP</option>
                                        <option value="SLL" @if(config('settings.currencyId') == "SLL") selected @endif>SLL</option>
                                        <option value="SOS" @if(config('settings.currencyId') == "SOS") selected @endif>SOS</option>
                                        <option value="SRD" @if(config('settings.currencyId') == "SRD") selected @endif>SRD</option>
                                        <option value="SSP" @if(config('settings.currencyId') == "SSP") selected @endif>SSP</option>
                                        <option value="STN" @if(config('settings.currencyId') == "STN") selected @endif>STN</option>
                                        <option value="SVC" @if(config('settings.currencyId') == "SVC") selected @endif>SVC</option>
                                        <option value="SYP" @if(config('settings.currencyId') == "SYP") selected @endif>SYP</option>
                                        <option value="SZL" @if(config('settings.currencyId') == "SZL") selected @endif>SZL</option>
                                        <option value="THB" @if(config('settings.currencyId') == "THB") selected @endif>THB</option>
                                        <option value="TJS" @if(config('settings.currencyId') == "TJS") selected @endif>TJS</option>
                                        <option value="TMT" @if(config('settings.currencyId') == "TMT") selected @endif>TMT</option>
                                        <option value="TND" @if(config('settings.currencyId') == "TND") selected @endif>TND</option>
                                        <option value="TOP" @if(config('settings.currencyId') == "TOP") selected @endif>TOP</option>
                                        <option value="TRY" @if(config('settings.currencyId') == "TRY") selected @endif>TRY</option>
                                        <option value="TTD" @if(config('settings.currencyId') == "TTD") selected @endif>TTD</option>
                                        <option value="TWD" @if(config('settings.currencyId') == "TWD") selected @endif>TWD</option>
                                        <option value="TZS" @if(config('settings.currencyId') == "TZS") selected @endif>TZS</option>
                                        <option value="UAH" @if(config('settings.currencyId') == "UAH") selected @endif>UAH</option>
                                        <option value="UGX" @if(config('settings.currencyId') == "UGX") selected @endif>UGX</option>
                                        <option value="USD" @if(config('settings.currencyId') == "USD") selected @endif>USD</option>
                                        <option value="USN" @if(config('settings.currencyId') == "USN") selected @endif>USN</option>
                                        <option value="UYI" @if(config('settings.currencyId') == "UYI") selected @endif>UYI</option>
                                        <option value="UYU" @if(config('settings.currencyId') == "UYU") selected @endif>UYU</option>
                                        <option value="UZS" @if(config('settings.currencyId') == "UZS") selected @endif>UZS</option>
                                        <option value="VEF" @if(config('settings.currencyId') == "VEF") selected @endif>VEF</option>
                                        <option value="VND" @if(config('settings.currencyId') == "VND") selected @endif>VND</option>
                                        <option value="VUV" @if(config('settings.currencyId') == "VUV") selected @endif>VUV</option>
                                        <option value="WST" @if(config('settings.currencyId') == "WST") selected @endif>WST</option>
                                        <option value="XAF" @if(config('settings.currencyId') == "XAF") selected @endif>XAF</option>
                                        <option value="XCD" @if(config('settings.currencyId') == "XCD") selected @endif>XCD</option>
                                        <option value="XDR" @if(config('settings.currencyId') == "XDR") selected @endif>XDR</option>
                                        <option value="XOF" @if(config('settings.currencyId') == "XOF") selected @endif>XOF</option>
                                        <option value="XPF" @if(config('settings.currencyId') == "XPF") selected @endif>XPF</option>
                                        <option value="XSU" @if(config('settings.currencyId') == "XSU") selected @endif>XSU</option>
                                        <option value="XUA" @if(config('settings.currencyId') == "XUA") selected @endif>XUA</option>
                                        <option value="YER" @if(config('settings.currencyId') == "YER") selected @endif>YER</option>
                                        <option value="ZAR" @if(config('settings.currencyId') == "ZAR") selected @endif>ZAR</option>
                                        <option value="ZMW" @if(config('settings.currencyId') == "ZMW") selected @endif>ZMW</option>
                                        <option value="ZWL" @if(config('settings.currencyId') == "ZWL") selected @endif>ZWL</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Currency Symbol:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="currencyFormat"
                                            value="{{ config('settings.currencyFormat') }}" placeholder="Currency Symbol like  $ or €">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if(config('settings.storeLogo') !== NULL)
                                    <div class="col-lg-9 offset-lg-3">
                                        <img src="{{ substr(url('/'), 0, strrpos(url('/'), '/')) }}/assets/img/logos/{{ config('settings.storeLogo') }}" alt="logo" class="img-fluid mb-2" style="width: 135px;">
                                    </div>
                                    @endif
                                    <label class="col-lg-3 col-form-label"><strong>Logo: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control-uniform" name="logo" data-fouc>
                                        <span class="help-text text-muted">Image size 320x89</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if(config('settings.favicon-32x32') !== NULL)
                                    <div class="col-lg-9 offset-lg-3">
                                        <img src="{{ substr(url('/'), 0, strrpos(url('/'), '/')) }}/assets/img/favicons/{{ config('settings.favicon-96x96') }}" alt="favicon-96x96" class="img-fluid mb-2" style="width: 70px;">
                                    </div>
                                    @endif
                                    <label class="col-lg-3 col-form-label"><strong>Favicon: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control-uniform" name="favicon" data-fouc>
                                        <span class="help-text text-muted">Square Image of min size: 512x512</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if(config('settings.splashLogo') !== NULL)
                                    <div class="col-lg-9 offset-lg-3">
                                        <img src="{{ substr(url('/'), 0, strrpos(url('/'), '/')) }}/assets/img/splash/{{ config('settings.splashLogo') }}" alt="splash screen" class="img-fluid mb-2" style="width: 30%;">
                                    </div>
                                    @endif
                                    <label class="col-lg-3 col-form-label"><strong>Splash Screen: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control-uniform" name="splashLogo" data-fouc>
                                        <span class="help-text text-muted">Image size 480x853</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if(config('settings.firstScreenHeroImage') !== NULL)
                                    <div class="col-lg-9 offset-lg-3">
                                        <img src="{{ substr(url('/'), 0, strrpos(url('/'), '/')) }}/{{ config('settings.firstScreenHeroImage') }}" alt="Hero Image" class="img-fluid mb-2" style="width: 30%;">
                                    </div>
                                    @endif
                                    <label class="col-lg-3 col-form-label"><strong>Hero Image: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control-uniform" name="firstScreenHeroImage" data-fouc>
                                        <span class="help-text text-muted">Image size 592x640</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Show Promo Slider? </strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.showPromoSlider') == "true") checked="checked" @endif name="showPromoSlider">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Show GDPR Checkbox? </strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.showGdpr') == "true") checked="checked" @endif name="showGdpr">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Minimum Payout for Restaurant in {{ config('settings.currencyFormat') }}: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="minPayout"
                                            value="{{ config('settings.minPayout') }}" placeholder="Minimum Payout for Restaurant in {{ config('settings.currencyFormat') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="seoSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    SEO Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Meta Title: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="seoMetaTitle"
                                            value="{{ config('settings.seoMetaTitle') }}" placeholder="Meta Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Meta Description: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="seoMetaDescription"
                                            value="{{ config('settings.seoMetaDescription') }}" placeholder="Meta Description">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Open Graph Title: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="seoOgTitle"
                                            value="{{ config('settings.seoOgTitle') }}" placeholder="Open Graph Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Open Graph Description: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="seoOgDescription"
                                            value="{{ config('settings.seoOgDescription') }}" placeholder="Open Graph Meta Description">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if(config('settings.seoOgImage') !== NULL)
                                    <div class="col-lg-9 offset-lg-3">
                                        <img src="{{ substr(url('/'), 0, strrpos(url('/'), '/')) }}/assets/img/social/{{ config('settings.seoOgImage') }}" alt="Open Graph Image" class="img-fluid mb-2" style="width: 30%;">
                                    </div>
                                    @endif
                                    <label class="col-lg-3 col-form-label"><strong>Open Graph Image: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control-uniform" name="seoOgImage" data-fouc>
                                        <span class="help-text text-muted">Image size 1200x630 </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Twitter Cards Title: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="seoTwitterTitle"
                                            value="{{ config('settings.seoTwitterTitle') }}" placeholder="Twitter Cards Description">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Twitter Cards Description</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="seoTwitterDescription"
                                            value="{{ config('settings.seoTwitterDescription') }}" placeholder="Twitter Cards Description">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    @if(config('settings.seoTwitterImage') !== NULL)
                                    <div class="col-lg-9 offset-lg-3">
                                        <img src="{{ substr(url('/'), 0, strrpos(url('/'), '/')) }}/assets/img/social/{{ config('settings.seoTwitterImage') }}" alt="Twitter Image" class="img-fluid mb-2" style="width: 30%;">
                                    </div>
                                    @endif
                                    <label class="col-lg-3 col-form-label"><strong>Twitter Cards Image: </strong></label>
                                    <div class="col-lg-9">
                                        <input type="file" class="form-control-uniform" name="seoTwitterImage" data-fouc>
                                        <span class="help-text text-muted">Image size 600x335</span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="designSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Design Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Store Color:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control colorpicker-show-input" name="storeColor" data-preferred-format="rgb" value="{{ config('settings.storeColor') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Background Color:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control colorpicker-show-input" name="cartColorBg" data-preferred-format="rgb" value="{{ config('settings.cartColorBg') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Text Color:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control colorpicker-show-input" name="cartColorText" data-preferred-format="rgb" value="{{ config('settings.cartColorText') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>New Item Badge Color:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control colorpicker-show-input" name="newBadgeColor" data-preferred-format="rgb" value="{{ config('settings.newBadgeColor') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Popular Item Badge Color:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control colorpicker-show-input" name="popularBadgeColor" data-preferred-format="rgb" value="{{ config('settings.popularBadgeColor') }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Recommended Item Badge Color:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control colorpicker-show-input" name="recommendedBadgeColor" data-preferred-format="rgb" value="{{ config('settings.recommendedBadgeColor') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pushNotificationSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Push Notification Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Enable Push Notifications</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.enablePushNotification') == "true") checked="checked" @endif name="enablePushNotification">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Push Notifications for Order Updates</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.enablePushNotificationOrders') == "true") checked="checked" @endif name="enablePushNotificationOrders">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Firebase Sender ID:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firebaseSenderId"
                                            value="{{ config('settings.firebaseSenderId') }}" placeholder="Enter Firebase Sender ID">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Firebase FCM Public Key (Certificate):</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firebasePublic"
                                            value="{{ config('settings.firebasePublic') }}" placeholder="Enter Firebase Public Key (Certificate)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Firebase FCM Secret:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firebaseSecret"
                                            value="{{ config('settings.firebaseSecret') }}" placeholder="Enter Firebase Secret">
                                        <span class="help-text text-muted"><a href="https://account.mapbox.com/auth/signup" target="_blank" rel="nofollow">Click Here</a> to get your Firebase Credentials. <br>Read the docs to know more about push notifications.</span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="socialLoginSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Social Login Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Enable Facebook Login</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.enableFacebookLogin') == "true") checked="checked" @endif name="enableFacebookLogin">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Facebook App ID:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="facebookAppId"
                                            value="{{ config('settings.facebookAppId') }}" placeholder="Enter Facebook App ID">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Facebook Login Button Text:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="facebookLoginButtonText"
                                            value="{{ config('settings.facebookLoginButtonText') }}" placeholder="Facebook Login Button Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Enable Google Login</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.enableGoogleLogin') == "true") checked="checked" @endif name="enableGoogleLogin">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Google App ID:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="googleAppId"
                                            value="{{ config('settings.googleAppId') }}" placeholder="Enter Google App ID">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Google Login Button Text:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="googleLoginButtonText"
                                            value="{{ config('settings.googleLoginButtonText') }}" placeholder="Google Login Button Text">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="mapSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Map Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Show Map</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.showMap') == "true") checked="checked" @endif name="showMap">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Map API Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="mapApiKey"
                                            value="{{ config('settings.mapApiKey') }}" placeholder="Enter MapBox API Key">
                                        <span class="help-text text-muted"><a href="https://account.mapbox.com/auth/signup" target="_blank" rel="nofollow">Click Here</a> to get your Map API key from MapBox</span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="paymentGatewaySettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Payment Gateway Settings
                                </legend>
                                @php
                                $activePaymentGatewayCount = count($activePaymentGateways);
                                @endphp
                                @foreach($paymentGateways as $paymentGateway)
                                <div class="form-group row" id="paymentGatewaysData">
                                    <label class="col-lg-5 col-form-label"><strong>{{ $paymentGateway->name }} </strong>({{ $paymentGateway->description }})</label>
                                    <div class="col-lg-6 mt-2">
                                        <label>
                                        <input value="{{ $paymentGateway->id }}" type="checkbox" class="switchery-primary payment-gateway-switch"
                                        @if($paymentGateway->is_active && $activePaymentGatewayCount == 1)
                                        checked="checked" 
                                        disabled="disabled"
                                        @endif 
                                        @if($paymentGateway->is_active)
                                        checked="checked"
                                        @endif  
                                        name="{{ $paymentGateway->name }}">
                                        </label>
                                    </div>
                                </div>
                                @endforeach
                                <hr>
                                <h2>Stripe</h2>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Stripe Public Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="stripePublicKey"
                                            value="{{ config('settings.stripePublicKey') }}" placeholder="Stripe Public Key (Leave blank if not using Stripe)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Stripe Secret Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="stripeSecretKey"
                                            value="{{ config('settings.stripeSecretKey') }}" placeholder="Stripe Secret Key (Leave blank if not using Stripe)">
                                    </div>
                                </div>
                                <hr>
                                <h2>PayPal</h2>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Paypal Environment:</strong></label>
                                    <div class="col-lg-9">
                                        <select name="paypalEnv" class="form-control form-control-lg">
                                        <option value="sandbox" @if(config('settings.paypalEnv') == "sandbox") selected @endif>Sandbox (Testing)</option>
                                        <option value="production" @if(config('settings.paypalEnv') == "production") selected @endif>Production (Live)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Paypal Sandbox Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="paypalSandboxKey"
                                            value="{{ config('settings.paypalSandboxKey') }}" placeholder="Paypal Sandbox Client Key (Leave blank if not using PayPal)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Paypal Production Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="paypalProductionKey"
                                            value="{{ config('settings.paypalProductionKey') }}" placeholder="Paypal Production Client Key (Leave blank if not using PayPal)">
                                    </div>
                                </div>
                                <hr>
                                <h2>PayStack</h2>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>PayStack Public Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="paystackPublicKey"
                                            value="{{ config('settings.paystackPublicKey') }}" placeholder="PayStack Public Key (Leave blank if not using PayStack)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>PayStack Private Key:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="paystackPrivateKey"
                                            value="{{ config('settings.paystackPrivateKey') }}" placeholder="PayStack Private Key (Leave blank if not using PayStack)">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="googleAnalyticsSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Google Analytics Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Enable Google Analytics</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.enableGoogleAnalytics') == "true") checked="checked" @endif name="enableGoogleAnalytics">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Analytics UA ID:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="googleAnalyticsId"
                                            value="{{ config('settings.googleAnalyticsId') }}" placeholder="UA-00000000-00">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="taxSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Tax Settings
                                </legend>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Enable Tax:</strong></label>
                                    <div class="col-lg-9">
                                        <div class="checkbox checkbox-switchery mt-2">
                                            <label>
                                            <input value="true" type="checkbox" class="switchery-primary" @if(config('settings.taxApplicable') == "true") checked="checked" @endif name="taxApplicable">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Tax Percentage:</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="taxPercentage"
                                            value="{{ config('settings.taxPercentage') }}" placeholder="Tax in Percentage">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="translationSettings">
                                <legend class="font-weight-semibold text-uppercase font-size-sm">
                                    Translation Settings
                                </legend>
                                <!-- DESKTOP -->
                                <button class="btn btn-primary translation-section-btn" type="button"> <i class="icon-display4 mr-1"></i>Desktop Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Heading</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopHeading"
                                            value="{{ config('settings.desktopHeading') }}" placeholder="Heading Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Sub Heading</strong></label>
                                    <div class="col-lg-9">
                                        <textarea class="summernote-editor" name="desktopSubHeading" placeholder="Sub Heading Text" rows="6">{{ config('settings.desktopSubHeading') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Use App Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopUseAppButton"
                                            value="{{ config('settings.desktopUseAppButton') }}" placeholder="Use App Button Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement One Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementOneTitle"
                                            value="{{ config('settings.desktopAchievementOneTitle') }}" placeholder="Achievement One Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement One Sub Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementOneSub"
                                            value="{{ config('settings.desktopAchievementOneSub') }}" placeholder="Achievement One Sub Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement Two Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementTwoTitle"
                                            value="{{ config('settings.desktopAchievementTwoTitle') }}" placeholder="Achievement Two Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement Two Sub Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementTwoSub"
                                            value="{{ config('settings.desktopAchievementTwoSub') }}" placeholder="Achievement Two Sub Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement Three Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementThreeTitle"
                                            value="{{ config('settings.desktopAchievementThreeTitle') }}" placeholder="Achievement Four Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement Three Sub Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementThreeSub"
                                            value="{{ config('settings.desktopAchievementThreeSub') }}" placeholder="Achievement Three Sub Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement Four Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementFourTitle"
                                            value="{{ config('settings.desktopAchievementFourTitle') }}" placeholder="Achievement Four Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Achievement Four Sub Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopAchievementFourSub"
                                            value="{{ config('settings.desktopAchievementFourSub') }}" placeholder="Achievement Sub Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Footer Address</strong></label>
                                    <div class="col-lg-9">
                                        <textarea class="summernote-editor" name="desktopFooterAddress">{{ config('settings.desktopFooterAddress') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Social Heading Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopFooterSocialHeader"
                                            value="{{ config('settings.desktopFooterSocialHeader') }}" placeholder="Social Heading Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Facebook Link</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopSocialFacebookLink"
                                            value="{{ config('settings.desktopSocialFacebookLink') }}" placeholder="Facebook Link (Icon won't be shown if left empty)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Google Plus Link</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopSocialGoogleLink"
                                            value="{{ config('settings.desktopSocialGoogleLink') }}" placeholder="Google Plus Link (Icon won't be shown if left empty)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Youtube Link</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopSocialYoutubeLink"
                                            value="{{ config('settings.desktopSocialYoutubeLink') }}" placeholder="Youtube Link (Icon won't be shown if left empty)">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Instagram Link</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="desktopSocialInstagramLink"
                                            value="{{ config('settings.desktopSocialInstagramLink') }}" placeholder="Instagram Link (Icon won't be shown if left empty)">
                                    </div>
                                </div>
                                <!-- END DESKTOP -->
                                <!-- MOBILE -->
                                <!-- First Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>First Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Heading</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firstScreenHeading"
                                            value="{{ config('settings.firstScreenHeading') }}" placeholder="First Screen Heading">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Sub Heading</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firstScreenSubHeading"
                                            value="{{ config('settings.firstScreenSubHeading') }}" placeholder="First Screen Sub Heading">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Setup Locaion Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firstScreenSetupLocation"
                                            value="{{ config('settings.firstScreenSetupLocation') }}" placeholder="Setup Location Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Welcome Message Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firstScreenWelcomeMessage"
                                            value="{{ config('settings.firstScreenWelcomeMessage') }}" placeholder="Welcome Message Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firstScreenLoginText"
                                            value="{{ config('settings.firstScreenLoginText') }}" placeholder="Login Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="firstScreenLoginBtn"
                                            value="{{ config('settings.firstScreenLoginBtn') }}" placeholder="Login Button Text">
                                    </div>
                                </div>
                                <!-- END First Screen Settings -->
                                <!-- Login Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Login Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login Error Message</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="loginErrorMessage"
                                            value="{{ config('settings.loginErrorMessage') }}" placeholder="Login Error Message">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Please Wait Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="pleaseWaitText"
                                            value="{{ config('settings.pleaseWaitText') }}" placeholder="Please Wait Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login Title</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="loginLoginTitle"
                                            value="{{ config('settings.loginLoginTitle') }}" placeholder="Login Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login SubTitle Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="loginLoginSubTitle"
                                            value="{{ config('settings.loginLoginSubTitle') }}" placeholder="Login SubTitle Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login Email Label Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="loginLoginEmailLabel"
                                            value="{{ config('settings.loginLoginEmailLabel') }}" placeholder="Login Button Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Login Password Label Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="loginLoginPasswordLabel"
                                            value="{{ config('settings.loginLoginPasswordLabel') }}" placeholder="Login Button Text">
                                    </div>
                                </div>
                                <!-- END Login Screen Settings-->
                                <!-- Search Location Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Search Location Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Search Location Placeholder Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="searchAreaPlaceholder"
                                            value="{{ config('settings.searchAreaPlaceholder') }}" placeholder="Search Location Placeholder Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Search Popular Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="searchPopularPlaces"
                                            value="{{ config('settings.searchPopularPlaces') }}" placeholder="Search Popular Text">
                                    </div>
                                </div>
                                <!-- END Search Location Screen Settings -->
                                <!-- HomePage Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>HomePage Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Restaurant Count Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="restaurantCountText"
                                            value="{{ config('settings.restaurantCountText') }}" placeholder="Restaurant Count Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Featured Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="restaurantFeaturedText"
                                            value="{{ config('settings.restaurantFeaturedText') }}" placeholder="Restaurant Featured Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Mins Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="homePageMinsText"
                                            value="{{ config('settings.homePageMinsText') }}" placeholder="Mins Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>For Two Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="homePageForTwoText"
                                            value="{{ config('settings.homePageForTwoText') }}" placeholder="For Two Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Footer Near Me Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="searchPopularPlaces"
                                            value="{{ config('settings.searchPopularPlaces') }}" placeholder="Footer Near Me Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Footer Explore Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="footerExplore"
                                            value="{{ config('settings.footerExplore') }}" placeholder="Footer ExploreText">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Footer Cart Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="footerCart"
                                            value="{{ config('settings.footerCart') }}" placeholder="Footer Cart Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Footer Account Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="footerAccount"
                                            value="{{ config('settings.footerAccount') }}" placeholder="Footer Account Text">
                                    </div>
                                </div>
                                <!--END HomePage Screen Settings -->
                                <!-- Explore Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Explore Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Restaurant Search Placeholder Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="restaurantSearchPlaceholder"
                                            value="{{ config('settings.restaurantSearchPlaceholder') }}" placeholder="Restaurant Search Placeholder Text">
                                    </div>
                                </div>
                                <!-- END Explore Screen Settings -->
                                <!-- Items Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Items Screen Settings </button>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Recommended Badge Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="recommendedBadgeText"
                                            value="{{ config('settings.recommendedBadgeText') }}" placeholder="Recommended Badge Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Popular Item Badge Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="popularBadgeText"
                                            value="{{ config('settings.popularBadgeText') }}" placeholder="Popular Item Badge Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>New Item Badge Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="newBadgeText"
                                            value="{{ config('settings.newBadgeText') }}" placeholder="New Item Badge Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Recommended Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="itemsPageRecommendedText"
                                            value="{{ config('settings.itemsPageRecommendedText') }}" placeholder="Recommended Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Fixed Cart View Cart Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="floatCartViewCartText"
                                            value="{{ config('settings.floatCartViewCartText') }}" placeholder="Fixed Cart View Cart Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Fixed Cart Items Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="floatCartItemsText"
                                            value="{{ config('settings.floatCartItemsText') }}" placeholder="Fixed Cart Items Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Customizable Item Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="customizableItemText"
                                            value="{{ config('settings.customizableItemText') }}" placeholder="Customization Heading">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Customization Heading</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="customizationHeading"
                                            value="{{ config('settings.customizationHeading') }}" placeholder="Customization Heading">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Customizable Done Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="customizationDoneBtnText"
                                            value="{{ config('settings.customizationDoneBtnText') }}" placeholder="Customizable Done Button Text">
                                    </div>
                                </div>
                                <!--END Items Screen Settings -->
                                <!-- Cart Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Cart Screen Settings </button> 
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartPageTitle"
                                            value="{{ config('settings.cartPageTitle') }}" placeholder="Cart Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Items In Cart Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartItemsInCartText"
                                            value="{{ config('settings.cartItemsInCartText') }}" placeholder="Items In Cart Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Empty Cart Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartEmptyText"
                                            value="{{ config('settings.cartEmptyText') }}" placeholder="Empty Cart Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Suggestions Placeholder Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartSuggestionPlaceholder"
                                            value="{{ config('settings.cartSuggestionPlaceholder') }}" placeholder="Cart Suggestions Placeholder Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Coupon Placeholder Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartCouponText"
                                            value="{{ config('settings.cartCouponText') }}" placeholder="Cart Coupon Placeholder Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Applied Coupon Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartApplyCoupon"
                                            value="{{ config('settings.cartApplyCoupon') }}" placeholder="Applied Coupon Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Invalid Coupon Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartInvalidCoupon"
                                            value="{{ config('settings.cartInvalidCoupon') }}" placeholder="Invalid Coupon Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Bill Details Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartBillDetailsText"
                                            value="{{ config('settings.cartBillDetailsText') }}" placeholder="Cart Bill Details Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Total Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartItemTotalText"
                                            value="{{ config('settings.cartItemTotalText') }}" placeholder="Cart Total Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart To Pay Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartToPayText"
                                            value="{{ config('settings.cartToPayText') }}" placeholder="Cart To Pay Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Charges Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartDeliveryCharges"
                                            value="{{ config('settings.cartDeliveryCharges') }}" placeholder="Delivery Charges Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Restaurant Charges Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartRestaurantCharges"
                                            value="{{ config('settings.cartRestaurantCharges') }}" placeholder="Restaurant Charges Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Select Your Address Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartSetYourAddress"
                                            value="{{ config('settings.cartSetYourAddress') }}" placeholder="Cart Select Your Address Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>New Address Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="buttonNewAddress"
                                            value="{{ config('settings.buttonNewAddress') }}" placeholder="New Address Button Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Change Location Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartChangeLocation"
                                            value="{{ config('settings.cartChangeLocation') }}" placeholder="Cart Change Location Button Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Deliver To Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartDeliverTo"
                                            value="{{ config('settings.cartDeliverTo') }}" placeholder="Cart Deliver To Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Select Payment Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutSelectPayment"
                                            value="{{ config('settings.checkoutSelectPayment') }}" placeholder="Select Payment Button Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Login Header Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartLoginHeader"
                                            value="{{ config('settings.cartLoginHeader') }}" placeholder="Cart Login Header Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Login Sub Header Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartLoginSubHeader"
                                            value="{{ config('settings.cartLoginSubHeader') }}" placeholder="Cart Login Sub Header">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cart Login Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="cartLoginButtonText"
                                            value="{{ config('settings.cartLoginButtonText') }}" placeholder="Cart Login Button Text">
                                    </div>
                                </div>
                                <!-- END Cart Screen Settings -->
                                <!-- Checkout Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Checkout Screen Settings </button> 
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Checkout Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutPageTitle"
                                            value="{{ config('settings.checkoutPageTitle') }}" placeholder="Checkout Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Checkout Payment List Title Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutPaymentListTitle"
                                            value="{{ config('settings.checkoutPaymentListTitle') }}" placeholder="Checkout Payment List Title Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Checkout Payment In Process Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutPaymentInProcess"
                                            value="{{ config('settings.checkoutPaymentInProcess') }}" placeholder="Checkout Payment In Process Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Stripe Text Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutStripeText"
                                            value="{{ config('settings.checkoutStripeText') }}" placeholder="Stripe Text Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Stripe Sub Text Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutStripeSubText"
                                            value="{{ config('settings.checkoutStripeSubText') }}" placeholder="Stripe Sub Text Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cash On Delivery Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutCodText"
                                            value="{{ config('settings.checkoutCodText') }}" placeholder="Cash On Delivery Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Cash On Delivery Sub Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="checkoutCodSubText"
                                            value="{{ config('settings.checkoutCodSubText') }}" placeholder="Cash On Delivery Sub Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>PayStack Payment Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="paystackPayText"
                                            value="{{ config('settings.paystackPayText') }}" placeholder="PayStack Payment Text">
                                    </div>
                                </div>
                                <!-- END Checkout Screen Settings -->
                                <!-- Running Order Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Running Order Screen Settings </button> 
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Placed Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderPlacedTitle"
                                            value="{{ config('settings.runningOrderPlacedTitle') }}" placeholder="Order Placed Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Placed Sub Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderPlacedSub"
                                            value="{{ config('settings.runningOrderPlacedSub') }}" placeholder="Order Placed Sub Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Preparing Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderPreparingTitle"
                                            value="{{ config('settings.runningOrderPreparingTitle') }}" placeholder="Order Preparing Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Preparing Sub Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderPreparingSub"
                                            value="{{ config('settings.runningOrderPreparingSub') }}" placeholder="Order Preparing Sub Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>On Way Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderOnwayTitle"
                                            value="{{ config('settings.runningOrderOnwayTitle') }}" placeholder="On Way Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>On Way Sub Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderOnwaySub"
                                            value="{{ config('settings.runningOrderOnwaySub') }}" placeholder="On Way Sub Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Assigned Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderDeliveryAssignedTitle"
                                            value="{{ config('settings.runningOrderDeliveryAssignedTitle') }}" placeholder="Delivery Assigned Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Assigned Sub Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderDeliveryAssignedSub"
                                            value="{{ config('settings.runningOrderDeliveryAssignedSub') }}" placeholder="Delivery Assigned Sub Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Canceled Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderCanceledTitle"
                                            value="{{ config('settings.runningOrderCanceledTitle') }}" placeholder="Order Canceled Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Canceled Sub Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderCanceledSub"
                                            value="{{ config('settings.runningOrderCanceledSub') }}" placeholder="Order Canceled Sub Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Refresh Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="runningOrderRefreshButton"
                                            value="{{ config('settings.runningOrderRefreshButton') }}" placeholder="Refresh Button Text">
                                    </div>
                                </div>
                                <!-- END Running Order Screen Settings -->
                                <!-- Account Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Account Screen Settings </button> 
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>My Account Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="accountMyAccount"
                                            value="{{ config('settings.accountMyAccount') }}" placeholder="My Account Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Manage Address Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="accountManageAddress"
                                            value="{{ config('settings.accountManageAddress') }}" placeholder="Manage Address Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>My Orders Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="accountMyOrders"
                                            value="{{ config('settings.accountMyOrders') }}" placeholder="My Orders Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Helo & FAQ Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="accountHelpFaq"
                                            value="{{ config('settings.accountHelpFaq') }}" placeholder="Helo & FAQ Text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Logout Button Text</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="accountLogout"
                                            value="{{ config('settings.accountLogout') }}" placeholder="Logout Button Text">
                                    </div>
                                </div>
                                <!-- END Account Screen Settings -->
                                <hr>
                                <!--  Delivery Screen Settings -->
                                <button class="btn btn-primary translation-section-btn mt-4" type="button"> <i class="icon-mobile mr-1"></i>Delivery Screen Settings </button> 
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Welcome Message</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryWelcomeMessage"
                                            value="{{ config('settings.deliveryWelcomeMessage') }}" placeholder="Delivery Welcome Message">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Accepted Orders Title</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryAcceptedOrdersTitle"
                                            value="{{ config('settings.deliveryAcceptedOrdersTitle') }}" placeholder="Accepted Orders Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>No Accepted Orders Message</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryNoOrdersAccepted"
                                            value="{{ config('settings.deliveryNoOrdersAccepted') }}" placeholder="No Accepted Orders Message">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>New Orders Title</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryNewOrdersTitle"
                                            value="{{ config('settings.deliveryNewOrdersTitle') }}" placeholder="New Orders Title">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>No New Orders Message</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryNoNewOrders"
                                            value="{{ config('settings.deliveryNoNewOrders') }}" placeholder="No New Orders Message">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Items</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryOrderItems"
                                            value="{{ config('settings.deliveryOrderItems') }}" placeholder="Order Items">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Restaurant Address</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryRestaurantAddress"
                                            value="{{ config('settings.deliveryRestaurantAddress') }}" placeholder="Restaurant Address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Address</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryDeliveryAddress"
                                            value="{{ config('settings.deliveryDeliveryAddress') }}" placeholder="Delivery Address">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Get Direction Button</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryGetDirectionButton"
                                            value="{{ config('settings.deliveryGetDirectionButton') }}" placeholder="Get Direction Button">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Online Payment</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryOnlinePayment"
                                            value="{{ config('settings.deliveryOnlinePayment') }}" placeholder="Online Payment">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Pin Placeholder</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryDeliveryPinPlaceholder"
                                            value="{{ config('settings.deliveryDeliveryPinPlaceholder') }}" placeholder="Delivery Pin Placeholder">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Accept to Deliver Button</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryAcceptOrderButton"
                                            value="{{ config('settings.deliveryAcceptOrderButton') }}" placeholder="Accept to Deliver Button">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Picked Up Button</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryPickedUpButton"
                                            value="{{ config('settings.deliveryPickedUpButton') }}" placeholder="Picked Up Button">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivered Button</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryDeliveredButton"
                                            value="{{ config('settings.deliveryDeliveredButton') }}" placeholder="Delivered Button">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Order Completed Button</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryOrderCompletedButton"
                                            value="{{ config('settings.deliveryOrderCompletedButton') }}" placeholder="Order Completed Button">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Already Accepted Message</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryAlreadyAccepted"
                                            value="{{ config('settings.deliveryAlreadyAccepted') }}" placeholder="Delivery Already Accepted Message">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Invalid Delivery Pin Message</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryInvalidDeliveryPin"
                                            value="{{ config('settings.deliveryInvalidDeliveryPin') }}" placeholder="Invalid Delivery Pin Message">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Logout Button</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryLogoutDelivery"
                                            value="{{ config('settings.deliveryLogoutDelivery') }}" placeholder="Delivery Logout Button">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label"><strong>Delivery Logout Confirmation</strong></label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control form-control-lg" name="deliveryLogoutConfirmation"
                                            value="{{ config('settings.deliveryLogoutConfirmation') }}" placeholder="Delivery Logout Confirmation Message">
                                    </div>
                                </div>
                                <!--  END Delivery Screen Settings -->
                                <!-- END MOBILE -->
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf">
                    <div class="text-right mt-5">
                        <button type="submit" class="btn btn-primary">
                        Save All Settings
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
    
        function setSwitchery(switchElement, checkedBool) {
            if((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
                switchElement.setPosition(true);
                switchElement.handleOnchange(true);
            }
        }
    
        $('.form-control-uniform').uniform();
    
        // Display color formats
        $(".colorpicker-show-input").spectrum({
          showInput: true
        });
    
        if (Array.prototype.forEach) {
               var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery-primary'));
               elems.forEach(function(html) {
                   var switchery = new Switchery(html, { color: '#2196F3' });
               });
           }
           else {
               var elems = document.querySelectorAll('.switchery-primary');
               for (var i = 0; i < elems.length; i++) {
                   var switchery = new Switchery(elems[i], { color: '#2196F3' });
               }
           }
    
        $('.summernote-editor').summernote({
               height: 200,
               popover: {
                   image: [],
                   link: [],
                   air: []
                 }
        });
        
        $('.payment-gateway-switch').click(function(event) {
            var paymentgateway_id = $(this).val();
            var token = $("#csrf").val();
            console.log(paymentgateway_id);
    
            $.ajax({
                url: '{{ route('admin.togglePaymentGateways') }}',
                type: 'POST',
                dataType: 'json',
                data: {id: paymentgateway_id, _token: token},
            })
            .done(function() {
                $.jGrowl("Payment Gateway Updated", {
                    position: 'bottom-center',
                    header: 'SUCCESS 👌',
                    theme: 'bg-success',
                });
            })
            .fail(function() {
                $.jGrowl("Something went wrong! (Atleast one gateway needs to be active)", {
                    position: 'bottom-center',
                    header: 'Wooopsss ⚠️',
                    theme: 'bg-warning',
                });
                $('#paymentGatewaysData :input[value="'+ paymentgateway_id +'"]');
            })
        });
    });
</script>
@endsection