@extends('admin.layouts.master')
@section("title") User Availabilty - Dashboard
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>User Unavailabilities</h4>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
           <form action="#">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">From Date:</label>
                    <div class="col-lg-9">
                        <input type="date" class="form-control form-control-lg" name="from_date" id="from_date"
                            value="{{ $fromDate }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">To Date:</label>
                    <div class="col-lg-9">
                        <input type="date" class="form-control form-control-lg" name="to_date" id="to_date"
                            value="{{ $toDate }}" required>
                    </div>
                </div>
                <!--<div class="form-group row">-->
                <!--    <label class="col-lg-3 col-form-label">Opt Out:</label>-->
                <!--    <div class="col-lg-9">-->
                <!--        <select multiple class="form-control form-control-lg" id="optout">-->
                <!--            <option value="breakfast">Breakfast</option>-->
                <!--            <option value="lunch">Lunch</option>-->
                <!--            <option value="snacks">Snacks</option>-->
                <!--            <option value="dinner">Dinner</option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="text-right">
                    <button id="checkUserAvail" type="button" class="btn btn-primary" data-id="{{ route('admin.useravailability') }}/">
                    Check Users Unavailability
                    <i class="icon-database-insert ml-1"></i></button>
                </div>
                <input type="hidden" id="token" value="{{ csrf_token() }}">
            </form>
        </div>
    </div>
    @if(!Request::is('admin/user-availability'))
        <div class="container">
            <!--<div class="row mt-4">-->
            <!--    <div class="col-12 col-xl-4 mb-2">-->
            <!--        <strong>Total Number Of Users :- </strong>{{ $user_count }}-->
            <!--    </div>-->
            <!--    <div class="col-12 col-xl-4 mb-2">-->
            <!--        <strong>Total Number Of Users Optted Out :- </strong>{{ $user_opttedout }}-->
            <!--    </div>-->
            <!--    <div class="col-12 col-xl-4 mb-2">-->
            <!--        <strong>Total Number Of Users Available :- </strong>{{ $user_available }}-->
            <!--    </div>-->
            <!--</div>-->
            <div class="row mt-4">
                <div class="col-12 col-xl-3 mb-2">
                    <div class="col-xl-12 dashboard-display p-3">
                        <a class="block block-link-shadow text-right" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="text-center" style="color: #717171; font-weight: 500;">Users Optted Out From Breakfast</div>
                                <div class="dashboard-display-number text-center">{{ $count_breakfast }}</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-xl-3 mb-2">
                    <div class="col-xl-12 dashboard-display p-3">
                        <a class="block block-link-shadow text-right" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="text-center" style="color: #717171; font-weight: 500;">Users Optted Out From Lunch</strong></div>
                                <div class="dashboard-display-number text-center">{{ $count_lunch }}</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-xl-3 mb-2">
                    <div class="col-xl-12 dashboard-display p-3">
                        <a class="block block-link-shadow text-right" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="text-center" style="color: #717171; font-weight: 500;">Users Optted Out From Snacks</div>
                                <div class="dashboard-display-number text-center">{{ $count_snacks }}</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-xl-3 mb-2">
                    <div class="col-xl-12 dashboard-display p-3">
                        <a class="block block-link-shadow text-right" href="javascript:void(0)">
                            <div class="block-content block-content-full clearfix">
                                <div class="text-center" style="color: #717171; font-weight: 500;">Users Optted Out From Dinner</div>
                                <div class="dashboard-display-number text-center">{{ $count_dinner }}</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
<script>
    $(function () {
        $('#to_date').change(function() {
            var date = $(this).val();
            console.log(date, 'change')
            if( (new Date($('#from_date').val()).getTime() > new Date(date).getTime()))
            {
                alert("End Date should be greater than start date");
                $('#checkUserAvail').prop('disabled', true);
            }
            else{
                 $('#checkUserAvail').prop('disabled', false);
            }
        });
         $('#from_date').change(function() {
            var date = $(this).val();
            console.log(date, 'change')
            if( (new Date($('#to_date').val()).getTime() >= new Date(date).getTime()))
            {
             
                $('#checkUserAvail').prop('disabled', false);
            }
          
            else{
                 $('#checkUserAvail').prop('disabled', true);
            }
        });
        
        $("#checkUserAvail").click(function (e) { 
            let token = $("#token").val();
            let fdate = $("#from_date").val();
            let tdate = $("#to_date").val();
            
            
            
            if(fdate != '' && tdate != ''){
                var param = fdate+'&'+tdate;
                
                let val = $('#checkUserAvail').data("id");
                var url = val+''+param;
                window.location = url;
            }
            else{
                alert("Choose dates");
            }
        });
    });
    
</script>
@endsection