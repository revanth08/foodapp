@extends('admin.layouts.master')
@section("title") Items - Dashboard
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                @if(empty($query))
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
                @else
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX mr-2">{{ $count }}</span>
                <span class="font-weight-bold mr-2">Results for "{{ $query }}"</span>
                @endif
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewItem"
                    data-toggle="modal" data-target="#addNewItemModal">
                <b><i class="icon-user-plus"></i></b>
                Add New Item
                </button>
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left" id="addBulkItem"
                    data-toggle="modal" data-target="#addBulkItemModal">
                <b><i class="icon-database-insert"></i></b>
                Bulk CSV Upload
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <form action="{{ route('restaurant.post.searchItems') }}" method="GET">
        <div class="form-group form-group-feedback form-group-feedback-right search-box">
            <input type="text" class="form-control form-control-lg search-input"
                placeholder="Search with item name" name="query">
            <div class="form-control-feedback form-control-feedback-lg">
               <button class="btn btn-default" >Search</button>
            </div>
        </div>
        <button type="submit" class="hidden">Search</button>
        @csrf
    </form>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Item's Restaurant</th>
                            <th>Item's Category</th>
                            <th>Price</th>
                            <th>Attributes</th>
                            <th>Time Avail</th>
                            <th>Quantity</th>
                            <th style="width: 15%">Created At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td><img src="{{substr(url("/"), 0, strrpos(url("/"), '/'))}}{{ $item->image }}" alt="{{ $item->name }}" height="80" width="80" style="border-radius: 0.275rem;"></td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->restaurant->name }}</td>
                            <td>{{ $item->item_category->name }}</td>
                            <td>{{ $item->price }}</td>
                            <td>
                                @if($item->is_recommended)
                                <span class="badge badge-flat border-grey-800 text-danger text-capitalize mr-1">
                                Recommended
                                </span>
                                @endif
                                @if($item->is_popular)
                                <span class="badge badge-flat border-grey-800 text-primary text-capitalize mr-1">
                                Popular
                                </span>
                                @endif
                                @if($item->is_new)
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize mr-1">
                                New
                                </span>
                                @endif
                            </td>
                            <td>{{ substr_replace( $item->from_time, ':', 2, 0) }} to {{ substr_replace( $item->to_time, ':', 2, 0) }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->created_at->diffForHumans() }}</td>
                            <td class="text-center">
                                <div class="btn-group btn-group-justified">
                                    <a href="{{ route('restaurant.get.editItem', $item->id) }}"
                                        class="badge badge-primary badge-icon"> EDIT <i
                                        class="icon-database-edit2 ml-1"></i></a>
                                    @if($item->is_active)
                                    <a href="{{ route('restaurant.disableItem', $item->id) }}" class="badge badge-primary badge-icon ml-1" data-popup="tooltip" title="Disable Item" data-placement="bottom"> <i class="icon-switch2"></i> </a>
                                    @else
                                    <a href="{{ route('restaurant.disableItem', $item->id) }}" class="badge badge-primary badge-icon ml-1" data-popup="tooltip" title="Enable Item" data-placement="bottom" style="background-color: #FF5722"> <i class="icon-switch2"></i> </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $items->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<div id="addNewItemModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Add New Item</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('restaurant.saveNewItem') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item Name:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="name"
                                placeholder="Item Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Price:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg price" name="price"
                                placeholder="Item Price in {{ config('settings.currencyFormat') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item's Restaurant:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="restaurant_id" required>
                                @foreach ($restaurants as $restaurant)
                                <option value="{{ $restaurant->id }}" class="text-capitalize">{{ $restaurant->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     @if (count($kitchens) > 0)
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item's Kitchen:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="kitchen_id" required>
                            @foreach ($kitchens as $kitchen) 
                            <option value="{{ $kitchen->id }}" class="text-capitalize" >{{ $kitchen->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item's Category:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="item_category_id" required>
                                @foreach ($itemCategories as $itemCategory)
                                <option value="{{ $itemCategory->id }}" class="text-capitalize">{{ $itemCategory->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Addons Category:</label>
                        <div class="col-lg-9">
                           <select multiple="multiple" class="form-control select" data-fouc name="addon_category_item[]">
                        @foreach($addonCategories as $addonCategory)
                        <option value="{{ $addonCategory->id }}" class="text-capitalize">{{ $addonCategory->name }}</option>
                        @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Image:</label>
                        <div class="col-lg-9">
                            <img class="slider-preview-image hidden"/>
                            <div class="uploader">
                                <input type="file" class="form-control-lg form-control-uniform" name="image" required accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);">
                                <span class="help-text text-muted">Image sizes: 162x118 or 324x237 or 486x355 and so on.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Available From:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="from_time" required>
                            <option value="0000" class="text-capitalize" selected>00:00</option>
                            <option value="0030" class="text-capitalize" >00:30</option>
                            <option value="0100" class="text-capitalize" >01:00</option>
                            <option value="0130" class="text-capitalize" >01:30</option>
                            <option value="0200" class="text-capitalize" >02:00</option>
                            <option value="0230" class="text-capitalize" >02:30</option>
                            <option value="0300" class="text-capitalize" >03:00</option>
                            <option value="0330" class="text-capitalize" >03:30</option>
                            <option value="0400" class="text-capitalize" >04:00</option>
                            <option value="0430" class="text-capitalize" >04:30</option>
                            <option value="0500" class="text-capitalize" >05:00</option>
                            <option value="0530" class="text-capitalize" >05:30</option>
                            <option value="0600" class="text-capitalize" >06:00</option>
                            <option value="0630" class="text-capitalize" >06:30</option>
                            <option value="0700" class="text-capitalize" >07:00</option>
                            <option value="0730" class="text-capitalize" >07:30</option>
                            <option value="0800" class="text-capitalize" >08:00</option>
                            <option value="0830" class="text-capitalize" >08:30</option>
                            <option value="0900" class="text-capitalize" >09:00</option>
                            <option value="0930" class="text-capitalize" >09:30</option>
                            <option value="1000" class="text-capitalize" >10:00</option>
                            <option value="1030" class="text-capitalize" >10:30</option>
                            <option value="1100" class="text-capitalize" >11:00</option>
                            <option value="1130" class="text-capitalize" >11:30</option>
                            <option value="1200" class="text-capitalize" >12:00</option>
                            <option value="1230" class="text-capitalize" >12:30</option>
                            <option value="1300" class="text-capitalize" >13:00</option>
                            <option value="1330" class="text-capitalize" >13:30</option>
                            <option value="1400" class="text-capitalize" >14:00</option>
                            <option value="1430" class="text-capitalize" >14:30</option>
                            <option value="1500" class="text-capitalize" >15:00</option>
                            <option value="1530" class="text-capitalize" >15:30</option>
                            <option value="1600" class="text-capitalize" >16:00</option>
                            <option value="1630" class="text-capitalize" >16:30</option>
                            <option value="1700" class="text-capitalize" >17:00</option>
                            <option value="1730" class="text-capitalize" >17:30</option>
                            <option value="1800" class="text-capitalize" >18:00</option>
                            <option value="1830" class="text-capitalize" >18:30</option>
                            <option value="1900" class="text-capitalize" >19:00</option>
                            <option value="1930" class="text-capitalize" >19:30</option>
                            <option value="2000" class="text-capitalize" >20:00</option>
                            <option value="2030" class="text-capitalize" >20:30</option>
                            <option value="2100" class="text-capitalize" >21:00</option>
                            <option value="2130" class="text-capitalize" >21:30</option>
                            <option value="2200" class="text-capitalize" >22:00</option>
                            <option value="2230" class="text-capitalize" >22:30</option>
                            <option value="2300" class="text-capitalize" >23:00</option>
                            <option value="2330" class="text-capitalize" >23:30</option>
                            <option value="2359" class="text-capitalize" >23:59</option>

                            </select>
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Available Till:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="to_time" required>
                            <option value="0000" class="text-capitalize" >00:00</option>   
                            <option value="0030" class="text-capitalize" >00:30</option>
                            <option value="0100" class="text-capitalize" >01:00</option>
                            <option value="0130" class="text-capitalize" >01:30</option>
                            <option value="0200" class="text-capitalize" >02:00</option>
                            <option value="0230" class="text-capitalize" >02:30</option>
                            <option value="0300" class="text-capitalize" >03:00</option>
                            <option value="0330" class="text-capitalize" >03:30</option>
                            <option value="0400" class="text-capitalize" >04:00</option>
                            <option value="0430" class="text-capitalize" >04:30</option>
                            <option value="0500" class="text-capitalize" >05:00</option>
                            <option value="0530" class="text-capitalize" >05:30</option>
                            <option value="0600" class="text-capitalize" >06:00</option>
                            <option value="0630" class="text-capitalize" >06:30</option>
                            <option value="0700" class="text-capitalize" >07:00</option>
                            <option value="0730" class="text-capitalize" >07:30</option>
                            <option value="0800" class="text-capitalize" >08:00</option>
                            <option value="0830" class="text-capitalize" >08:30</option>
                            <option value="0900" class="text-capitalize" >09:00</option>
                            <option value="0930" class="text-capitalize" >09:30</option>
                            <option value="1000" class="text-capitalize" >10:00</option>
                            <option value="1030" class="text-capitalize" >10:30</option>
                            <option value="1100" class="text-capitalize" >11:00</option>
                            <option value="1130" class="text-capitalize" >11:30</option>
                            <option value="1200" class="text-capitalize" >12:00</option>
                            <option value="1230" class="text-capitalize" >12:30</option>
                            <option value="1300" class="text-capitalize" >13:00</option>
                            <option value="1330" class="text-capitalize" >13:30</option>
                            <option value="1400" class="text-capitalize" >14:00</option>
                            <option value="1430" class="text-capitalize" >14:30</option>
                            <option value="1500" class="text-capitalize" >15:00</option>
                            <option value="1530" class="text-capitalize" >15:30</option>
                            <option value="1600" class="text-capitalize" >16:00</option>
                            <option value="1630" class="text-capitalize" >16:30</option>
                            <option value="1700" class="text-capitalize" >17:00</option>
                            <option value="1730" class="text-capitalize" >17:30</option>
                            <option value="1800" class="text-capitalize" >18:00</option>
                            <option value="1830" class="text-capitalize" >18:30</option>
                            <option value="1900" class="text-capitalize" >19:00</option>
                            <option value="1930" class="text-capitalize" >19:30</option>
                            <option value="2000" class="text-capitalize" >20:00</option>
                            <option value="2030" class="text-capitalize" >20:30</option>
                            <option value="2100" class="text-capitalize" >21:00</option>
                            <option value="2130" class="text-capitalize" >21:30</option>
                            <option value="2200" class="text-capitalize" >22:00</option>
                            <option value="2230" class="text-capitalize" >22:30</option>
                            <option value="2300" class="text-capitalize" >23:00</option>
                            <option value="2330" class="text-capitalize" >23:30</option>
                            <option value="2359" class="text-capitalize" selected>23:59</option>
                            </select>
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Quantity:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg price" name="quantity"
                                placeholder="Item Quantity " required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is Recommended?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary recommendeditem" checked="checked" name="is_recommended">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is Popular?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary popularitem" checked="checked" name="is_popular">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is New?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary newitem" checked="checked" name="is_new">
                                </label>
                            </div>
                        </div>
                    </div>
                    @csrf
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        SAVE
                        <i class="icon-database-insert ml-1"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="addBulkItemModal" class="modal fade mt-5" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">CSV Bulk Upload for Items</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('restaurant.itemBulkUpload') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">CSV File: </label>
                        <div class="col-lg-10">
                            <div class="uploader">
                                <input type="file" accept=".csv" name="item_csv" class="form-control-uniform form-control-lg" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-left">
                        <button type="button" class="btn btn-primary" id="downloadSampleItemCsv">
                        Download Sample CSV
                        <i class="icon-file-download ml-1"></i>
                        </button>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        Upload
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input) {
       if (input.files && input.files[0]) {
           let reader = new FileReader();
           reader.onload = function (e) {
               $('.slider-preview-image')
                   .removeClass('hidden')
                   .attr('src', e.target.result)
                   .width(120)
                   .height(120);
           };
           reader.readAsDataURL(input.files[0]);
       }
    }
    $(function () {
        $('.select').select2({
        minimumResultsForSearch: Infinity,
        placeholder: 'Select addons categories if applicable',
        });
       $('.select-search').select2({
           minimumResultsForSearch: Infinity,
       });
    
       var recommendeditem = document.querySelector('.recommendeditem');
       new Switchery(recommendeditem, { color: '#f44336' });
    
       var popularitem = document.querySelector('.popularitem');
       new Switchery(popularitem, { color: '#8360c3' });
    
       var newitem = document.querySelector('.newitem');
       new Switchery(newitem, { color: '#333' });
       
       $('.form-control-uniform').uniform();
       
        $('#downloadSampleItemCsv').click(function(event) {
           event.preventDefault();
           window.location.href = "{{substr(url("/"), 0, strrpos(url("/"), '/'))}}/assets/docs/items-sample-csv.csv";
       });
        $('.price').numeric({allowThouSep:false, maxDecimalPlaces: 2 });
    });
</script>
@endsection