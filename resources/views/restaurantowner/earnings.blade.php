@extends('admin.layouts.master')
<head>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
   </head>
@section("title") Earnings & Payouts - Dashboard
@endsection
@section('content')
<div class="content">
    @if(!empty($restaurants))
    {{-- <div class="row"> --}}
        <form action="{{ route('restaurant.earnings') }}" method="GET">
            <div class="form-group row mt-5">
                <div class="col-lg-4">
                    <div>
                        <label class=" col-form-label"><span class="text-danger">*</span> Select Restaurant:</label>
                    </div>
                    <select class="form-control select-search" name="restaurant_id" required id="" style="">
                        <option value="">Select </option>
                        @foreach ($restaurants as $restaurant)
                            <option value="{{ $restaurant->id}}" name="{{ $restaurant->id}}" class="text-capitalize">
                                {{ $restaurant->name }} ({{ $restaurant->location->name }})
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-lg-4">
                    <div>
                        <label class= "col-form-label"><span class="text-danger">*</span> Select Date:</label>
                    </div>
                    <input type="text" name="datetimes" class="form-control datetimes" placeholder="Select-Date" />
                    {{-- <div class="form-control-feedback form-control-feedback-lg">
                        <span><button class="btn btn-default">Search</button></span>
                    </div> --}}
                </div>
                {{-- <div class="form-control-feedback form-control-feedback-lg">
                        <span><button class="btn btn-default">Search</button></span>
                    </div> --}}
                <div class="col-lg-4">
                    <div>
                        <label class= "col-form-label"><br></label>
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
            
            @csrf
        </form>
        <div class="row mt-4 d-none d-md-block">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="chart-container has-scroll">
                        <div class="chart has-fixed-height has-minimum-width" id="basic_donut1"></div>
                    </div>
                </div>
            </div>
        </div>
    {{-- </div> --}}
    <script>
        $(function(){
          // bind change event to select
          $('#dynamic_select').on('change', function () {
              var url = $(this).val(); // get selected value
              if (url) { // require a URL
                  window.location = url; // redirect
              } 
              return false;
          });
        });
    </script>
    @endif

    @if(empty($restaurants))
    <div class="row mt-4">
        <div class="col-12 col-xl-4 mb-2">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="text-center" style="color: #717171; font-weight: 500;">Net earnings before commission</div>
                        <div class="dashboard-display-number text-center">{{ config('settings.currencyFormat') }}{{ isset($balanceBeforeCommission) ? $balanceBeforeCommission : '0'  }}</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-xl-4 mb-2">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="text-center" style="color: #717171; font-weight: 500;">Your Balance (after commission of <strong>{{ $restaurant->commission_rate }}%)</strong></div>
                        <div class="dashboard-display-number text-center">{{ config('settings.currencyFormat') }}{{ isset( $balanceAfterCommission) ?  $balanceAfterCommission : '0' }}</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-12 col-xl-4 mb-2">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="text-center" style="color: #717171; font-weight: 500;">Total value of your sales, before commission</div>
                        <div class="dashboard-display-number text-center">{{ config('settings.currencyFormat') }}{{ isset( $totalEarning) ?  $totalEarning : '0'  }}</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-4 d-none d-md-block">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="chart-container has-scroll">
                    <div class="chart has-fixed-height has-minimum-width" id="basic_donut"></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    
    

    {{-- <div class="container">
        <div class="row mt-5">
            <div class="col-xl-12 p-3" style="border-radius: 4px; background-color: #fff; box-shadow: 0 1px 6px 1px rgba(0, 0, 0, 0.05);">
                <h4>
                    <strong>Request For Payout</strong>
                </h4>

                @if(!((double) isset( $balanceAfterCommission) ?  $balanceAfterCommission : '' > (double)config('settings.minPayout')))
                <p>
                    Your current balance is <strong>{{ config('settings.currencyFormat') }}{{isset( $balanceAfterCommission) ?  $balanceAfterCommission : ''}}</strong>. You will be eligible for a payout when your balance amount surpasses <strong>{{ config('settings.currencyFormat') }}{{ config('settings.minPayout') }}</strong>.
                </p>
                <i class="icon-exclamation" style="position: absolute; font-size: 5rem; color: #FF5722; right: 15px; top: 15px; opacity: 0.1;"></i>
                @else
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#sendPayoutRequest">Request for Payout</button>
                <i class="icon-piggy-bank" style="position: absolute; font-size: 5rem; color: #FF5722; right: 15px; top: 15px; opacity: 0.1;"></i>

                <div id="sendPayoutRequest" class="modal fade mt-5" tabindex="-1">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><span class="font-weight-bold">Request for Payout</span></h5>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <span class="help-text">
                                    You are requesting for a payout of <strong>{{ config('settings.currencyFormat') }}{{isset( $balanceAfterCommission) ?  $balanceAfterCommission : ''}}</strong>
                                </span>
                                <div class="modal-footer mt-4">
                                    <form method="POST" action="{{ route('restaurant.sendPayoutRequest') }}">
                                        <input type="hidden" name="restaurant_id" value={{$restaurant->id}}>
                                        @csrf
                                    <button type="submit" class="btn btn-primary">Send Request</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endif

            </div>
        </div>
    </div> --}}
    
    {{-- @if(!empty($payoutRequests))
    <div class="container">
        <div class="row mt-5 mb-5" style="border-radius: 4px; background-color: #fff; box-shadow: 0 1px 6px 1px rgba(0, 0, 0, 0.05);">
            <div class="col-xl-12">
                <h4 class="p-3">
                    <strong>
                        Requested Payouts
                    </strong>
                </h4>
                <div class="table-responsive" style="overflow: hidden; height: auto; min-height: 10rem;">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    Amount
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Transaction Mode
                                </th>
                                <th>
                                    Transaction ID
                                </th>
                                <th>
                                    Message
                                </th>
                                <th>
                                    Created
                                </th>
                                <th>
                                    Updated
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($payoutRequests as $payoutRequest)
                            <tr>
                                <td>{{ $payoutRequest->amount }}</td>
                                <td><span class="badge badge-flat border-grey-800 text-default text-capitalize">{{ $payoutRequest->status }}</span></td>
                                <td>
                                    @if($payoutRequest->transaction_mode != NULL)
                                    {{ $payoutRequest->transaction_mode }}
                                    @else
                                    ----
                                    @endif
                                </td>
                                <td>
                                    @if($payoutRequest->transaction_id != NULL)
                                    {{ $payoutRequest->transaction_id }}
                                    @else
                                    ----
                                    @endif
                                </td>
                                 <td>
                                    @if($payoutRequest->message != NULL)
                                    {{ $payoutRequest->message }}
                                    @else
                                    ----
                                    @endif
                                </td>
                                <td>{{ $payoutRequest->created_at->diffForHumans() }}</td>
                                <td>
                                    @if($payoutRequest->updated_at != NULL)
                                    {{ $payoutRequest->updated_at->diffForHumans() }}
                                    @else
                                    ----
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif --}}
    
    <script>
        $(function () {
            require.config({
                paths: {
                    echarts: '{{ substr(url("/"), 0, strrpos(url("/"), '/'))}}/assets/backend/global_assets/js/plugins/visualization/echarts'
                }
            });
        
            require(
                [
                    'echarts',
                    'echarts/theme/limitless',
                    'echarts/chart/pie',
                    'echarts/chart/funnel'
                ],
        
                function (ec, limitless) {
        
                    var basic_donut = ec.init(document.getElementById('basic_donut'), limitless);
                  
                    basic_donut_options = {
        
                        // Add title
                        title: {
                            text: 'Overview Of Order Statuses',
                            subtext: 'Of all orders till ',
                            x: 'center'
                        },
        
                        // Add legend
                        legend: {
                            show: false,
                            orient: 'vertical',
                            x: 'left',
                            data: ['']
                        },
        
                        // Display toolbox
                        toolbox: {
                            show: false,
                        },
        
                        // Enable drag recalculate
                        calculable: false,
        
                        // Add series
                        series: [
                            {
                                name: 'Orders',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                center: ['50%', '58%'],
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true
                                        },
                                        labelLine: {
                                            show: true
                                        }
                                    },
                                    emphasis: {
                                        label: {
                                            show: true,
                                            formatter: '{b}' + '\n\n' + '{c} ({d}%)',
                                            position: 'center',
                                            textStyle: {
                                                fontSize: '17',
                                                fontWeight: '500'
                                            }
                                        }
                                    }
                                },
        
                                data: {{ isset( $chartData) ?  $chartData : '' }}
                            }
                        ]
                    };
                    basic_donut.setOption(basic_donut_options);
        
                    window.onresize = function () {
                        setTimeout(function (){
                            basic_donut.resize();
                        }, 200);
                    }
                }
            );
        });
    </script>
    <script>
        $(function () {
            require.config({
                paths: {
                    echarts: '{{ substr(url("/"), 0, strrpos(url("/"), '/'))}}/assets/backend/global_assets/js/plugins/visualization/echarts'
                }
            });
        
            require(
                [
                    'echarts',
                    'echarts/theme/limitless',
                    'echarts/chart/pie',
                    'echarts/chart/funnel'
                ],
        
                function (ec, limitless) {
        
                    var basic_donut1 = ec.init(document.getElementById('basic_donut1'), limitless);
                
                    basic_donut1_options = {
        
                        // Add title
                        title: {
                            text: 'Overview Of Order Statuses',
                            subtext: 'Of all orders till ',
                            x: 'center'
                        },
        
                        // Add legend
                        legend: {
                            show: false,
                            orient: 'vertical',
                            x: 'left',
                            data: ['']
                        },
        
                        // Display toolbox
                        toolbox: {
                            show: false,
                        },
        
                        // Enable drag recalculate
                        calculable: false,
        
                        // Add series
                        series: [
                            {
                                name: 'Orders',
                                type: 'pie',
                                radius: ['50%', '70%'],
                                center: ['50%', '58%'],
                                itemStyle: {
                                    normal: {
                                        label: {
                                            show: true
                                        },
                                        labelLine: {
                                            show: true
                                        }
                                    },
                                    emphasis: {
                                        label: {
                                            show: true,
                                            formatter: '{b}' + '\n\n' + '{c} ({d}%)',
                                            position: 'center',
                                            textStyle: {
                                                fontSize: '17',
                                                fontWeight: '500'
                                            }
                                        }
                                    }
                                },
        
                                data: {{ isset( $chartDataPresentDay) ?  $chartDataPresentDay : '' }}
                            }
                        ]
                    };
                    basic_donut1.setOption(basic_donut1_options);
        
                    window.onresize = function () {
                        setTimeout(function (){
                            basic_donut1.resize();
                        }, 200);
                    }
                }
            );
        });
    </script>


    <script>
        $(function () {
        
            require.config({
                paths: {
                    echarts: '{{ substr(url("/"), 0, strrpos(url("/"), '/'))}}/assets/backend/global_assets/js/plugins/visualization/echarts'
                }
            });
        
            require(
                [
                    'echarts',
                    'echarts/theme/limitless',
                    'echarts/chart/bar',
                    'echarts/chart/line'
                ],
        
                function (ec, limitless) {
        
                    var basic_area = ec.init(document.getElementById('basic_area'), limitless);
                  
                    basic_area_options = {
                        
                        // Setup grid
                        grid: {
                            x: 40,
                            x2: 20,
                            y: 35,
                            y2: 25
                        },
        
                        // Add tooltip
                        tooltip: {
                            trigger: 'axis'
                        },
        
                        
                        calculable: false,
        
        
                            // Horizontal axis
                            xAxis: [{
                                type: 'category',
                                boundaryGap: false,
                                data: [''],
                            }],
        
                            // Vertical axis
                            yAxis: [{
                                name: "Earning in {{ config('settings.currencyFormat') }}",
                                nameLocation: "end",
                                type: 'value'
                            }],
        
                            // Add series
                            series: [
                                {
                                    name: 'Sales in {{ config('settings.currencyFormat') }}',
                                    type: 'line',
                                    smooth: true,
                                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                                    data: [''],
                                    itemStyle: {
                                        normal: {
                                            label: {
                                                show: true,
                                                textStyle: {
                                                    fontWeight: 500,
                                                }
                                            }
                                        }
                                    },
                                },
                            ]
                        };
                    basic_area.setOption(basic_area_options);
        
                    window.onresize = function () {
                        setTimeout(function (){
                            basic_area.resize();
                        }, 200);
                    }
                }
            );
        });
    </script>
</div>
<script type="text/javascript">
    $(function() {
  $('input[name="datetimes"]').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'YYYY/MM/DD hh:mm:ss'
    }
  });
});
</script>
@endsection