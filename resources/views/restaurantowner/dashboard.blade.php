@extends('admin.layouts.master')
@section("title")
Dashboard
@endsection
@section('content')
<div class="content">
    <div class="row mt-4">
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-city"></i>
                        </div>
                        <div class="dashboard-display-number">{{ $restaurantsCount }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Restaurants</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-basket"></i>
                        </div>
                        <div class="dashboard-display-number">{{ $ordersCount }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Orders Processed</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-stack-star"></i>
                        </div>
                        <div class="dashboard-display-number">{{ $orderItemsCount }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Items Sold</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-coin-dollar"></i>
                        </div>
                        <div class="dashboard-display-number">{{ config('settings.currencyFormat') }}
                            {{ $totalEarning }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Earnings</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row pt-4 p-0">
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="panel-heading">
                    <h4 class="panel-title pl-3 pt-3"><strong>NEW ORDERS</strong></h4>
                    <hr>
                </div>
                <div id="newOrdersTable" class="table-responsive @if(!count($newOrders)) hidden @endif">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th class="text-center">Accept Order</th>
                                <th class="text-center">Decline Items</th>
                            </tr>
                        </thead>
                        <tbody id="newOrdersData">
                            @php $prevOrderId = ''; $acceptbtn = ''; $totalbtn = ''; @endphp
                            @foreach($newOrders as $nO)
                            <tr>
                                <td>

                                    <a href="{{ route('restaurant.viewOrder', $nO->unique_order_id) }}"
                                        class="letter-icon-title">
                                        {{ $prevOrderId != $nO->unique_order_id ? $nO->unique_order_id : "" }}
                                        @php $prevOrderId = $nO->unique_order_id; @endphp
                                    </a>
                                </td>
                                <td>
                                    {{ $nO->names }}
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ config('settings.currencyFormat') }}
                                        {{ $nO->price }}</span>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">
                                        @if ($totalbtn != $nO->total)
                                        {{ config('settings.currencyFormat') }} {{$nO->total}}
                                        @else
                                        {{ "" }}

                                        @endif
                                        @php $totalbtn = $nO->total; @endphp
                                    </span>
                                </td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        NEW
                                    </span>
                                </td>
                                <td class="text-center">
                                    @if ($acceptbtn != $nO->unique_order_id)
                                    <a href="{{ route('restaurant.acceptOrder', $nO->unique_order_id) }}"
                                        class="badge badge-primary badge-icon">
                                        Accept Order
                                        <i class="icon-checkmark3 ml-1"></i>
                                    </a>
                                    @else
                                    {{ "" }}
                                    @endif
                                    @php $acceptbtn = $nO->unique_order_id; @endphp
                                </td>
                                <td class="text-center">
                                    <a class="badge badge-primary badge-icon" data-toggle="modal"
                                        data-target="#exampleModalCenter{{$nO->id}}"
                                        style="color: white; cursor: pointer;">
                                        Decline Item <i class="icon-cross ml-1"></i>
                                    </a>
                                    <div class="modal fade" id="exampleModalCenter{{$nO->id}}" tabindex="-1"
                                        role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" id="{{$nO->id}}"
                                            role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" style="font-size: 16px;">
                                                    Do you want to remove <span
                                                        style="font-size: 20px; color: red;">{{$nO->name}}</span> ?
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="{{ route('restaurant.declineItems', $nO->orderitemsId) }}"
                                                        type="button" class="btn btn-primary">Yes</a>
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">No</button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-3">
                        {{$newOrders->links() }}
                    </div>
                </div>
                @if(!count($newOrders))
                <div class="text-center text-muted pb-2" id="newOrdersNoOrdersMessage">
                    <h4> No orders to show</h4>
                </div>
                @endif
            </div>
        </div>
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="panel-heading">
                    <h4 class="panel-title pl-3 pt-3"><strong>PREPARING ORDERS</strong></h4>
                    <hr>
                </div>
                @if(count($acceptedOrders))
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Items</th>
                                <th>Order Status</th>
                                <th class="text-center">Order Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $prevOrder = '' @endphp
                            @foreach($acceptedOrders as $aO)
                            <tr>
                                <td>
                                    @if ($prevOrder != $aO->unique_order_id)
                                    <a href="{{ route('restaurant.viewOrder', $aO->unique_order_id) }}"
                                        class="letter-icon-title">
                                        {{ $aO->unique_order_id }}
                                    </a>
                                    @else
                                    {{""}}
                                    @endif
                                    @php $prevOrder = $aO->unique_order_id; @endphp
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ $aO->names }}</span>
                                </td>
                                <td>
                                    @if ($aO->item_status == 2)
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        PREPARING
                                    </span>
                                    @elseif ($aO->item_status == 3)
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize"
                                        style="background: green; color: white">
                                        COMPLETED
                                    </span>
                                    @else
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        CHECK IN KITCHEN
                                    </span>
                                    @endif

                                </td>
                                <td class="text-center">
                                    @if ($aO->item_status == 2)
                                    <a href="{{ route('restaurant.acceptOrderItem', $aO->id, $aO->order_id) }}"
                                        class="badge badge-primary badge-icon"> Mark Item Ready <i
                                            class="icon-checkmark3 ml-1"></i>
                                    </a>
                                    @elseif ($aO->item_status == 3)
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        ITEM READY
                                    </span>
                                    @else
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        CHECK IN KITCHEN
                                    </span>
                                    @endif

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-3">
                        {{$acceptedOrders->links() }}
                    </div>
                    @else
                    <div class="text-center text-muted pb-2">
                        <h4> No orders to show</h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="panel-heading">
                    <h4 class="panel-title pl-3 pt-3"><strong>COMPLETED ORDERS</strong></h4>
                    <hr>
                </div>
                @if(count($completedOrders))
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Items</th>
                                <th>Price</th>
                                <th>Payment Mode</th>
                                <th>Order Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($completedOrders as $cOrders)
                            <tr>
                                <td>
                                    <a href="{{ route('restaurant.viewOrder', $cOrders->unique_order_id) }}"
                                        class="letter-icon-title">{{ $cOrders->unique_order_id }}</a>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ $cOrders->names }}</span>
                                </td>
                                <td>
                                    <span class="text-semibold no-margin">{{ config('settings.currencyFormat') }}
                                        {{ $cOrders->total }}</span>
                                </td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        {{ $cOrders->payment_mode }}</span>
                                </td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize"
                                        style="background: green; color: white">
                                        COMPLETED
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <div class="text-center text-muted pb-2">
                        <h4> No orders to show</h4>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        var notification = document.createElement('audio');
        notification.setAttribute('src', '{{substr(url("/"), 0, strrpos(url("/"), ' / '))}}/assets/backend/audio/new-order.mp3');
        notification.setAttribute('type', 'audio/mp3');
        notification.setAttribute('muted', 'muted');

        let array = @json($newOrdersJS);

        setInterval(function () {
            $.ajax({
                url: '{{ route('restaurant.getNewOrders') }}',
                type: 'GET',
                dataType: 'json',
            })

                .done(function (newArray) {
                    if (JSON.stringify(array) !== JSON.stringify(newArray)) {
                        //if orders are not same so something here..
                        array = newArray;
                        //play sound
                        notification.play();

                        var tableData = "";
                        $.map(newArray, function (item, index) {
                            var viewOrderURL = "{{ url('/restaurant-owner/order') }}/" + item.unique_order_id;
                            var acceptedOrderURL = "{{ url('/restaurant-owner/orders/accept-order') }}/" + item.id;
                            var restaurantLocation = item.restaurant.name + " (" + item.restaurant.location.name + ")";
                            tableData += '<tr><td> <a href="' + viewOrderURL + '" class="letter-icon-title">' + item.unique_order_id + '</a> </td>';
                            tableData += '<td>' + restaurantLocation + '</td>';
                            tableData += '<td><span class="text-semibold no-margin">{{ config('settings.currencyFormat') }}' + item.total + '</span></td>';
                            tableData += '<td> <span class="badge badge-flat border-grey-800 text-default text-capitalize"> NEW </span> </td>';
                            tableData += '<td class="text-center"> <a href="' + acceptedOrderURL + '" class="badge badge-primary badge-icon"> Accept Order <i class="icon-checkmark3 ml-1"></i></a> </td></tr>'
                            $('#newOrdersData').html(tableData);
                            $('#newOrdersTable').removeClass('hidden')
                            $('#newOrdersNoOrdersMessage').remove();
                        });
                    }
                })
                .fail(function () {
                    console.log("error");
                })
        }, 3000);
    });
</script>
@endsection