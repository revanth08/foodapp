@extends('admin.layouts.master')
@section("title") pos
Dashboard
@endsection
@section('content')
<!-- <link rel="stylesheet" href="../../assets/backend/css/bootstrap.min.css"> -->
<link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .badge-notify {
        background: red;
        position: relative;
        top: -20px;
        right: 10px;
    }

    .my-cart-icon-affix {
        position: fixed;
        z-index: 999;
    }

    .boxshadow {
        -webkit-box-shadow: 0px 0px 17px 12px rgba(191, 191, 191, 1);
        -moz-box-shadow: 0px 0px 17px 12px rgba(191, 191, 191, 1);
        box-shadow: 0px 0px 17px 12px rgba(191, 191, 191, 1);
    }

    .navbar-brand img {
        height: 1.7rem;
        display: block;
    }

    a:hover,
    a:visited,
    a:link,
    a:active {
        text-decoration: none;
    }
</style>

<body>
    </br>
    <label for="">Select User: </label>
    <select class="form-control select-search" name="selectedUser" id="selectedUser" required style="width: 200px;"
        data-rsid="{{$restaurantIdFirst}}" data-rscode="{{$restaurantCode}}" data-token="{{ Auth::user()->auth_token}}">
        <option value="" selected> Select ID </option>
        @foreach($users as $user)
        <option value="{{ $user }}" class="text-capitalize" data-atkn="$user->auth_token" data-userHouse="$user->">
            {{ $user->name }}</option>
        @endforeach
    </select>
    <!-- <h1>{{$restaurantIdFirst}}</h1> -->
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="user-details">
                <table class="table table-bordered table-responsive" style="width: 500px;">
                    <thead>
                        <tr>
                            <th width="80">ID</th>
                            <th width="230">Name</th>
                            <th width="200">RoomNo</th>
                            <th width="330">Email</th>
                        </tr>
                    </thead>
                    <tbody id="userDetails">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="container page-header">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><i class="icon-circle-right2 mr-2"></i>
                    <span class="font-weight-bold mr-2">Take New Order Here</span>
                </h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
            <div style="float: right; cursor: pointer;">
                <span class="my-cart-icon"><img src="https://img.icons8.com/dotty/35/000000/purchase-order.png"><span
                        class="badge badge-notify my-cart-badge"></span></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 card">
            <div class="">
                <ul class="nav bg radius nav-pills nav-fill mb-3 bg" role="tablist">
                    <li class="nav-item" onclick="filterItems('all')">
                        <a class="nav-link show" data-toggle="pill" href="#">
                            <i class="fa fa-tags" aria-hidden="true"></i> All </a>
                    </li>
                    @foreach($itemCategories as $itemCategory)
                    <li class="nav-item" id="categories{{ $itemCategory->id }}"
                        onclick="filterItems({{ $itemCategory->id }})">
                        <a class="nav-link show" data-toggle="pill" href="#">
                            <i class="fa fa-tags" aria-hidden="true"></i> {{ $itemCategory->name }} </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <!-- @foreach ($items as $item) -->
            <span>
                <div class="row" style="padding: 30px;" id="items">
                </div>
            </span>
            <!-- @endforeach -->
            <div id="MyPopup" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                &times;</button>
                            <h4 class="modal-title">
                            </h4>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

<script type='text/javascript' src="../../assets/backend/js/jquery-2.2.3.min.js"></script>
<script type='text/javascript' src="../../assets/backend/js/jquery.mycart.js"></script>
<script type='text/javascript' src="../../assets/backend/js/bootstrap.min.js"></script>
<script>
    var allItems = {!! json_encode($items -> toArray(), JSON_HEX_TAG)!!};
    filterItems('all');
    //console.log("allItems"+JSON.stringify(allItems));

    var name = {!! json_encode($items -> toArray(), JSON_HEX_TAG)!!};
    //console.log("allItems"+JSON.stringify(allItems));

    function filterItems(itm_cat_id) {
        var filteredAry = [];
        if (itm_cat_id != 'all') {
            filteredAry = $.grep(allItems, function (n, i) {
                return parseInt(n.item_category_id) === itm_cat_id;
            });
        }
        else {
            filteredAry = allItems;
            //filteredAry = $.grep( allItems.data, function( n, i ) {  return n.item_category_id!== id;
            //});
        }

        $("#items").html("");
        var myHtml = "";
        for (var x = 0; x < filteredAry.length; x++) {
            myHtml += ' <div class="col-md-2" > '
                + ' <figure class="card card-product boxshadow" > '
                + ' <div class="img-wrap text-center"> '
                + ' </br><img src="../../' + filteredAry[x].image + '" alt="' + filteredAry[x].name + '" height="80%" width="80%" style="border-radius: 0.275rem; "> '
                + ' </div> '
                + ' <figcaption class="info-wrap text-center"> '
                + ' <h4 class="">' + filteredAry[x].name + '</h4> '
                + ' <div class="action-wrap"> '
                + ' <span class="price-new"> ' + filteredAry[x].price + '</span> '
                + ' </div> '
                + ' <div class="" style="padding: 5px;"> '
                + ' <a href="javascript:void(0);" class="btn btn-primary btn-sm  my-cart-btn btn-block text-center" style="width: 80%; right: -10%; padding-bottom: 5px;" data-id="' + filteredAry[x].id + '" data-name="' + filteredAry[x].name + '" data-price="' + filteredAry[x].price + '" data-kitchen_id="' + filteredAry[x].kitchen_id + '" data-quantity="1" data-image="{{substr(url("/"), 0, strrpos(url("/"), ' / '))}}' + filteredAry[x].image + '"> '
                + ' <i class="icon-cart-add"> '
                + ' </i> '
                + ' Add </a> '
                + ' </div> '
                + ' </div> '
                + ' </figcaption> '
                + ' </figure> '
                + ' </div> '
        }
        $("#items").html(myHtml);

    }

</script>

<script type="text/javascript">
    $(function () {
        var setAllProducts = function (products) {
            localStorage.products = JSON.stringify(products);
        };
        var goToCartIcon = function ($addTocartBtn) {
            var $cartIcon = $(".my-cart-icon");
            var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({ "position": "fixed", "z-index": "999" });
            $addTocartBtn.prepend($image);
            var position = $cartIcon.position();
            $image.animate({
                top: position.top,
                left: position.right
            }, 1, "linear", function () {
                $image.remove();
            });
        }

        $('.my-cart-btn').myCart({
            currencySymbol: '₹',
            classCartIcon: 'my-cart-icon',
            classCartBadge: 'my-cart-badge',
            classProductQuantity: 'my-product-quantity',
            classProductRemove: 'my-product-remove',
            classCheckoutCart: 'my-cart-checkout',
            affixCartIcon: true,
            showCheckoutModal: true,
            numberOfDecimals: 2,
            cartItems: [],
            clickOnAddToCart: function ($addTocart) {
                goToCartIcon($addTocart);
            },
            afterAddOnCart: function (products, totalPrice, totalQuantity) {
                console.log("afterAddOnCart", products, totalPrice, totalQuantity);
            },
            clickOnCartIcon: function ($cartIcon, products, totalPrice, totalQuantity) {
                console.log("cart icon clicked", $cartIcon, products, totalPrice, totalQuantity);
            },
            checkoutCart: function (products, totalPrice, totalQuantity) {
                var checkoutString = "Total Price: " + totalPrice + "\nTotal Quantity: " + totalQuantity;
                console.log(products);

                var selUserDetails = JSON.parse($('#selectedUser').val());
                var strUid = selUserDetails['id'];
                var strUName = selUserDetails['name'];
                var strUEmail = selUserDetails['email'];
                var strUHouse = selUserDetails['house'];
                var strUPhone = selUserDetails['phone'];
                //var strUPhone = selUserDetails['auth_token'];
                var strRid = $('#selectedUser').attr("data-rsid");
                var strtkn = $('#selectedUser').attr("data-token");
                var strRcode = $('#selectedUser').attr("data-rscode");

                var x = [];

                for (var i = 0; i < products.length; i++) {

                    x.push({
                        "id": products[i].id,
                        "restaurant_id": strRid,
                        "item_category_id": 1,
                        "name": products[i].name,
                        "price": products[i].price,
                        "kitchen_id": products[i].kitchen_id,
                        "image": "",
                        "addon_categories": [],
                        "quantity": products[i].quantity
                    });
                }

                console.log(x);
                var d = {
                    "token": strtkn,
                    "user": {
                        "success": true,
                        "data":
                        {
                            "id": strUid,
                            "auth_token": strtkn,
                            "name": strUName,
                            "email": strUEmail,
                            "phone": "strUPhone",
                            "default_address_id": 13,
                            "default_address": {
                                "address": "defaultRestaurant",
                                "house": "",
                                "landmark": "",
                                "house": strUHouse,
                                "landmark": "Layout 1",
                                "tag": "Home"
                            },
                            "delivery_pin": "3SOUN"
                        },
                        "running_order": null
                    },
                    "order": x,
                    "coupon": [],
                    "location": "Campus",
                    "order_comment": null,
                    "restaurantCode": strRcode,
                    "total":
                    {
                        "productQuantity": totalQuantity,
                        "totalPrice": totalPrice
                    },
                    "method": "QRPAYMENT",
                    "payment_token": ""
                }

                $.ajax({
                    type: 'POST',
                    dataType: JSON,
                    url: 'http://localhost/foodapp-repo/foodapp/public/api/place-order',
                    data: d,
                });


                //alert($("#selectedUser").val(), checkoutString);
                // alert(checkoutString)
                console.log("checking out", products, totalPrice, totalQuantity);
            },
            getDiscountPrice: function (products, totalPrice, totalQuantity) {
                console.log("calculating discount", products, totalPrice, totalQuantity);
                return totalPrice * 1;
            }
        });

        $("#addNewProduct").click(function (event) {
            var currentElementNo = $(".row").children().length + 1;
            $(".row").append('<div class="col-md-3 text-center"><img src="images/img_empty.png" width="150px" height="150px"><br>product ' + currentElementNo + ' - <strong> ' + currentElementNo + '</strong><br><button class="btn btn-danger my-cart-btn" data-id="' + currentElementNo + '" data-name="product ' + currentElementNo + '" data-summary="summary ' + currentElementNo + '" data-price="' + currentElementNo + '" data-quantity="1" data-image="images/img_empty.png">Add to Cart</button><a href="#" class="btn btn-info">Details</a></div>')
        });
    });

    $('#selectedUser').change(function () {
        var selUserDetails = JSON.parse($('#selectedUser').val());
        console.log(selUserDetails);
        var strHtml = "<tr><td>" + selUserDetails['id'] + "</td>";
        strHtml += " <td>" + selUserDetails['name'] + "</td>";
        strHtml += " <td>" + selUserDetails['house'] + "</td>";
        strHtml += " <td>" + selUserDetails['email'] + "</td></tr>";
        $("#userDetails").html(strHtml);
    });
</script>
@endsection