@extends('admin.layouts.master')
@section("title") Orders - Dashboard
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">TOTAL</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ $count }}</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button onclick="location.href = 'pos';" type="button"
                    class="btn btn-primary btn-labeled btn-labeled-left mr-2" id="newOrder">
                    <b><i class="icon-user-plus"></i></b>
                    Order here
                </button>
                <button type="button" class="btn btn-primary" id="downloadSampleOrdersCsv">
                    Download CSV
                    <i class="icon-file-download ml-1"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <div class="panel-heading">
                    <h4 class="panel-title pl-3"><strong>Summary</strong></h4>
                    <hr>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Food Item</th>
                            <th>Price/item</th>
                            <th>Total Items Sold</th>
                            <th>Value of Item Sold</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($itemSold as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->price }}</td>
                        <td>{{$item->totalQuantity}}</td>
                            <td>{{ $item->price * $item->totalQuantity }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="mt-3">
                    {{ $orders->links() }}
                </div> --}}
            </div>
        </div>
    </div>
    
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <form action="{{ route('restaurant.post.searchOrders') }}" method="GET">
                    <div class="panel-heading">
                        <h4 class="panel-title pl-3"><strong>Completed Orders</strong></h4>
                        <hr>
                    </div>
                    <div class="form-group form-group-feedback form-group-feedback-right search-box">
                        <input type="text" class="form-control form-control-lg search-input" placeholder="Search with order id..."
                            name="query">
                        <div class="form-control-feedback form-control-feedback-lg">
                            <span><button class="btn btn-default">Search</button></span>
                        </div>
                    </div>
                    <button type="submit" class="hidden">Search</button>
                    @csrf
                </form>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            {{-- <th>Restaurant Name</th> --}}
                            <th>Status</th>
                            <th>Total</th>
                            <th>Coupon</th>
                            <th>Order Placed At</th>
                            <th class="text-center" style="width: 10%;"><i class="
                                icon-circle-down2"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order->unique_order_id }}</td>
                            {{-- <td>{{ $order->restaurant->name }}</td> --}}
                            <td>
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize text-left">
                                    @if ($order->orderstatus_id == 0) Pending Payment @endif
                                    @if ($order->orderstatus_id == 1) Order Placed @endif
                                    @if ($order->orderstatus_id == 2) Order Accepted @endif
                                    @if ($order->orderstatus_id == 3) Delivery Assigned @endif
                                    @if ($order->orderstatus_id == 4) Picked Up @endif
                                    @if ($order->orderstatus_id == 5) Completed @endif
                                    @if ($order->orderstatus_id == 6) Canceled @endif

                                    @if($order->accept_delivery !== null)
                                    @if($order->orderstatus_id > 2 && $order->orderstatus_id < 6) Delivery by: <b>
                                        {{ $order->accept_delivery->user->name }}</b>
                                        @endif
                                        @endif
                                </span>
                            </td>
                            <td>{{ config('settings.currencyFormat') }} {{ $order->total }}</td>
                            <td>
                                @if($order->coupon_name == NULL) NONE @else
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                    {{ $order->coupon_name }}
                                </span>
                                @endif
                            </td>
                            <td>{{ $order->created_at->diffForHumans() }}</td>
                            <td class="text-center">
                                <a href="{{ route('restaurant.viewOrder', $order->unique_order_id) }}"
                                    class="badge badge-primary badge-icon"> VIEW <i class="icon-file-eye ml-1"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{-- <div class="mt-3">
                    {{ $orders->links() }}
                </div> --}}
            </div>
        </div>
    </div>
</div>
<script>
    $('#downloadSampleOrdersCsv').click(function (event) {
        event.preventDefault();
        window.location.href = "{{substr(url(" / "), 0, strrpos(url(" / "), '/'))}}/assets/docs/orders-sample-csv.csv";
    });
</script>
@endsection