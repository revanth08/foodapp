@extends('admin.layouts.master')
@section("title") Item - Dashboard
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Editing</span>
                <span class="badge badge-primary badge-pill animated flipInX">"{{ $item->name }} -> {{ $item->restaurant->name }}"</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<div class="content">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('restaurant.updateItem') }}" method="POST" enctype="multipart/form-data">
                    <legend class="font-weight-semibold text-uppercase font-size-sm">
                        <i class="icon-address-book mr-2"></i> Item Details
                    </legend>
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item Name:</label>
                        <div class="col-lg-9">
                            <input value="{{ $item->name }}" type="text" class="form-control form-control-lg" name="name"
                                placeholder="Item Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Price:</label>
                        <div class="col-lg-9">
                            <input value="{{ $item->price }}" type="text" class="form-control form-control-lg price" name="price"
                                placeholder="Item Price in {{ config('settings.currencyFormat') }}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item's Restaurant:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="restaurant_id" required>
                            @foreach ($restaurants as $restaurant)
                            <option value="{{ $restaurant->id }}" class="text-capitalize" @if($item->restaurant_id == $restaurant->id) selected="selected" @endif>{{ $restaurant->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    @if (count($kitchens) > 0)
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item's Kitchen:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="kitchen_id" required>
                              <option value="0" class="text-capitalize" @if($item->kitchen_id == 0) selected="selected" @endif>--- Select ---</option>
                            @foreach ($kitchens as $kitchen)
                            <option value="{{ $kitchen->id }}" class="text-capitalize" @if($item->kitchen_id == $kitchen->id) selected="selected" @endif>{{ $kitchen->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Item's Category:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="item_category_id" required>
                            @foreach ($itemCategories as $itemCategory)
                            <option value="{{ $itemCategory->id }}" class="text-capitalize" @if($item->item_category_id == $itemCategory->id) selected="selected" @endif>{{ $itemCategory->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Item's Addons:</label>
                        <div class="col-lg-9">
                            @foreach($item->addon_categories as $addonCategory)
                            <span class="badge badge-flat border-grey-800" style="font-size: 0.9rem;">{{ $addonCategory->name }}
                            </span>
                            @endforeach
                            <select multiple="multiple" class="form-control select" data-fouc name="addon_category_item[]">
                        @foreach($addonCategories as $addonCategory)
                        <option value="{{ $addonCategory->id }}" class="text-capitalize">{{ $addonCategory->name }}</option>
                        @endforeach
                    </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Image:</label>
                        <div class="col-lg-9">
                            <img src="{{ substr(url("/"), 0, strrpos(url("/"), '/')) }}{{ $item->image }}" alt="Image" width="160" style="border-radius: 0.275rem;">
                            <img class="slider-preview-image hidden" style="border-radius: 0.275rem;" />
                            <div class="uploader">
                                <input type="hidden" name="old_image" value="{{ $item->image }}">
                                <input type="file" class="form-control-lg form-control-uniform" name="image" accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);">
                                <span class="help-text text-muted">Image sizes: 162x118 or 324x237 or 486x355 and so on.</span>
                            </div>
                        </div>
                    </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Available From:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="from_time" required>
                            <option value="0000" class="text-capitalize" @if('0000' == $item->from_time) selected="selected" @endif>00:00</option>
                            <option value="0030" class="text-capitalize" @if('0030' == $item->from_time) selected="selected" @endif>00:30</option>
                            <option value="0100" class="text-capitalize" @if('0100' == $item->from_time) selected="selected" @endif>01:00</option>
                            <option value="0130" class="text-capitalize" @if('0130' == $item->from_time) selected="selected" @endif>01:30</option>
                            <option value="0200" class="text-capitalize" @if('0200' == $item->from_time) selected="selected" @endif>02:00</option>
                            <option value="0230" class="text-capitalize" @if('0230' == $item->from_time) selected="selected" @endif>02:30</option>
                            <option value="0300" class="text-capitalize" @if('0300' == $item->from_time) selected="selected" @endif>03:00</option>
                            <option value="0330" class="text-capitalize" @if('0330' == $item->from_time) selected="selected" @endif>03:30</option>
                            <option value="0400" class="text-capitalize" @if('0400' == $item->from_time) selected="selected" @endif>04:00</option>
                            <option value="0430" class="text-capitalize" @if('0430' == $item->from_time) selected="selected" @endif>04:30</option>
                            <option value="0500" class="text-capitalize" @if('0500' == $item->from_time) selected="selected" @endif>05:00</option>
                            <option value="0530" class="text-capitalize" @if('0530' == $item->from_time) selected="selected" @endif>05:30</option>
                            <option value="0600" class="text-capitalize" @if('0600' == $item->from_time) selected="selected" @endif>06:00</option>
                            <option value="0630" class="text-capitalize" @if('0630' == $item->from_time) selected="selected" @endif>06:30</option>
                            <option value="0700" class="text-capitalize" @if('0700' == $item->from_time) selected="selected" @endif>07:00</option>
                            <option value="0730" class="text-capitalize" @if('0730' == $item->from_time) selected="selected" @endif>07:30</option>
                            <option value="0800" class="text-capitalize" @if('0800' == $item->from_time) selected="selected" @endif>08:00</option>
                            <option value="0830" class="text-capitalize" @if('0830' == $item->from_time) selected="selected" @endif>08:30</option>
                            <option value="0900" class="text-capitalize" @if('0900' == $item->from_time) selected="selected" @endif>09:00</option>
                            <option value="0930" class="text-capitalize" @if('0930' == $item->from_time) selected="selected" @endif>09:30</option>
                            <option value="1000" class="text-capitalize" @if('1000' == $item->from_time) selected="selected" @endif>10:00</option>
                            <option value="1030" class="text-capitalize" @if('1030' == $item->from_time) selected="selected" @endif>10:30</option>
                            <option value="1100" class="text-capitalize" @if('1100' == $item->from_time) selected="selected" @endif>11:00</option>
                            <option value="1130" class="text-capitalize" @if('1130' == $item->from_time) selected="selected" @endif>11:30</option>
                            <option value="1200" class="text-capitalize" @if('1200' == $item->from_time) selected="selected" @endif>12:00</option>
                            <option value="1230" class="text-capitalize" @if('1230' == $item->from_time) selected="selected" @endif>12:30</option>
                            <option value="1300" class="text-capitalize" @if('1300' == $item->from_time) selected="selected" @endif>13:00</option>
                            <option value="1330" class="text-capitalize" @if('1330' == $item->from_time) selected="selected" @endif>13:30</option>
                            <option value="1400" class="text-capitalize" @if('1400' == $item->from_time) selected="selected" @endif>14:00</option>
                            <option value="1430" class="text-capitalize" @if('1430' == $item->from_time) selected="selected" @endif>14:30</option>
                            <option value="1500" class="text-capitalize" @if('1500' == $item->from_time) selected="selected" @endif>15:00</option>
                            <option value="1530" class="text-capitalize" @if('1530' == $item->from_time) selected="selected" @endif>15:30</option>
                            <option value="1600" class="text-capitalize" @if('1600' == $item->from_time) selected="selected" @endif>16:00</option>
                            <option value="1630" class="text-capitalize" @if('1630' == $item->from_time) selected="selected" @endif>16:30</option>
                            <option value="1700" class="text-capitalize" @if('1700' == $item->from_time) selected="selected" @endif>17:00</option>
                            <option value="1730" class="text-capitalize" @if('1730' == $item->from_time) selected="selected" @endif>17:30</option>
                            <option value="1800" class="text-capitalize" @if('1800' == $item->from_time) selected="selected" @endif>18:00</option>
                            <option value="1830" class="text-capitalize" @if('1830' == $item->from_time) selected="selected" @endif>18:30</option>
                            <option value="1900" class="text-capitalize" @if('1900' == $item->from_time) selected="selected" @endif>19:00</option>
                            <option value="1930" class="text-capitalize" @if('1930' == $item->from_time) selected="selected" @endif>19:30</option>
                            <option value="2000" class="text-capitalize" @if('2000' == $item->from_time) selected="selected" @endif>20:00</option>
                            <option value="2030" class="text-capitalize" @if('2030' == $item->from_time) selected="selected" @endif>20:30</option>
                            <option value="2100" class="text-capitalize" @if('2100' == $item->from_time) selected="selected" @endif>21:00</option>
                            <option value="2130" class="text-capitalize" @if('2130' == $item->from_time) selected="selected" @endif>21:30</option>
                            <option value="2200" class="text-capitalize" @if('2200' == $item->from_time) selected="selected" @endif>22:00</option>
                            <option value="2230" class="text-capitalize" @if('2230' == $item->from_time) selected="selected" @endif>22:30</option>
                            <option value="2300" class="text-capitalize" @if('2300' == $item->from_time) selected="selected" @endif>23:00</option>
                            <option value="2330" class="text-capitalize" @if('2330' == $item->from_time) selected="selected" @endif>23:30</option>
                            <option value="2359" class="text-capitalize" @if('2359' == $item->from_time) selected="selected" @endif>23:59</option>

                            </select>
                        </div>
                    </div>
                     <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Available Till:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="to_time" required>
                            <option value="0000" class="text-capitalize" @if('0000' == $item->to_time) selected="selected" @endif>00:00</option>
                            <option value="0030" class="text-capitalize" @if('0030' == $item->to_time) selected="selected" @endif>00:30</option>
                            <option value="0100" class="text-capitalize" @if('0100' == $item->to_time) selected="selected" @endif>01:00</option>
                            <option value="0130" class="text-capitalize" @if('0130' == $item->to_time) selected="selected" @endif>01:30</option>
                            <option value="0200" class="text-capitalize" @if('0200' == $item->to_time) selected="selected" @endif>02:00</option>
                            <option value="0230" class="text-capitalize" @if('0230' == $item->to_time) selected="selected" @endif>02:30</option>
                            <option value="0300" class="text-capitalize" @if('0300' == $item->to_time) selected="selected" @endif>03:00</option>
                            <option value="0330" class="text-capitalize" @if('0330' == $item->to_time) selected="selected" @endif>03:30</option>
                            <option value="0400" class="text-capitalize" @if('0400' == $item->to_time) selected="selected" @endif>04:00</option>
                            <option value="0430" class="text-capitalize" @if('0430' == $item->to_time) selected="selected" @endif>04:30</option>
                            <option value="0500" class="text-capitalize" @if('0500' == $item->to_time) selected="selected" @endif>05:00</option>
                            <option value="0530" class="text-capitalize" @if('0530' == $item->to_time) selected="selected" @endif>05:30</option>
                            <option value="0600" class="text-capitalize" @if('0600' == $item->to_time) selected="selected" @endif>06:00</option>
                            <option value="0630" class="text-capitalize" @if('0630' == $item->to_time) selected="selected" @endif>06:30</option>
                            <option value="0700" class="text-capitalize" @if('0700' == $item->to_time) selected="selected" @endif>07:00</option>
                            <option value="0730" class="text-capitalize" @if('0730' == $item->to_time) selected="selected" @endif>07:30</option>
                            <option value="0800" class="text-capitalize" @if('0800' == $item->to_time) selected="selected" @endif>08:00</option>
                            <option value="0830" class="text-capitalize" @if('0830' == $item->to_time) selected="selected" @endif>08:30</option>
                            <option value="0900" class="text-capitalize" @if('0900' == $item->to_time) selected="selected" @endif>09:00</option>
                            <option value="0930" class="text-capitalize" @if('0930' == $item->to_time) selected="selected" @endif>09:30</option>
                            <option value="1000" class="text-capitalize" @if('1000' == $item->to_time) selected="selected" @endif>10:00</option>
                            <option value="1030" class="text-capitalize" @if('1030' == $item->to_time) selected="selected" @endif>10:30</option>
                            <option value="1100" class="text-capitalize" @if('1100' == $item->to_time) selected="selected" @endif>11:00</option>
                            <option value="1130" class="text-capitalize" @if('1130' == $item->to_time) selected="selected" @endif>11:30</option>
                            <option value="1200" class="text-capitalize" @if('1200' == $item->to_time) selected="selected" @endif>12:00</option>
                            <option value="1230" class="text-capitalize" @if('1230' == $item->to_time) selected="selected" @endif>12:30</option>
                            <option value="1300" class="text-capitalize" @if('1300' == $item->to_time) selected="selected" @endif>13:00</option>
                            <option value="1330" class="text-capitalize" @if('1330' == $item->to_time) selected="selected" @endif>13:30</option>
                            <option value="1400" class="text-capitalize" @if('1400' == $item->to_time) selected="selected" @endif>14:00</option>
                            <option value="1430" class="text-capitalize" @if('1430' == $item->to_time) selected="selected" @endif>14:30</option>
                            <option value="1500" class="text-capitalize" @if('1500' == $item->to_time) selected="selected" @endif>15:00</option>
                            <option value="1530" class="text-capitalize" @if('1530' == $item->to_time) selected="selected" @endif>15:30</option>
                            <option value="1600" class="text-capitalize" @if('1600' == $item->to_time) selected="selected" @endif>16:00</option>
                            <option value="1630" class="text-capitalize" @if('1630' == $item->to_time) selected="selected" @endif>16:30</option>
                            <option value="1700" class="text-capitalize" @if('1700' == $item->to_time) selected="selected" @endif>17:00</option>
                            <option value="1730" class="text-capitalize" @if('1730' == $item->to_time) selected="selected" @endif>17:30</option>
                            <option value="1800" class="text-capitalize" @if('1800' == $item->to_time) selected="selected" @endif>18:00</option>
                            <option value="1830" class="text-capitalize" @if('1830' == $item->to_time) selected="selected" @endif>18:30</option>
                            <option value="1900" class="text-capitalize" @if('1900' == $item->to_time) selected="selected" @endif>19:00</option>
                            <option value="1930" class="text-capitalize" @if('1930' == $item->to_time) selected="selected" @endif>19:30</option>
                            <option value="2000" class="text-capitalize" @if('2000' == $item->to_time) selected="selected" @endif>20:00</option>
                            <option value="2030" class="text-capitalize" @if('2030' == $item->to_time) selected="selected" @endif>20:30</option>
                            <option value="2100" class="text-capitalize" @if('2100' == $item->to_time) selected="selected" @endif>21:00</option>
                            <option value="2130" class="text-capitalize" @if('2130' == $item->to_time) selected="selected" @endif>21:30</option>
                            <option value="2200" class="text-capitalize" @if('2200' == $item->to_time) selected="selected" @endif>22:00</option>
                            <option value="2230" class="text-capitalize" @if('2230' == $item->to_time) selected="selected" @endif>22:30</option>
                            <option value="2300" class="text-capitalize" @if('2300' == $item->to_time) selected="selected" @endif>23:00</option>
                            <option value="2330" class="text-capitalize" @if('2330' == $item->to_time) selected="selected" @endif>23:30</option>
                            <option value="2359" class="text-capitalize" @if('2359' == $item->to_time) selected="selected" @endif>23:59</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Quantity:</label>
                        <div class="col-lg-9">
                            <input value="{{ $item->quantity }}" type="text" class="form-control form-control-lg quantity" name="quantity"
                                placeholder="Item Quantity " required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is Recommended?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary recommendeditem" @if($item->is_recommended) checked="checked" @endif name="is_recommended">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is Popular?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary popularitem" @if($item->is_popular) checked="checked" @endif name="is_popular">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Is New?</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switchery mt-2">
                                <label>
                                <input value="true" type="checkbox" class="switchery-primary newitem" @if($item->is_new) checked="checked" @endif name="is_new">
                                </label>
                            </div>
                        </div>
                    </div>
                    @csrf
                    <div class="text-left">
                        <div class="btn-group btn-group-justified" style="width: 225px">
                            @if($item->is_active)
                            <a class="btn btn-primary" href="{{ route('restaurant.disableItem', $item->id) }}">
                            DISABLE
                            <i class="icon-switch2 ml-1"></i>
                            </a>
                            @else
                            <a class="btn btn-danger" href="{{ route('restaurant.disableItem', $item->id) }}">
                            ENABLE
                            <i class="icon-switch2 ml-1"></i>
                            </a>
                            @endif 
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                        UPDATE
                        <i class="icon-database-insert ml-1"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $('.slider-preview-image')
                    .removeClass('hidden')
                    .attr('src', e.target.result)
                    .width(120)
                    .height(120);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(function () {
        $('.select').select2({
            minimumResultsForSearch: Infinity,
        });
    
         var recommendeditem = document.querySelector('.recommendeditem');
        new Switchery(recommendeditem, { color: '#f44336' });
    
        var popularitem = document.querySelector('.popularitem');
        new Switchery(popularitem, { color: '#8360c3' });
    
        var newitem = document.querySelector('.newitem');
        new Switchery(newitem, { color: '#333' });
        
        $('.form-control-uniform').uniform();
        $('.price').numeric({allowThouSep:false, maxDecimalPlaces: 2 });
    });
</script>
@endsection