@extends('admin.layouts.master')
@section("title") Kitchens - Dashboard
@endsection
@section('content')
<div class="page-header">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><i class="icon-circle-right2 mr-2"></i>
                <span class="font-weight-bold mr-2">Number of Kitchens</span>
                <span class="badge badge-primary badge-pill animated flipInX">{{ count($kitchens) }}</span>
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none py-0 mb-3 mb-md-0">
            <div class="breadcrumb">
                <button type="button" class="btn btn-secondary btn-labeled btn-labeled-left mr-2" id="addNewKitchen"
                    data-toggle="modal" data-target="#addNewKitchenModal">
                    <b><i class="icon-user-plus"></i></b>
                    Add New Kitchen
                </button>
            </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="row mt-4">
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-city"></i>
                        </div>
                        <div class="dashboard-display-number">{{ count($kitchens) }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Kitchens</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-basket"></i>
                        </div>
                        <div class="dashboard-display-number">{{ count($kitchenItemsInProgress) }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Items In Progress</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-stack-star"></i>
                        </div>
                        <div class="dashboard-display-number">{{ count($preparingOrders) }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Items Cooked</div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-6 col-xl-3">
            <div class="col-xl-12 dashboard-display p-3">
                <a class="block block-link-shadow text-right" href="javascript:void(0)">
                    <div class="block-content block-content-full clearfix">
                        <div class="float-left mt-10 d-none d-sm-block">
                            <i class="dashboard-display-icon icon-coin-dollar"></i>
                        </div>
                        <div class="dashboard-display-number">{{ count($kitchens) }}</div>
                        <div class="font-size-sm text-uppercase text-muted">Available Items</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row pt-4 p-0">
        <div class="col-xl-12">
            <div class="panel panel-flat dashboard-main-col mt-4">
                <div class="panel-heading">
                    <h4 class="panel-title pl-3 pt-3"><strong>Preparing Items</strong></h4>
                    <hr>
                </div>
                <div id="newOrdersTable" class="table-responsive @if(!count($kitchenItemsInProgress)) hidden @endif">
                    <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th class="text-center"><i class="
                                icon-circle-down2"></i></th>
                                <th>Order Status</th>
                            </tr>
                        </thead>
                        <tbody id="newOrdersData">
                            @php $prevKitchen = ''; $prevOrder = '' @endphp
                            @foreach($kitchenItemsInProgress as $nO)
                            <tr>
                                <td>
                                    {{ $prevOrder != $nO->order->unique_order_id ? $nO->order->unique_order_id : "" }}
                                    @php $prevOrder = $nO->order->unique_order_id; @endphp
                                </td>
                                <td>
                                    {{$nO->name}}
                                </td>
                                <td>
                                    {{$nO->quantity}}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('restaurant.acceptOrderItem', $nO->id, $nO->order_id) }}"
                                        class="badge badge-primary badge-icon"> Mark Item Ready <i
                                            class="icon-checkmark3 ml-1"></i></a>
                                </td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        PREPARING
                                    </span>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-3">
                        {{$kitchenItemsInProgress->links() }}
                    </div>
                </div>
                @if(!count($kitchenItemsInProgress))
                <div class="text-center text-muted pb-2" id="newOrdersNoOrdersMessage">
                    <h4> No orders to show</h4>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-xl-12">
        <div class="panel panel-flat dashboard-main-col mt-4">
            <div class="panel-heading">
                <h4 class="panel-title pl-3 pt-3"><strong>Preparing Orders</strong></h4>
                <hr>
            </div>
            @if(count($preparingOrders))
            <div class="table-responsive">
                <table class="table text-nowrap">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th class="text-center"><i class="icon-circle-down2"></i></th>
                            <th>Order Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $prevKitchen = ''; $prevOrder = '' @endphp
                        @foreach($preparingOrders as $aO)
                        <tr>
                            <td>
                                {{ $prevOrder != $aO->order->unique_order_id ? $aO->order->unique_order_id : "" }}

                                @php $prevOrder = $aO->order->unique_order_id; @endphp
                            </td>
                            <td>
                                {{$aO->name}}
                            </td>
                            <td>
                                {{$aO->quantity}}
                            </td>
                            <td class="text-center">
                                <a href="{{ route('restaurant.acceptOrderItem', $aO->orderitemId) }}"
                                    class="badge badge-primary badge-icon"> Mark Item Ready <i
                                        class="icon-checkmark3 ml-1"></i></a>
                            </td>
                            <td>
                                @if ($aO->item_status == 2)
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                    PREPARING
                                </span>
                                @elseif ($aO->item_status == 3)
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize"
                                    style="background: green; color: white">
                                    COMPLETED
                                </span>
                                @else
                                <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                    CHECK IN KITCHEN
                                </span>
                                @endif

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{$preparingOrders->links() }}
                </div>
                @else
                <div class="text-center text-muted pb-2">
                    <h4> No orders to show</h4>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="pt-4">
        <div class="col-xl-12 ">
            <div class="card card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th style="width: 15%">Completed Items</th>
                                <th class="text-center" style="width: 10%;"><i class="
                                    icon-circle-down2"></i></th>
                                <th>View Kitchen Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kitchens as $kitchen)
                            <tr>
                                <td>{{ $kitchen->id }}</td>
                                <td>{{ $kitchen->name }}</td>
                                <td>
                                    <span class="badge badge-flat border-grey-800 text-default text-capitalize">
                                        @if($kitchen->is_active) Active @else Inactive @endif
                                    </span>
                                </td>
                                <td>{{ count($preparingOrders) }}</td>
                                <td class="text-center">
                                    @if($kitchen->is_active)
                                    <a href="{{ route('restaurant.inActiveNewKitchen', $kitchen->id) }}"
                                        class="badge badge-primary badge-icon ml-1" data-popup="tooltip"
                                        title="Disable Kitchen" data-placement="bottom"> <i class="icon-switch2"></i>
                                    </a>
                                    @else
                                    <a href="{{ route('restaurant.inActiveNewKitchen', $kitchen->id) }}"
                                        class="badge badge-danger badge-icon ml-1" data-popup="tooltip"
                                        title="Enable Category" data-placement="bottom"> <i class="icon-switch2"></i>
                                    </a>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('restaurant.viewKitchenStatus', $kitchen->id) }}"
                                        class="badge badge-primary badge-icon"> VIEW <i
                                            class="icon-file-eye ml-1"></i></a>
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="addNewKitchenModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="font-weight-bold">Add New Kitchen</span></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="{{ route('kitchens.saveNewKitchen') }}" method="POST" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Kitchen Name:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control form-control-lg" name="name"
                                placeholder="Kitchen Name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label"><span class="text-danger">*</span>Restaurant:</label>
                        <div class="col-lg-9">
                            <select class="form-control select-search" name="restaurant_id" required>
                                @foreach ($restaurant as $restaurants)
                                <option value="{{ $restaurants->id }}" class="text-capitalize">{{ $restaurants->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @csrf
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">
                            SAVE
                            <i class="icon-database-insert ml-1"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection